import React, { useEffect } from 'react';
import { View, Text, ImageBackground, Image, Switch, TextInput, TouchableOpacity, StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../Utility/index';


const ImageStart = ({ navigation,Images }) => {


    return (

        <View>
            <Image source={Images} style={styles.img} resizeMode="contain" ></Image>



        </View>

    );
};

export default ImageStart;

const styles = StyleSheet.create({
    img:
    {
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,

        width: wp("100%"),
        height: hp("35%"),
        top: "5%"

    },
});