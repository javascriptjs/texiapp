import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../../Utility/index';
import constants from '../../../Constants/colors';

const iconStyles = {
    size: 100,
    color: '#FFFFFF',
};

const styles = StyleSheet.create({
    mainview:
    {
        flex: 1,
        backgroundColor: constants.backgroundDull
    },
    containerMain: {
        flex: 1,
        backgroundColor: constants.backgroundDull, width: "100%",
        height: "100%"

    },
    text2:
    {
        color: "black",
        marginTop: hp("3%"),
        fontSize: wp("4.8%"),
        fontWeight: "bold",
        textAlign: "center",
        justifyContent: "flex-start",
    },
    main1:
    {
        alignSelf: "center",
        marginTop: hp("10%"),
        backgroundColor: "#FFFFFF",
        elevation: 10,
        borderRadius: 20,
        width: "90%",

    },

    img:
    {
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,

        width: wp("100%"),
        height: hp("35%"),
        top: "5%"

    },
    mainbtn1:
    {
        marginBottom: "10%"
    },
    mainbtn:
    {
        marginTop: hp("4%"),
        marginBottom: "10%"
    },
    btnview:
    {
         backgroundColor: constants.bluebtn,
        marginTop: hp("5%"),
        borderRadius: 25,



        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        alignSelf: "center",

        width: wp("80%"),
        height: hp("7.5%"),
    },
    btnview2:
    {
        backgroundColor: constants.backgroundDull,
        marginTop: "10%",
        borderRadius: 25,
        elevation: 10,

        alignSelf: "center",

        width: wp("80%"),
        height: hp("7.5%"),
    },
    btnview1:
    {
        backgroundColor: constants.backgroundDull,
        marginTop: "10%",
        borderRadius: 25,
        elevation: 10,



        flexDirection: "row",
        alignItems: "center",
        alignSelf: "center",

        width: wp("80%"),
        height: hp("7.5%"),
    },
    textbtn1:
    {
        color: "black",
        fontSize: wp("4%")

    },

    textbtn:
    {
        color: "white",
        fontSize: wp("4%")
    },
    amount:
    {
        color: "black",
        fontSize: wp("4%"),
        marginTop: hp("1%"),
        textAlign: "center",
        justifyContent: "center",
        alignItems: "center",
    },
    placeholder: { fontSize: wp('4%'), right: 1 },
    drop: {
        backgroundColor: '#ffffff00',
        right: 7,
        borderColor: '#ffffff00',
        borderWidth: 2,
        width: '85%',
        alignSelf: 'center',
    },
    droptext: {
        backgroundColor: 'white',
        width: '83%',
        alignSelf: 'center',
    },
    heightin: { height: 49 },


    SectionStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#000',
        alignSelf: "center",
        width: wp("80%"),
        height: hp("7.5%"),


        backgroundColor: constants.backgroundDull,
        marginTop: "10%",
        borderRadius: 25,
        elevation: 10,
    },
    SectionStyle1: {
        flexDirection: 'row',
        alignSelf: "center",
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#000',
        width: wp("80%"),
        height: hp("7.5%"),


        backgroundColor: constants.backgroundDull,
        marginTop: "5%",
        borderRadius: 25,
        elevation: 10,
    },

    ImageStyle: {

        alignItems: 'center',
        width: wp("5%"), height: hp("5%")
    },
    ImageStyle1: {

        alignItems: 'center', left: wp("2%"),
        width: wp("4%"), height: hp("4%")
    },
    ImageStyle3: {

        alignItems: 'center',
        width: wp("4%"), height: hp("4%"),
    },
    row:
    {
        flexDirection: "row",
        justifyContent: "center",
        alignSelf: "center",


        marginTop: "4%",
        marginBottom: "4%",



    },
    lineview:
        { flexDirection: 'row', alignItems: 'center', marginTop: ("5%"), width: wp("80%"), alignSelf: "center" },
    line: { flex: 1, height: hp("0.1%"), backgroundColor: 'black' },
    or: { textAlign: 'center', marginLeft: wp("10%"), marginRight: wp("10%"), fontWeight: "bold", fontSize: wp("4%") },
    container:
    {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 25,
        marginTop: "5%"
    },

    textBoxBtnHolder:
    {
        paddingLeft: "3%",
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: "center",
        width: wp("80%"),
        height: hp("7.5%"),

        backgroundColor: constants.backgroundDull,
        borderRadius: 25,
        elevation: 10,
    },
    btnImage:
    {
        alignItems: 'center',
        width: wp("5%"), height: hp("5%")
    },
    visibilityBtn:
    {
        position: 'absolute',
        right: 3,
        height: 40,
        width: 35,
        padding: 5
    },
    link:
        { color: "black", textAlign: "center", fontSize: wp("4%") }


});
export default styles;