import { Dimensions, StyleSheet, Platform } from 'react-native';
const window = Dimensions.get('window');
import constants from '../../Constants/colors';

import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from '../../Utility/index';
const { strings, colors, fonts, urls, PATH } = constants;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    main:
    { flex: 1,
         backgroundColor: constants.backgroundDull },
    innerhead:
    {
        width: '90%',
        alignSelf: 'center',
        justifyContent: 'flex-start',
        marginTop: hp("3%")
    },
    icon:
    {
        color: constants.greyesh,
        alignSelf: "center",
        alignItems: 'center',
    },
    button:
    {
        width: wp('90%'),
        backgroundColor: constants.Button_Color,
        alignItems: 'center',
        justifyContent: 'center',
        height: hp('6%'),
        borderRadius: 8,
    },
    titlestyle:
    {
        color: constants.white_Colors,
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        alignSelf: 'center',
        fontFamily: 'Poppins',
        fontSize: wp("4%"),

    },
    main:
    {
        flexDirection: 'row',
        width: wp('90%'), marginTop: hp('5%'),
        justifyContent: 'space-between',
        // borderTopRightRadius: 20,
        //  borderTopLeftRadius: 20, 
        paddingTop: hp("2%"),
        paddingBottom: hp("2%"),
        alignSelf: 'center',
    },
    starImageStyle: {
        width: wp('4%'),
        height: hp('4%'),
        // resizeMode: 'cover',
    },
    logic:
        { color: 'black', left: wp('1%'), fontWeight: '700', fontSize: wp('3.5%') },
    customRatingBarStyle: {
        // justifyContent: 'center',
        flexDirection: 'row',
    },
    imgview:
    {
        flexDirection: 'column',
        width: wp('20%'),
        justifyContent: 'center',
    },
    img:
        { borderRadius: 100 },
    column:
        { flexDirection: 'column', width: wp('60%'), paddingLeft: wp("2%") },
    column2:
        { flexDirection: 'column', width: wp('10%'), alignItems: "center", justifyContent: "space-between" },
    row:
        { flexDirection: "row", },
    names:
        { fontSize: wp('4%'), fontWeight: '700' },
    complete:
    {
        backgroundColor: constants.bluebtn,
        paddingLeft: wp("7%"),
        paddingRight: wp("7%"),
        //  paddingTop: hp("1%"),
        // paddingBottom: hp("1%"), 
        borderRadius: 30
    },
    km:
        { color: constants.red, fontSize: wp("3.5%") },
    km2:
        { color: constants.grey, fontSize: wp("3.5%") },
    imgmain:
    {
        flexDirection: "row",
        marginTop: hp("1%"),
        width: wp('70%'),
        justifyContent: "space-between"
    },
    btnview:
    {
        backgroundColor: constants.white,
        // borderWidth:1,
        elevation: 2,
        marginTop: hp("5%"),
        borderRadius: 50,
        flexDirection: "row",
        //  justifyContent: "center",
        alignItems: "center",
        alignSelf: "center",

        width: wp("90%"),
        height: hp("7.5%"),
    },
    rowsearch:
        { flexDirection: "row", alignItems: "center", width: wp("90%") },
    col1:
        { flexDirection: "column", width: wp("10%"), alignItems: "center" },
    col2:
        { flexDirection: "column", width: wp("70%"), paddingLeft: wp("4%") },
    col3:
        { flexDirection: "column", width: wp("20%"), alignItems: "center" },
});

export default styles;
