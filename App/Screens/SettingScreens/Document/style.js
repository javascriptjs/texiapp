import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../../Utility/index';
import constants from '../../../Constants/colors';



const styles = StyleSheet.create({

 marginview:
 { marginTop: hp("3%"), alignSelf: "center", width: wp("80%"), },
 marginview1:
 { marginBottom:wp("10%")},
text:
{ fontSize: wp("3.5%"), textAlign: "left" },
infoview:
{
    color: constants.black_Text,
  marginTop: hp("2%"),
  fontSize: wp("4%"),
  fontWeight: "bold",
  justifyContent: "flex-start",},
  info:
  {width:wp('70%'),alignSelf:"center",},
});
export default styles;
