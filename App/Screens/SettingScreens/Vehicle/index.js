import React, { useEffect } from 'react';
import { useState } from 'react';
import { View, Text, ImageBackground, Image, Switch, TouchableOpacity } from 'react-native';
import Mainheader from '../../../Components/Mainheader';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from '../../../Utility';
import styles from "./style"
import TextInputleft from '../../../Components/TextInputleft'
import { ScrollView } from 'react-native-gesture-handler';
import CommonButton from '../../../Components/Button'

const Vechile = ({ navigation }) => {
    const [Name, SetvehicleName] = useState('');

    const [Modal, SetvehicleModal] = useState('');
    const [Year, SetvehicleYear] = useState('');
    const [Plate, SetvehiclePlate] = useState('');
    const [Color, SetvehicleColor] = useState('');
    const [Type, SetvehicleType] = useState('');

    return (

        <View style={styles.main}>
            <Mainheader navigation={navigation} navigating={() => navigation.goBack()} props='Add Vehicle' image={"arrow-back"}></Mainheader>
            <ScrollView >
                <View style={{ marginTop: hp("4%") }}>
                    <View style={styles.marginview}>
                        <Text style={styles.text}>VEHICLE BRAND</Text>
                        <TextInputleft Image3={'arrow-forward-ios'} onchangetexting={Name => SetvehicleName(Name)}></TextInputleft>
                    </View>
                    <View style={styles.marginview}>
                        <Text style={styles.text}>MODEL</Text>
                        <TextInputleft Image3={'arrow-forward-ios'} onchangetexting={Modal => SetvehicleModal(Modal)}></TextInputleft>
                    </View>
                    <View style={styles.marginview}>
                        <Text style={styles.text}>YEAR</Text>
                        <TextInputleft Image3={'arrow-forward-ios'} onchangetexting={Year => SetvehicleYear(Year)}></TextInputleft>
                    </View>
                    <View style={styles.marginview}>
                        <Text style={styles.text}>LICENSE PLATE</Text>
                        <TextInputleft Image3={'arrow-forward-ios'} onchangetexting={Plate => SetvehiclePlate(Plate)}></TextInputleft>
                    </View>
                    <View style={styles.marginview}>
                        <Text style={styles.text}>COLOR</Text>
                        <TextInputleft Image3={'arrow-forward-ios'} onchangetexting={Color => SetvehicleColor(Color)}></TextInputleft>
                    </View>
                    <View style={styles.marginview}>
                        <Text style={styles.text}>BOOKING TYPE</Text>
                        <TextInputleft Image3={'arrow-forward-ios'} onchangetexting={Type => SetvehicleType(Type)}></TextInputleft>
                    </View>
                    <View style={styles.marginview1}>
                    <CommonButton navigating={()=>navigation.navigate("Setting")} props={"Save Now"}></CommonButton>

</View>
                </View>
            </ScrollView>
        </View>
    );
};

export default Vechile;
