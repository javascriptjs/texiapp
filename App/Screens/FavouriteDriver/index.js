

import React, { useState, useRef, useEffect } from 'react';
import { SafeAreaView, StyleSheet, View, Text, Image, TouchableOpacity, Switch, TextInput,FlatList } from 'react-native';
import styles from './style';
import LinearGradient from 'react-native-linear-gradient';
import MenuHeader from '../../Components/Mainheader';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../Utility/index';
import MapboxGL from '@react-native-mapbox-gl/maps';
import { Input, Button } from 'react-native-elements';
import constants from '../../Constants/colors';

import Mainheader from '../../Components/Mainheader';


import Subheading from '../../Components/Subheading';
import { ScrollView } from 'react-native-gesture-handler';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import RowArrowView from '../../Components/RowArrowView'
import { Avatar } from 'react-native-paper';

const FavouriteDriver = ({ navigation }) => {


    const data = [
        {
            text1: 'Jhon Smith',
            text2: '$20.00/hr',
            text3: '4.5',
        },
        {
            text1: 'Danial',
            text2: '$20.00/hr',
            text3: '4.5',
        },
        {
            text1: 'Aexa Soa',
            text2: '$24.00/hr',
            text3: '4.5',
        },
        {
            text1: 'Jhon Smith',
            text2: '$29.00/hr',
            text3: '4',
        },
        {
            text1: 'Jhon ',
            text2: '$20.00/hr',
            text3: '4.5',
        },
        {
            text1: 'Jhon Smith',
            text2: '$20.00/hr',
            text3: '4.5',
        },
        {
            text1: 'Bodmash',
            text2: '$20.00/hr',
            text3: '4',
        },
        {
            text1: 'Jhon Smith',
            text2: '$10.00/hr',
            text3: '4.5',
        },
        {
            text1: 'Jhon Smith',
            text2: '$20.00/hr',
            text3: '4.5',
        },
        {
            text1: 'Bodmash',
            text2: '$30.00/hr',
            text3: '3.5',
        },
        {
            text1: 'Jhon Smith',
            text2: '$20.00/hr',
            text3: '5.5',
        },


    ];

    return (
        <View style={styles.main}>

              {/* <Mainheader navigation={navigation}  navigating={() => navigation.goBack()}props='Favorite Driver' image={"arrow-back"}></Mainheader> */}
              <MenuHeader navigation={navigation} image={'menu'} props='Favorite Driver' navigating={() => navigation.openDrawer()} >

</MenuHeader>
<ScrollView>

<View style={{width:wp("95%"),alignSelf:"center",}}>

<FlatList
                        data={data}
                        numColumns={3}
                        renderItem={({ item }) =>
            <View style={styles.row1}>
                <View style={styles.imageview}>
                    <View style={styles.col1}>
                        <View style={styles.row2}>

                            <MaterialCommunityIcons name={"heart"} size={wp("6%")} color={constants.red} onPress={() => SetLike(false)} />
                        </View>
                        <View style={styles.col2}>
                            <View style={styles.row3}>

                                <Text style={styles.rating}>{item.text3}</Text>

                                <MaterialIcons name={"star-rate"} size={wp("6%")} color={constants.yellow} onPress={() => SetStar(false)} />

                            </View>
                        </View>
                    </View>
                    <Avatar.Image style={styles.img} size={wp("19%")} source={{ uri: 'https://i.stack.imgur.com/uoVWQ.png' }} />


                    <Text style={styles.Text1}>{item.text1}</Text>

                    <Text style={styles.username}>{item.text2}</Text>

                </View>
                
               
</View>
    }/>
    
</View>
</ScrollView>
        </View>
    );

};


export default FavouriteDriver;

