import React, { useEffect } from 'react';
import { useState } from 'react';
import { ScrollView } from 'react-native';
import { View, Text, ImageBackground, Image, Switch, TextInput, TouchableOpacity } from 'react-native';
import Mainheader from '../../../Components/Mainheader';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../../Utility/index';
import ImageStart from '../../../Components/ImageStart';
import Mainheading from '../../../Components/Mainheading';
import Subheading from '../../../Components/Subheading';
import CommonButton from '../../../Components/Button'
import YellowBtn from '../../../Components/yellowbtn'

import TextInputing from '../../../Components/TextInputing'
import styles from './style';

const PaymentMethod = ({ navigation }) => {


    return (
        <View style={styles.main}>

            <Mainheader navigation={navigation} navigating={() => navigation.goBack()} props='Payment Method' image={"arrow-back"}></Mainheader>
            <ScrollView>
                <View style={styles.containerMain}>
                    <Image source={require('../../../Assets/success.png')} style={styles.img} resizeMode="contain" ></Image>
                    <View style={{}}>
                        <View style={styles.main1}>
                        <Mainheading props={"Payment Success"}></Mainheading>
                            <View style={{ flexDirection: "row", alignSelf: 'center' }}>
                                <Text style={styles.amount2}>Tip ID # 125050</Text>
                         
                            </View>
                            <Subheading props={"Thank you for choosing our\nservice and trusted Ride to help you\nwith your problems"}></Subheading>

                        </View>


                      
                        <YellowBtn navigating={()=>navigation.navigate("Setting")}props={"Leave a Review"}></YellowBtn>

                        <CommonButton navigating={()=>navigation.navigate("Home")}  props={"Back to Home"}></CommonButton>


                    </View>
                </View>
            </ScrollView>
        </View>

    );
};

export default PaymentMethod;
