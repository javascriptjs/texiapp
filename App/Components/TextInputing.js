
import React, { useState } from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Alert,
  Platform,
  Switch,
  TextInput
} from 'react-native';
import constants from '../Constants/colors';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from '../Utility/index';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

const  TextInputing= ({ navigation, Image1,Image2,Image3,secureTextEntryes,placeholdertext,Keyboard,onchangetexting, autoCapitaL}) => {

  return (
        <View style={styles.container}>
                                <View style={styles.textBoxBtnHolder}>
                                    {/* <Image source={Image1} resizeMode="contain" style={styles.ImageStyle} /> */}
                                    <MaterialIcons name={Image1} size={wp("6%")}  color={constants.black_Text} />

                                    <Image source={Image2} resizeMode="contain" style={styles.ImageStyle1} ></Image>
                                    <TextInput
                                        style={{ flex: 1, left: wp("1%"), fontSize: wp("4%") }}
                                        placeholder={placeholdertext}
                                        placeholderTextColor={constants.blackicon}
                                        keyboardType={Keyboard}
                                        onChangeText={onchangetexting}
                                        autoCapitalize={autoCapitaL}
                                       secureTextEntry={secureTextEntryes}

                                    />
                                    <TouchableOpacity
                                        activeOpacity={0.8}
                                        style={styles.visibilityBtn}
                                    >
                                      

                                        <Image source={Image3} resizeMode='contain' style={styles.btnImage} />
                                    </TouchableOpacity>
                                </View>
                            </View>


  );
};
export default TextInputing;


const styles = StyleSheet.create({
  container:
  {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      paddingHorizontal: 25,
      marginTop: "5%"
  },

  textBoxBtnHolder:
  {
      paddingLeft: "3%",
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      alignSelf: "center",
      width: wp("80%"),
      height: hp("7.5%"),

      backgroundColor: constants.faidwhite,
      borderRadius: 25,
      elevation: 10,
  },
  btnImage:
  {
      alignItems: 'center',
      width: wp("5%"), height: hp("5%")

  },
  ImageStyle: {

    alignItems: 'center',
    width: wp("5%"), height: hp("5%")
},
ImageStyle1: {

    alignItems: 'center', left: wp("2%"),
    width: wp("4%"), height: hp("4%")
},
  visibilityBtn:
  {
      position: 'absolute',
      right: 3,
      height: 40,
      width: 35,
      padding: 5
  },
});