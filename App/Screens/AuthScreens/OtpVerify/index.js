

import React, { useState } from 'react';
import { SafeAreaView, StyleSheet, View, Text, Image, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import styles from './style';
import { Header } from 'react-native-elements';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../../Utility/index';
import Mainheader from '../../../Components/Mainheader';
import CommonButton from '../../../Components/Button'
import TextInputing from '../../../Components/TextInputing'
import ImageStart from '../../../Components/ImageStart';
import Mainheading from '../../../Components/Mainheading';
import Subheading from '../../../Components/Subheading';
import constants from '../../../Constants/colors';

import {
    CodeField,
    Cursor,
    useBlurOnFulfill,
    useClearByFocusCell,
} from 'react-native-confirmation-code-field';
const OtpVerify = ({ navigation }) => {
    const CELL_COUNT = 4;

    const [value, setValue] = useState('');
    const ref = useBlurOnFulfill({ value, cellCount: CELL_COUNT });
    const [props, getCellOnLayoutHandler] = useClearByFocusCell({
        value,
        setValue,
    });
    const Nextview = () => {
        navigation.navigate("SignIn")
    }



    return (
        <View style={styles.mainhd}>
          
              <Mainheader navigation={navigation}  navigating={() => navigation.goBack()}props='Phone Verification' image={"arrow-back"}></Mainheader>

            <ScrollView>
                <View style={styles.containerMain}>
                    <ImageStart Images={require('../../../Assets/dropwrite.png')}></ImageStart>

                        <View style={styles.main1}>
                        <Mainheading props={"OTP Verification"}></Mainheading>
                            <Subheading props={"An authentication code has been sent to"}></Subheading>
                            <Subheading props={"(+880) 111 222 333"}></Subheading>

                            {/* <Text style={styles.text2}>OTP Verification</Text>
                            <Text style={styles.amount}>An authentication code has been sent to</Text>
                            <Text style={styles.amount}>(+880) 111 222 333</Text> */}


                            <View style={{ width: "80%", alignSelf: "center" }}>
                                <CodeField
                                    ref={ref}
                                    {...props}
                                    // Use `caretHidden={false}` when users can't paste a text value, because context menu doesn't appear
                                    value={value}
                                    onChangeText={setValue}
                                    cellCount={CELL_COUNT}
                                    rootStyle={styles.codeFieldRoot}
                                    keyboardType="number-pad"
                                    textContentType="oneTimeCode"
                                    renderCell={({ index, symbol, isFocused }) => (
                                        <Text
                                            key={index}
                                            style={[styles.cell, isFocused && styles.focusCell]}
                                            onLayout={getCellOnLayoutHandler(index)}>
                                            {symbol || (isFocused ? <Cursor /> : null)}
                                        </Text>
                                    )}
                                />
                            </View>
                            <View style={styles.otphead}>
                                <Text style={styles.receive}>I didn't receive code </Text>
                                <Text style={styles.resend}> Resend Code </Text>

                            </View>
                            <View style={styles.time}>
                                <Text style={styles.timeout}>1:20 Sec left </Text>

                            </View>
                            
                            <CommonButton navigating={() => Nextview()} props={"VERIFY NOW"}></CommonButton>


                        </View>


                    </View>
            </ScrollView>
        </View>
    );

};


export default OtpVerify;
