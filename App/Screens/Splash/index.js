import React, { useEffect } from 'react';
import { useState } from 'react';
import { View, Text, ImageBackground, Image } from 'react-native';

const Splash = ({ navigation }) => {
    useEffect(() => {
    const timeoutHandle = setTimeout(() => {
      retrieveData();
    }, 1000);
  });
  //////AFTER ONE SECOND FUNCATION CALL////
  const retrieveData = () => {
    navigation.navigate('SwiperScreen');
  };
  return (

    <View style={{width:"100%",height:"100%"}}>
       <ImageBackground source={require('../../Assets/background.png')} style={{width:"100%",height:"100%"}} resizeMode="cover" 
       >

       <Image source={require('../../Assets/logo.png')} style={{width:"50%",alignSelf:"center",justifyContent:"center",flex:1}} resizeMode="contain" />

       </ImageBackground>
       
       
     
    </View>
  );
};

export default Splash;
