import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {Alert, Image, StyleSheet} from 'react-native';
import {MainStackNavigator, ContactStackNavigator} from './StackNavigator';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../Utility/index';
import Home from '../Screens/TabScreens/Home'
import Location from "../Screens/TabScreens/Location"
import PickUp from '../Screens/OnlineScreen/PickUp';
import MyProfile from '../Screens/TabScreens/MyProfile';
import FavouriteDriver from '../Screens/FavouriteDriver'
import PickupPoint from '../Screens/TabScreens/PickupPoint'
// import OnlineScreen from "../Screens/OnlineScreen"
const Tab = createBottomTabNavigator();
const BottomTabNavigator = () => {
 return (

  
<Tab.Navigator
initialRouteName="PickUp"
tabBarOptions={{
  activeTintColor: '#3A56FF',
}}
>
<Tab.Screen
  name="Home"
  component={Home}
  options={{
    tabBarLabel: 'Home',
    tabBarIcon: ({ color, size }) => (
      <MaterialCommunityIcons name="home" color={color} size={size} />
    ),
  }}
/>
<Tab.Screen
  name="Discover"
  component={PickupPoint}
  options={{
    tabBarLabel: 'Discover',
    tabBarIcon: ({ color, size }) => (
      <MaterialIcons name="location-on" color={color} size={size} />
    ),
    // tabBarBadge: 3,
  }}
/>
<Tab.Screen
  name="  "
  component={PickupPoint}
  
  options={{
    tabBarIcon: ({ color, size }) => (
        // <Image source={require('../Assets/tabride.png') } resizeMode="contain" style={{width:wp("20%"), bottom:hp("3%")}}></Image>
       
        <Image source={require('../Assets/tabride.png') } resizeMode="contain" style={{width:wp("10%"),}}></Image>

    ),
  }}
/>
<Tab.Screen
  name="Favorites"
  component={FavouriteDriver}
  options={{
    tabBarLabel: 'Favorites',
    tabBarIcon: ({ color, size }) => (
      <MaterialCommunityIcons name="bell" color={color} size={size} />
    ),
     tabBarBadge: 3,
      tabBarBadgeStyle: { backgroundColor: '#FFBC00',color:"white" } 
    
  }}
/>
{/* <Tab.Screen
  name="Favorites"
  component={PickUp}
  options={{
    tabBarLabel: 'Favorites',
    tabBarIcon: ({ color, size }) => (
      <MaterialCommunityIcons name="account" color={color} size={size} />
    ),
  }}
/> */}
<Tab.Screen
  name="Profile"
  component={MyProfile}
  options={{
    tabBarLabel: 'Profile',
    tabBarIcon: ({ color, size }) => (
      <MaterialCommunityIcons name="account" color={color} size={size} />
    ),
  }}
/>
</Tab.Navigator>
 );
};
 
export default BottomTabNavigator;
const styles = StyleSheet.create({
    // tabImg: {
    //   width: 30,
    //   height: 30,
    //   alignSelf: 'center',
    //   tintColor: 'grey',
    // },
    // selectedTabImg: {
    //   width: 30,
    //   height: 30,
    //   alignSelf: 'center',
    //   tintColor: 'skyblue',
    // },
   });
    
    
   