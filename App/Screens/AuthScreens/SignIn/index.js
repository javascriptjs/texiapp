

import React, { useState } from 'react';
import { SafeAreaView, StyleSheet, View, Text, Image, TouchableOpacity, Alert, TextInput, ScrollView } from 'react-native';
import styles from './style';
import { Header } from 'react-native-elements';
import Mainheader from '../../../Components/Mainheader';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../../Utility/index';
import ImageStart from '../../../Components/ImageStart';
import Mainheading from '../../../Components/Mainheading';
import Subheading from '../../../Components/Subheading';
import CommonButton from '../../../Components/Button'
import TextInputing from '../../../Components/TextInputing'
import * as Utility from '../../../Utility/index';
import DropDownPicker from 'react-native-custom-dropdown';
import constants from '../../../Constants/colors';

const SignIn = ({ navigation }) => {
    const [pick, setpick] = useState('');
    const [email, Setemail] = useState('');
    const [phone, Setphone] = useState('');

    const [Password, SetPassword] = useState('');



    const Nextview = () => {
        if (Utility.isFieldEmpty(email.trim() && Password || phone)) {
            return Alert.alert('Please fill all the fields');
        }

        else {
            navigation.navigate("DrawerScreen")
        }
    }

    return (
        <View style={styles.mainhd}>
            <Mainheader navigation={navigation} props='Sign In' navigating={() => navigation.goBack()} image={"arrow-back"}></Mainheader>

            <ScrollView>
                <View style={styles.containerMain}>
                    <ImageStart Images={require('../../../Assets/signInImg.png')}></ImageStart>
                    <View >
                        <View style={styles.main1}>
                            <Mainheading props={"Login"}></Mainheading>
                            <Subheading props={"email and phone number"}></Subheading>
                            {pick == "" ?


                                <DropDownPicker
                                    items={[
                                        { label: 'E-mail', value: 'E-mail', },
                                        { label: 'Phone', value: 'Phone' },
                                    ]}
                                    zIndex={10}
                                    // placeholder="email and phone number "
                                    arrowSize={wp('6%')}

                                    itemStyle={styles.itemdrop}
                                    style={{
                                        width: wp('80%'),
                                        height: hp("7.5%"),
                                        elevation: 10,
                                        borderColor: constants.faidwhite,
                                        alignSelf: 'center',
                                        marginTop: "5%"
                                        , borderBottomEndRadius: 10,
                                        //  backgroundColor: 'white',
                                        backgroundColor: constants.faidwhite,

                                        borderTopRightRadius: 25,
                                        borderTopLeftRadius: 25,
                                        borderBottomLeftRadius: 25,
                                        borderBottomEndRadius: 25,
                                        borderBottomRightRadius: 25
                                    }}
                                    dropDownStyle={{
                                        width: wp('80%'),
                                        elevation: 10,
                                        borderColor: constants.faidwhite,
                                        alignSelf: 'center',
                                        backgroundColor: constants.faidwhite,

                                        borderTopEndRadius: 10,
                                        borderTopRightRadius: 10,
                                        borderTopLeftRadius: 10,
                                        borderBottomEndRadius: 10,
                                        borderBottomLeftRadius: 10,
                                        marginTop: hp('3%')
                                    }}
                                    labelStyle={{
                                        fontSize: wp('4%'),
                                        color: 'black',
                                    }}
                                    onChangeItem={item => setpick({
                                        pick: item.value
                                    })}
                                    selectedLabelStyle={{ fontSize: wp('4%') }}
                                />
                                : null}
                            {pick.pick == "E-mail" ?
                                <View>


                                    <TextInputing Image1={'email'} autoCapitaL={"none"} onchangetexting={email => Setemail(email)}></TextInputing>

                                    <TextInputing Image1={"remove-red-eye"} secureTextEntryes={true} autoCapitaL={"none"} onchangetexting={Password => SetPassword(Password)}></TextInputing>
                                </View>
                                :
                                pick.pick == "Phone" ?
                                    <View>
                                        <TextInputing Image1={"phone"} Keyboard={"numeric"} onchangetexting={phone => Setphone(phone)}></TextInputing>
                                    </View>
                                    : null}

                            <TouchableOpacity onPress={() => navigation.navigate("ForgetPasswd")}>
                                <View style={{
                                    width: wp("80%"),
                                    marginTop: hp("1%"),
                                    alignSelf: "center"
                                }} ><Text style={{
                                    fontSize: wp("4%"),
                                    textAlign: "right"
                                }}>Forgot Your Password?</Text>

                                </View>
                            </TouchableOpacity>
                            <CommonButton navigating={() => Nextview()} props={"SIGN IN"}></CommonButton>


                        </View>

                        <TouchableOpacity onPress={() => navigation.navigate('SignUp')}>
                            <View style={styles.mainbtn}>
                                <Text style={styles.link}>Already have an account? <Text style={{ color: "#3A56FF" }}>Sign Up</Text></Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        </View>
    );

};


export default SignIn;
