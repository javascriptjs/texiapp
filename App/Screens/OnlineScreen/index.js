

import React, { useState, useRef, useEffect } from 'react';
import { SafeAreaView, StyleSheet, View, Text, Image, TouchableOpacity, Dimensions, Switch, } from 'react-native';
import styles from './style';
import LinearGradient from 'react-native-linear-gradient';
import MenuHeader from '../../Components/MenuHeader';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../Utility/index';
import { Input, Button } from 'react-native-elements';
import MapView, { Callout, Marker } from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import { Modalize } from 'react-native-modalize';
import constants from '../../Constants/colors';
import {useSelector, useDispatch} from 'react-redux';

const { width, height } = Dimensions.get('window')
const ASPECT_RATIO = width / height
const LATITUDE_DELTA = 0.005
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO

const OnlineScreen = ({ navigation }) => {
    const GOOGLE_MAPS_APIKEY = "AIzaSyAU2NF-HSqBGtddXdSELItUumaj2LEz1IQ";
    const dispatch = useDispatch();
    const CurrentLocation = useSelector(state => state.GET_CURRENT_LOCATION);
    const [mapsize, setmap] = useState(false)
  
    const updown = (value) => {
        setmap(value)
    }
    const [toggle, setToggle] = useState(true);
    const [pickup, Setpickup] = useState('');

    
    const [Nextsceen, setvalue] = useState('');
    const Nextview = (item) => {
        console.log('item', item)
        if (item.back == true) {
            setvalue(true);
        }
       


    }


    return (
        <View style={{ flex: 1, backgroundColor: constants.backgroundDull }}>

            <MenuHeader navigation={navigation} image={'menu'} props='Online' toggleicon={
                <Switch
                    trackColor={{ false: '#d3d3d3', true: '#d3d3d3', }}
                    thumbColor="#3A56FF"

                    ios_backgroundColor="gray"
                    onValueChange={(value) => setToggle(value)}
                    value={toggle}
                />}
            ></MenuHeader>
          
            <MapView style={styles.map}
                // minZoomLevel={12}
                zoomEnabled={true}
                // minZoomLevel={10}
                initialRegion={{
                    latitude:Number(CurrentLocation.currentLatitude),
                    longitude:Number(CurrentLocation.currentLongitude),
                    latitudeDelta: 0.40,
                    longitudeDelta: 0.40,
                }}>

                <Marker coordinate={{ latitude:Number(CurrentLocation.currentLatitude),
                        longitude:Number(CurrentLocation.currentLongitude),}}>
                <Image
                        source={require('../../Assets/UberX.png')}
                        style={{ width: 30, height: 30, resizeMode: "contain" }}
                        resizeMode="contain"
                    />
                </Marker>
              
                <Marker
                    coordinate={{ latitude: 30.709457, longitude:76.77549 }}>
                  

                </Marker>
                <MapViewDirections
                    origin={{ latitude:Number(CurrentLocation.currentLatitude),
                        longitude:Number(CurrentLocation.currentLongitude),}}
                    destination={{ latitude: 30.709457, longitude:76.77549 }}
                    apikey={"AIzaSyAU2NF-HSqBGtddXdSELItUumaj2LEz1IQ"}
                    strokeWidth={5}
                    strokeColor="blue"
                />
            </MapView>
            {Nextsceen == true ?
                <View style={{ alignSelf: 'center', backgroundColor: "transparent", position: 'absolute', zIndex: 10, bottom: 0, marginBottom: hp("5%") }}>
                    {/* <Button title="Button"></Button> */}


                    <TouchableOpacity onPress={() => navigation.navigate("OnlineScreen")}>
                        <View style={styles.btnview}>
                            <Text style={styles.textbtn}>Go For Pickup</Text>

                        </View>

                    </TouchableOpacity>


                    <TouchableOpacity onPress={() => navigation.navigate("Home")}>

                        <View style={styles.btnview2}>
                            <Text style={styles.textbtn}>View Details</Text>

                        </View>

                    </TouchableOpacity>
                </View>
                :
                <Modalize alwaysOpen={200}>
                    <View style={{ paddingBottom: wp("10%") }}>

                        <View style={{ flexDirection: 'row', width: wp('90%'), padding: hp("2%"), marginTop: hp('5%'), alignSelf: 'center', justifyContent: 'space-between', elevation: 10, borderTopRightRadius: 20, borderTopLeftRadius: 20, backgroundColor: constants.backgroundDull, alignSelf: 'center', }}>
                            <View style={{ flexDirection: 'column', width: wp('20%'), justifyContent: 'flex-start' }}>
                                <Image source={require('../../Assets/Oval.png')} style={{ width: wp('15%'), height: hp('10%') }} resizeMode="contain" ></Image>
                            </View>
                            <View style={{ flexDirection: 'column', width: wp('50%') }}>
                                <Text style={{ fontSize: wp('4%'), fontWeight: '700' }}>Jhon Smith</Text>
                                <Text style={{ fontSize: wp('3.5%'), color: '#BEC2CE' }}>2.2km</Text>

                            </View>

                            <TouchableOpacity
                                activeOpacity={0.8}
                                style={{ alignItems: 'center' }}
                            >
                                <Text style={{ fontSize: wp('4%'), fontWeight: '700' }}>$30.00</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ width: wp('90%'), backgroundColor: constants.white, elevation: 10, borderBottomLeftRadius: 20, borderBottomRightRadius: 20, alignSelf: 'center', paddingBottom: hp("5%") }}>

                            <View style={{ width: wp('90%'), alignItems: 'center', marginTop: hp('3%') }}>
                                <Input
                                    autoCapitalize="none"
                                    placeholder="969 Us  West, Demopolis AL 36732."
                                    label='PICK UP'

                                    labelStyle={{
                                        fontSize: wp('4%'),
                                        left: wp('1%'), fontSize: wp('3%'),
                                        color: '#BEC2CE'
                                    }}

                                    placeholderTextColor="#242E42"
                                    style={styles.margin}
                                    inputContainerStyle={styles.input}
                                    onChangeText={pickup => Setpickup(pickup)}
                                />
                            </View>
                            <View style={{ width: wp('90%'), alignItems: 'center', marginTop: hp('3%') }}>
                                <Input
                                    autoCapitalize="none"
                                    placeholder="South Oates Street, Dothan AL 36301"
                                    label='DROP OFF'

                                    labelStyle={{
                                        fontSize: wp('4%'),
                                        left: wp('1%'), fontSize: wp('3%'),
                                        color: '#BEC2CE'
                                    }}

                                    placeholderTextColor="#242E42"
                                    style={styles.margin}
                                    inputContainerStyle={styles.input}
                                    onChangeText={pickup => Setpickup(pickup)}
                                />
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: '90%', alignSelf: 'center', alignItems: 'center' }}>
                                <TouchableOpacity>
                                    <View style={{
                                        backgroundColor: "#FF5645",
                                        //   marginTop: hp("5%"),
                                        borderRadius: 25,



                                        flexDirection: "row",
                                        justifyContent: "center",
                                        alignItems: "center",
                                        alignSelf: "center",

                                        width: wp("35%"),
                                        height: hp("7.5%"),
                                    }}>
                                        <Text style={{ fontSize: wp('4%'), color: 'white', fontWeight: '700' }}>Ignore</Text>

                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={(item) => Nextview({ back: true })}>
                                    <View style={{
                                        backgroundColor: "#3A56FF",
                                        //   marginTop: hp("5%"),
                                        borderRadius: 25,



                                        flexDirection: "row",
                                        justifyContent: "center",
                                        alignItems: "center",
                                        alignSelf: "center",

                                        width: wp("35%"),
                                        height: hp("7.5%"),
                                    }}>
                                        <Text style={{ fontSize: wp('4%'), color: 'white', fontWeight: '700' }}>Accept</Text>

                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', width: wp('90%'), padding: hp("2%"), marginTop: hp('5%'), alignSelf: 'center', backgroundColor: constants.backgroundDull, elevation: 10, justifyContent: 'space-between', borderTopRightRadius: 20, borderTopLeftRadius: 20, alignSelf: 'center', }}>
                            <View style={{ flexDirection: 'column', width: wp('20%'), justifyContent: 'flex-start' }}>
                                <Image source={require('../../Assets/Oval.png')} style={{ width: wp('15%'), height: hp('10%') }} resizeMode="contain" ></Image>
                            </View>
                            <View style={{ flexDirection: 'column', width: wp('50%') }}>
                                <Text style={{ fontSize: wp('4%'), fontWeight: '700' }}>Jhon Smith</Text>
                                <Text style={{ fontSize: wp('3.5%'), color: '#BEC2CE' }}>2.2km</Text>

                            </View>

                            <TouchableOpacity
                                activeOpacity={0.8}
                                style={{ alignItems: 'center' }}
                            >
                                <Text style={{ fontSize: wp('4%'), fontWeight: '700' }}>$30.00</Text>
                                <Text style={{ fontSize: wp('3.5%'), color: '#BEC2CE' }}>Earned</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ backgroundColor: constants.white, elevation: 10, width: wp('90%'), borderBottomLeftRadius: 20, borderBottomRightRadius: 20, alignSelf: 'center', paddingBottom: hp("5%") }}>

                            <View style={{ width: wp('90%'), alignItems: 'center', marginTop: hp('3%') }}>
                                <Input
                                    autoCapitalize="none"
                                    placeholder="969 Us  West, Demopolis AL 36732."
                                    label='PICK UP'

                                    labelStyle={{
                                        fontSize: wp('4%'),
                                        left: wp('1%'), fontSize: wp('3%'),
                                        color: '#BEC2CE'
                                    }}

                                    placeholderTextColor="#242E42"
                                    style={styles.margin}
                                    inputContainerStyle={styles.input}
                                    onChangeText={pickup => Setpickup(pickup)}
                                />
                            </View>
                            <View style={{ width: wp('90%'), alignItems: 'center', marginTop: hp('3%') }}>
                                <Input
                                    autoCapitalize="none"
                                    placeholder="South Oates Street, Dothan AL 36301"
                                    label='DROP OFF'

                                    labelStyle={{
                                        fontSize: wp('4%'),
                                        left: wp('1%'), fontSize: wp('3%'),
                                        color: '#BEC2CE'
                                    }}

                                    placeholderTextColor="#242E42"
                                    style={styles.margin}
                                    inputContainerStyle={styles.input}
                                    onChangeText={pickup => Setpickup(pickup)}
                                />
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: '90%', alignSelf: 'center', alignItems: 'center' }}>
                                <TouchableOpacity>
                                    <View style={{
                                        backgroundColor: "#FF5645",
                                        //   marginTop: hp("5%"),
                                        borderRadius: 25,



                                        flexDirection: "row",
                                        justifyContent: "center",
                                        alignItems: "center",
                                        alignSelf: "center",

                                        width: wp("35%"),
                                        height: hp("7.5%"),
                                    }}>
                                        <Text style={{ fontSize: wp('4%'), color: 'white', fontWeight: '700' }}>Ignore</Text>

                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={(item) => Nextview({ back: true })}>
                                    <View style={{
                                        backgroundColor: "#3A56FF",
                                        //   marginTop: hp("5%"),
                                        borderRadius: 25,



                                        flexDirection: "row",
                                        justifyContent: "center",
                                        alignItems: "center",
                                        alignSelf: "center",

                                        width: wp("35%"),
                                        height: hp("7.5%"),
                                    }}>
                                        <Text style={{ fontSize: wp('4%'), color: 'white', fontWeight: '700' }}>Accept</Text>

                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', width: wp('90%'), padding: hp("2%"), marginTop: hp('5%'), alignSelf: 'center', justifyContent: 'space-between', borderTopRightRadius: 20, borderTopLeftRadius: 20, backgroundColor: constants.backgroundDull, elevation: 10, alignSelf: 'center', }}>
                            <View style={{ flexDirection: 'column', width: wp('20%'), justifyContent: 'flex-start' }}>
                                <Image source={require('../../Assets/Oval.png')} style={{ width: wp('15%'), height: hp('10%') }} resizeMode="contain" ></Image>
                            </View>
                            <View style={{ flexDirection: 'column', width: wp('50%') }}>
                                <Text style={{ fontSize: wp('4%'), fontWeight: '700' }}>Jhon Smith</Text>
                                <Text style={{ fontSize: wp('3.5%'), color: '#BEC2CE' }}>2.2km</Text>

                            </View>

                            <TouchableOpacity
                                activeOpacity={0.8}
                                style={{ alignItems: 'center' }}
                            >
                                <Text style={{ fontSize: wp('4%'), fontWeight: '700' }}>$30.00</Text>
                                <Text style={{ fontSize: wp('3.5%'), color: '#BEC2CE' }}>Earned</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ width: wp('90%'), backgroundColor: constants.white, elevation: 10, borderBottomLeftRadius: 20, borderBottomRightRadius: 20, alignSelf: 'center', paddingBottom: hp("5%") }}>

                            <View style={{ width: wp('90%'), alignItems: 'center', marginTop: hp('3%') }}>
                                <Input
                                    autoCapitalize="none"
                                    placeholder="969 Us  West, Demopolis AL 36732."
                                    label='PICK UP'

                                    labelStyle={{
                                        fontSize: wp('4%'),
                                        left: wp('1%'), fontSize: wp('3%'),
                                        color: '#BEC2CE'
                                    }}

                                    placeholderTextColor="#242E42"
                                    style={styles.margin}
                                    inputContainerStyle={styles.input}
                                    onChangeText={pickup => Setpickup(pickup)}
                                />
                            </View>
                            <View style={{ width: wp('90%'), alignItems: 'center', marginTop: hp('3%') }}>
                                <Input
                                    autoCapitalize="none"
                                    placeholder="South Oates Street, Dothan AL 36301"
                                    label='DROP OFF'

                                    labelStyle={{
                                        fontSize: wp('4%'),
                                        left: wp('1%'), fontSize: wp('3%'),
                                        color: '#BEC2CE'
                                    }}

                                    placeholderTextColor="#242E42"
                                    style={styles.margin}
                                    inputContainerStyle={styles.input}
                                    onChangeText={pickup => Setpickup(pickup)}
                                />
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: '90%', alignSelf: 'center', alignItems: 'center' }}>
                                <TouchableOpacity>
                                    <View style={{
                                        backgroundColor: "#FF5645",
                                        //   marginTop: hp("5%"),
                                        borderRadius: 25,



                                        flexDirection: "row",
                                        justifyContent: "center",
                                        alignItems: "center",
                                        alignSelf: "center",

                                        width: wp("35%"),
                                        height: hp("7.5%"),
                                    }}>
                                        <Text style={{ fontSize: wp('4%'), color: 'white', fontWeight: '700' }}>Ignore</Text>

                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={(item) => Nextview({ back: true })}>
                                    <View style={{
                                        backgroundColor: "#3A56FF",
                                        //   marginTop: hp("5%"),
                                        borderRadius: 25,



                                        flexDirection: "row",
                                        justifyContent: "center",
                                        alignItems: "center",
                                        alignSelf: "center",

                                        width: wp("35%"),
                                        height: hp("7.5%"),
                                    }}>
                                        <Text style={{ fontSize: wp('4%'), color: 'white', fontWeight: '700' }}>Accept</Text>

                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </Modalize>}


        </View>
    );

};


export default OnlineScreen;
