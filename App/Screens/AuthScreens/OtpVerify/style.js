import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../../Utility/index';
import constants from '../../../Constants/colors';

const styles = StyleSheet.create({
   mainhd:
   { flex: 1,backgroundColor: constants.backgroundDull },
    containerMain: {
        flex: 1,
        backgroundColor: constants.backgroundDull,width:"100%",
        height:"100%"

    },
    timeout:
    { color: "black", fontSize: wp("4%"), },
    time:
    { flexDirection: "row", justifyContent: "space-between", width: "80%", alignSelf: "center", marginTop: hp("1%") },
    resend:
    { color: "black", fontSize: wp("4%"), color: "#FFBC00", width: wp("45%") },
    receive:
    { color: "black", fontSize: wp("4%"), width: wp("45%") },
    viewin:
    { color: 'black', fontWeight: 'bold', fontSize: wp('4%'), },
    contain:
    {
        backgroundColor: 'white',
        elevation: 10,
    },
    otphead:
    { flexDirection: "row", justifyContent: "space-between", width: "80%", alignSelf: "center", marginTop: hp("2%") },
    text2:
    {
        color: "black",
        // fontSize: 20,
        marginTop:hp("3%"),
        fontSize: wp("4.8%"),
        fontWeight: "bold",
        textAlign: "center",
        justifyContent: "flex-start",
        // marginTop: hp("3%")
    },
    main1:
    {
        alignSelf: "center",
        marginTop:hp("10%"),
        backgroundColor:"#FFFFFF",
        elevation:10,
        borderRadius:20,
        width:"90%",
        marginBottom:"10%"

    },

    img:
    {
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,

        width: wp("100%"),
        height: hp("35%"),
        top: "5%"

    },
    mainbtn1:
        {  
        marginBottom:"10%"
    },
    mainbtn:
        { marginTop:hp("4%"), 
        marginBottom:"10%"
    },
    btnview:
    {
        backgroundColor: "#3A56FF",
        marginTop: hp("5%"),
        borderRadius: 25,



        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        alignSelf: "center",

        width: wp("80%"),
        height: hp("7.5%"),
    },
    btnview2:
    {
        backgroundColor: "#F4F5F7",
        marginTop: "10%",
        borderRadius: 25,
        elevation:10,



        // flexDirection: "row",
        // justifyContent: "center",
        // alignItems: "center",
        alignSelf: "center",

        width: wp("80%"),
        height: hp("7.5%"),
    },
    btnview1:
    {
        backgroundColor: "#F3F3F3",
        marginTop: "10%",
        borderRadius: 25,
        elevation:10,



        flexDirection: "row",
        // justifyContent: "center",
        alignItems: "center",
        alignSelf: "center",

        width: wp("80%"),
        height: hp("7.5%"),
    },
    textbtn1:
    {
        color: "black",
        // fontWeight: "bold",
        fontSize: wp("4%")
        
    },

    textbtn:
    {
        color: "white",
        // fontWeight: "bold",
        fontSize: wp("4%")
    },
    amount:
    {
        color: "black",
        fontSize: wp("4%"),
        marginTop: hp("1%"),
        textAlign: "center",
        justifyContent: "center",
        alignItems: "center",
    },
    placeholder: {fontSize: wp('4%'), right: 1},
    drop: {
        backgroundColor: '#ffffff00',
        right: 7,
        borderColor: '#ffffff00',
        borderWidth: 2,
        width: '85%',
        alignSelf: 'center',
      },
      droptext: {
        backgroundColor: 'white',
        width: '83%',
        alignSelf: 'center',
      },
      heightin: {height: 49},


      SectionStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: '#fff',
        // borderWidth: .5,
        borderColor: '#000',
        // height: hp("5%"),
        alignSelf:"center",
        width: wp("80%"),
        height: hp("7.5%"),
        

        backgroundColor: "#F4F5F7",
        marginTop: "10%",
        borderRadius: 25,
        elevation:10,
    },
    SectionStyle1: {
        flexDirection: 'row',
        alignSelf:"center",
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: '#fff',
        // borderWidth: .5,
        borderColor: '#000',
        // height: hp("5%"),
        width: wp("80%"),
        height: hp("7.5%"),
        

        backgroundColor: "#F4F5F7",
        marginTop: "5%",
        borderRadius: 25,
        elevation:10,
    },
     
    ImageStyle: {
      
        alignItems: 'center',
         width: wp("5%"), height: hp("5%") 
    },
    ImageStyle1: {
      
        alignItems: 'center',left:wp("2%"),
         width: wp("4%"), height: hp("4%") 
    },
    ImageStyle3: {
      
        alignItems: 'center',
         width: wp("4%"), height: hp("4%") ,
    },
    row:
    {
        flexDirection: "row",
        justifyContent: "center",
        alignSelf: "center",


                marginTop: "4%",
                marginBottom: "4%",


      
    },
    lineview:
    { flexDirection: 'row', alignItems: 'center' ,marginTop:("5%"),width:wp("80%"),alignSelf:"center"},
    line:{ flex: 1, height: hp("0.1%"), backgroundColor: 'black' },
    or:{  textAlign: 'center' ,marginLeft:wp("10%"),marginRight:wp("10%"),fontWeight:"bold",fontSize:wp("4%")},

    codeFieldRoot: {marginTop: 20},
    cell: {
      width: wp("15%"),
      height: hp("7.5%"),
      lineHeight: 45,
      fontSize: wp("4%"),
      borderWidth: 2,
      borderColor: '#F4F5F7',
      backgroundColor:"#F4F5F7",
      borderRadius:10,
      textAlign: 'center',
    },
    focusCell: {
      borderColor: '#F4F5F7',
    },

});
export default styles;