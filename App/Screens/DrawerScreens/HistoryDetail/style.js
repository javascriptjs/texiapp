import { Dimensions, StyleSheet, Platform } from 'react-native';
const window = Dimensions.get('window');
import constants from '../../../Constants/colors';

import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from '../../../Utility/index';
const { strings, colors, fonts, urls, PATH } = constants;
const styles = StyleSheet.create({
  main1:
  { flexDirection: "row",  paddingTop: hp("2%"),
        paddingBottom: hp("2%"),
        paddingLeft:wp("2%"),backgroundColor:constants.backgroundDull,
        elevation:10,
        width: wp('90%'),
         marginTop: hp('4%'),
          alignSelf: "center" },
        main2:
        {   paddingTop: hp("2%"),
              paddingBottom: hp("2%"),
              paddingLeft:wp("2%"),backgroundColor:constants.backgroundDull,
              elevation:10,width: wp('90%'), 
              marginTop: hp('1%'), 
              alignSelf: "center" },
   mainbtn1:
   {
       marginBottom: "10%",alignSelf:"center"
   },
   btnview:
   {
       backgroundColor: "#3A56FF",
       marginTop: hp("5%"),
       borderRadius: 50,
       flexDirection: "row",
       justifyContent: "center",
       alignItems: "center",
       alignSelf: "center",

       width: wp("80%"),
       height: hp("7.5%"),
   },

   textbtn:
   {
       color: "white",
       // fontWeight: "bold",
       fontSize: wp("4%")
   },
 
});

export default styles;
