

import React, { useState } from 'react';
import { SafeAreaView, StyleSheet, View, Text, Image, TouchableOpacity, Alert, TextInput, ScrollView } from 'react-native';
import styles from './style';
import Mainheader from '../../../Components/Mainheader';
// import { Header } from 'react-native-elements';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../../Utility/index';
import Mainheading from '../../../Components/Mainheading';
import Subheading from '../../../Components/Subheading';
import CommonButton from '../../../Components/Button'
import TextInputing from '../../../Components/TextInputing'
import ImageStart from '../../../Components/ImageStart';
import * as Utility from '../../../Utility/index';

const SignUp = ({ navigation }) => {
    const [email, Setemail] = useState('');
    const [phone, Setphone] = useState('');
    const [password, Setpassword] = useState('');
    const [Confirmpassword, SetConfirmpassword] = useState('');


    const Nextview = () => {
        if (phone) {
            if (Utility.isFieldEmpty(phone)) {
                return Alert.alert('Please fill valid number');
            }
            navigation.navigate("OtpVerify")
        }
        else {
            if (
                Utility.isFieldEmpty((email && password && Confirmpassword))
            ) {
                return Alert.alert('Please fill all the fields');
            } else if (Utility.isValidEmail(email)) {
                return Alert.alert('Please fill valid e-mail');
            } else if (Utility.isValidComparedPassword(password, Confirmpassword)) {
                return Alert.alert('Password Mismatch');
            }
            navigation.navigate("SignIn")

        }

    }

    return (
        <View style={styles.mainview}>
            <Mainheader navigation={navigation} props='Sign Up' navigating={() => navigation.goBack()} image={"arrow-back"}></Mainheader>

            <ScrollView>
                <View style={styles.containerMain}>
                    {/* <Image source={require('../../../Assets/dropwrite.png')} style={styles.img} resizeMode="contain" ></Image> */}
                    <ImageStart Images={require('../../../Assets/dropwrite.png')}></ImageStart>

                    <View style={{}}>
                        <View style={styles.main1}>
                            <Mainheading props={"Sign up with"}></Mainheading>
                            <Subheading props={"email and phone number"}></Subheading>

                            <TextInputing Image1={'email'} autoCapitaL={"none"} onchangetexting={email => Setemail(email)}></TextInputing>
                            <TextInputing Image1={"remove-red-eye"} secureTextEntryes={true} autoCapitaL={"none"} onchangetexting={password => Setpassword(password)}></TextInputing>
                            <TextInputing Image1={"remove-red-eye"} secureTextEntryes={true} autoCapitaL={"none"} onchangetexting={Confirmpassword => SetConfirmpassword(Confirmpassword)}></TextInputing>

                            <View style={styles.lineview}>
                                <View style={styles.line} />
                                <View>
                                    <Text style={styles.or}>OR</Text>
                                </View>
                                <View style={styles.line} />
                            </View>
                            <TextInputing Image1={"phone"} Keyboard={"numeric"} onchangetexting={phone => Setphone(phone)}></TextInputing>

                            <CommonButton navigating={() => Nextview()} props={"SIGN Up"}></CommonButton>


                        </View>
                        <TouchableOpacity onPress={() => navigation.navigate('SignIn')}>
                            <View style={styles.mainbtn}>
                                <Text style={styles.link}>Already have an account? <Text style={{ color: "#3A56FF" }}>Sign In</Text></Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        </View>
    );

};


export default SignUp;
