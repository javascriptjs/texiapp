import { Dimensions, StyleSheet, Platform } from 'react-native';
const window = Dimensions.get('window');
import constants from '../../../Constants/colors';

import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from '../../../Utility/index';
const { strings, colors, fonts, urls, PATH } = constants;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: constants.backgroundDull,
    },
    innerhead:
    {
        width: '90%',
        alignSelf: 'center',
        justifyContent: 'flex-start',
        marginTop: hp("3%")
    },
    icon:
    {
        color: constants.greyesh,
        alignSelf: "center",
        alignItems: 'center',
    },
    button:
    {
        width: wp('90%'),
        backgroundColor: constants.Button_Color,
        alignItems: 'center',
        justifyContent: 'center',
        height: hp('6%'),
        borderRadius: 8,
    },
    imageview:
    {
        width: wp('100%'),
        backgroundColor: constants.bluebtn,
        paddingTop: hp('10%'),
        paddingBottom: hp('10%'),
        alignSelf: "center",
    },

    text2:
    {
        color: constants.yellow,
        fontSize: wp("7%"),
        fontWeight: "bold",
        textAlign: "center",
        justifyContent: "flex-start",
    },
    username:
    {
        color: constants.white,
        fontSize: wp("4%"),
        textAlign: "center",
        justifyContent: "center",
        alignItems: "center",
    },
    titlestyle:
    {
        color: constants.white_Colors,
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        alignSelf: 'center',
        fontFamily: 'Poppins',
        fontSize: wp("4%"),

    },
    infoview:
    {
        color: constants.grey,
        //   marginTop: hp("3%"),
        fontSize: wp("4.2%"),
        fontWeight: "bold",
        justifyContent: "flex-start",
    },
    info:
        { width: wp('90%'), alignSelf: "center", },
    main:
    {
        flexDirection: 'row',
        width: wp('90%'),
        marginTop: hp('3%'),
        paddingTop: hp("2%"),
        paddingBottom: hp("2%"),
        paddingLeft: wp("2%"),
        alignSelf: 'center',
        backgroundColor: constants.white,
        elevation: 20
    },
    starImageStyle: {
        width: wp('4%'),
        height: hp('4%'),
        // resizeMode: 'cover',
    },
    logic:
        { color: 'black', left: wp('1%'), fontWeight: '700', fontSize: wp('3.5%') },
    customRatingBarStyle: {
        // justifyContent: 'center',
        flexDirection: 'row',
    },
    imgview:
    {
        flexDirection: 'column',
        width: wp('20%'),
        justifyContent: 'center',
    },
    img:
        { borderRadius: 100 },
    column:
        { flexDirection: 'column', width: wp('40%'), paddingLeft: wp("2%") },
    column2:
        { flexDirection: 'column', width: wp('30%'), alignItems: "center", },
    row:
        { flexDirection: "row", },
    names:
        { fontSize: wp('4%'), fontWeight: '700' },

    km:
        { color: constants.red, fontSize: wp("3.5%") },
    km2:
        { color: constants.grey, fontSize: wp("3.5%") },
    km3:
        { color: constants.bluebtn, fontSize: wp("3.5%"), fontWeight: '700' },

    imgmain:
    {
        flexDirection: "row",
        marginTop: hp("1%"),
        width: wp('70%'),
        justifyContent: "space-between"
    },
    btnview:
    {
        backgroundColor: constants.white,
        // borderWidth:1,
        elevation: 2,
        marginTop: hp("5%"),
        borderRadius: 50,
        flexDirection: "row",
        //  justifyContent: "center",
        alignItems: "center",
        alignSelf: "center",

        width: wp("90%"),
        height: hp("7.5%"),
    },
    rowsearch:
        { flexDirection: "row", alignItems: "center", width: wp("90%") },
    col1:
        { flexDirection: "column", width: wp("10%"), alignItems: "center" },
    col2:
        { flexDirection: "column", width: wp("70%"), },
    col3:
        { flexDirection: "column", width: wp("20%"), alignItems: "center" },
    mainbtn1:
    {
        // marginBottom: "10%"
        top: hp("-9%")
    },
    textbtn:
    {
        color: "white",
        // fontWeight: "bold",
        fontSize: wp("4%")
    },
    cols1:
        { flexDirection: "column", width: wp("10%"), alignItems: "center", paddingLeft: wp("5%") },
    cols2:
        { flexDirection: "column", width: wp("10%"), alignItems: "center", paddingRight: wp("5%") },
    btnview:
    {
        backgroundColor: constants.yellow,
        marginTop: hp("5%"),
        borderRadius: 50,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        alignSelf: "center",

        width: wp("90"),
        height: hp("7.5%"),
    },
});

export default styles;
