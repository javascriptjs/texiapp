import React, { useEffect } from 'react';
import { useState } from 'react';
import { ScrollView } from 'react-native';
import { View, Text, ImageBackground, Image, Switch, TextInput, TouchableOpacity } from 'react-native';
import Mainheader from '../../../Components/Mainheader';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../../Utility/index';
import ImageStart from '../../../Components/ImageStart';
import Mainheading from '../../../Components/Mainheading';
import Subheading from '../../../Components/Subheading';
import CommonButton from '../../../Components/Button'
import Whitebtn from '../../../Components/Whitebtn'

import TextInputing from '../../../Components/TextInputing'
import styles from './style';

const InviteFriends = ({ navigation }) => {


    return (
        <View style={{ flex: 1, backgroundColor: 'white' }}>

            <Mainheader navigation={navigation} navigating={() => navigation.goBack()} props='Invite Friends' image={"arrow-back"}></Mainheader>
            <ScrollView>
                <View style={styles.containerMain}>
                    <Image source={require('../../../Assets/Invite.png')} style={styles.img} resizeMode="contain" ></Image>
                    <View style={{}}>
                        <View style={styles.main1}>
                        <Mainheading props={"Invite Friends"}></Mainheading>
                            <View style={{ flexDirection: "row", alignSelf: 'center' }}>
                                <Text style={styles.amount1}>$50 </Text><Text style={styles.amount2}>a day Earn up to</Text>
                         
                            </View>
                            <Subheading props={"When your friend sign up with your\nreferral code, you can receive up to\n$50 a day."}></Subheading>

                        </View>
                        <Text style={styles.code}>SHARE YOUR INVITE CODE</Text>


                      
                        <Whitebtn props={"5205dsfd5f"}></Whitebtn>

                        <CommonButton  navigating={()=>navigation.navigate("Invite")} props={"Invites Friends"}></CommonButton>


                    </View>
                </View>
            </ScrollView>
        </View>

    );
};

export default InviteFriends;
