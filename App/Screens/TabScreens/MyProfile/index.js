

import React, { useState, useRef, useEffect } from 'react';
import { SafeAreaView, StyleSheet, View, Text, Image, TouchableOpacity, Switch } from 'react-native';
import styles from './style';
import LinearGradient from 'react-native-linear-gradient';
import MenuHeader from '../../../Components/Mainheader';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../../Utility/index';
import MapboxGL from '@react-native-mapbox-gl/maps';
import { Input, Button } from 'react-native-elements';
import constants from '../../../Constants/colors';

import Mainheading from '../../../Components/Mainheading';
import Subheading from '../../../Components/Subheading';
import { ScrollView } from 'react-native-gesture-handler';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import RowArrowView from '../../../Components/RowArrowView'
import Mainheader from '../../../Components/Mainheader';

const MyProfile = ({ navigation }) => {




    return (
        <View style={styles.main}>

            <MenuHeader navigation={navigation} image={'menu'} props='Profile' navigating={() => navigation.openDrawer()} toggleicon={

                <MaterialCommunityIcons

                name="account-edit" size={wp("6%")} color={constants.blackicon} onPress={()=>navigation.navigate('EditProfile')} />

            }
            ></MenuHeader>
            {/* <Mainheader navigation={navigation}  navigating={() => navigation.goBack()}props='Profile' image={"arrow-back"}
            toggleicon={
                <MaterialCommunityIcons

                name="account-edit" size={wp("6%")} color={constants.blackicon} onPress={()=>navigation.navigate('EditProfile')} />

            
            }></Mainheader> */}


            <View style={styles.imageview}>
                <Image source={{ uri: "https://www.hayalanka.com/wp-content/uploads/2017/07/avtar-image.jpg" }} style={{ width: wp('20%'), height: hp('10%'), borderRadius: 50, alignSelf: "center" }}></Image>
                <Mainheading props={"Jhon Smith"}></Mainheading>
                <Text style={styles.text2}>Gold Member</Text>

            </View>
            <View style={styles.info}>
                <Text style={styles.infoview}>INFORMATIONS</Text>
            </View>

            <RowArrowView prop1={'Username'} prop2={'createuiux'}></RowArrowView>
            <RowArrowView prop1={'Phone number'} prop2={'000-22222'}></RowArrowView>

            <RowArrowView prop1={'Email'} prop2={'mail@website.com'}></RowArrowView>

            <RowArrowView prop1={'Gender'} prop2={'Female'}></RowArrowView>
            <RowArrowView prop1={'Birthday'} prop2={'March 10, 1999'}></RowArrowView>


        </View>
    );

};


export default MyProfile;

