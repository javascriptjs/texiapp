import {
  GET_CURRENT_LOCATION,
} from './Const';

const initialState = {
  GET_CURRENT_LOCATION: {},
};

////========== SET ALL DATA IN STORE BY UNIQUE STATE ========
export default function (state = initialState, {type, payload}) {
  switch (type) {
    case GET_CURRENT_LOCATION: {
      return {
        ...state,
        GET_CURRENT_LOCATION: payload,
      };
    }
  
    default:
      return state;
  }
}
