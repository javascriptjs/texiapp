
import React, { useState, useEffect } from 'react';
import { SafeAreaView, StyleSheet, View, Text, Image, TouchableOpacity,PermissionsAndroid } from 'react-native';
import styles from './style';
// import Geolocation from 'react-native-geolocation-service';
import Geolocation from 'react-native-geolocation-service';
import * as Utility from '../../../Utility/index';

Geolocation.getCurrentPosition(info => console.log(info));
import {
    getLocation
  } from '../../../Redux/Action';
  import {useDispatch, useSelector} from 'react-redux';
const SwiperScreen = ({ navigation }) => {
    const dispatch = useDispatch();
    
    const[value,setlocationvalue]=useState('');
    console.log("value",value)
    // const [locationStatus, setLocationStatus] = useState('');
    const [Nextsceen, setvalue] = useState('');

    const Nextview = (item) => {
        if (item.back == true) {
            setvalue(true);
        }
        if (item.back == null) {
            setvalue(null);
        }


    }
    const [
        currentLongitude,
        setCurrentLongitude
      ] = useState('...');
      const [
        currentLatitude,
        setCurrentLatitude
      ] = useState('...');
      const [
        locationStatus,
        setLocationStatus
      ] = useState('');
    
      useEffect(() => {
        const requestLocationPermission = async () => {
          if (Platform.OS === 'ios') {
            getOneTimeLocation();
            subscribeLocationLocation();
          } else {
            try {
              const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                {
                  title: 'Location Access Required',
                  message: 'This App needs to Access your location',
                },
              );
              if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                //To Check, If Permission is granted
                getOneTimeLocation();
                subscribeLocationLocation();
              } else {
                setLocationStatus('Permission Denied');
              }
            } catch (err) {
              console.warn(err);
            }
          }
        };
        requestLocationPermission();
        return () => {
          Geolocation.clearWatch(watchID);
        };
      }, []);
    
      const getOneTimeLocation = () => {
        setLocationStatus('Getting Location ...');
        Geolocation.getCurrentPosition(
          //Will give you the current location
          (position) => {
            setLocationStatus('You are Here');
    
            //getting the Longitude from the location json
            const currentLongitude = 
              JSON.stringify(position.coords.longitude);
    
            //getting the Latitude from the location json
            const currentLatitude = 
              JSON.stringify(position.coords.latitude);
    
            //Setting Longitude state
            setCurrentLongitude(currentLongitude);
            
            //Setting Longitude state
            setCurrentLatitude(currentLatitude);
          },
          (error) => {
            setLocationStatus(error.message);
          },
          {
            enableHighAccuracy: false,
            timeout: 30000,
            maximumAge: 1000
          },
        );
      };
    
      const subscribeLocationLocation =async() => {
        watchID = Geolocation.watchPosition(async(position) => {
            //Will give you the location on location change
            
            setLocationStatus('You are Here');
            console.log(position);
    
            //getting the Longitude from the location json        
            const currentLongitude =
              JSON.stringify(position.coords.longitude);
    
            //getting the Latitude from the location json
            const currentLatitude = 
              JSON.stringify(position.coords.latitude);
    
            //Setting Longitude state
            setCurrentLongitude(currentLongitude);
    
            //Setting Latitude state
            setCurrentLatitude(currentLatitude);
             await Utility.setInLocalStorge('currentLongitude', currentLongitude);
           
            // setCurrentLatitude(currentLatitude);
            console.log("check location funcation",value)

            // await Utility.setInLocalStorge('currentLatitude', currentLatitude);
            await dispatch(getLocation(currentLatitude,currentLongitude));
          },
          (error) => {
            setLocationStatus(error.message);
          },
          {
            enableHighAccuracy: false,
            maximumAge: 1000
          },
        );
      };
    

   
    return (
        <SafeAreaView style={{ flex: 1 }}>

            {Nextsceen == true ?
                <View style={styles.containerMain}>
                    <Image source={require('../../../Assets/booking.png')} style={styles.img} resizeMode="contain" ></Image>
                    <View style={styles.bottomView}>
                        <View style={styles.main1}>
                            <Text style={styles.text2}>Easy Booking System</Text>
                            <Text style={styles.amount}>Welcome to Gosht Ride{"\n"}tickets online at lowest fares Check{"\n"}Welcome Air flight fares, schedule</Text>
                            <TouchableOpacity onPress={(item) => Nextview({ back: null })}>

                                <View style={styles.mainbtn}>

                                    <View style={styles.btnview}>
                                        <Text style={styles.textbtn}>Get Started</Text>

                                    </View>


                                </View>
                            </TouchableOpacity>

                        </View>
                    </View>
                </View>
                :
                Nextsceen == null ?
                    <View style={styles.containerMain}>
                        <Image source={require('../../../Assets/welcome.png')} style={styles.img} resizeMode="contain" ></Image>
                        <View style={styles.bottomView}>
                            <View style={styles.main1}>
                                <Text style={styles.text2}>Earn From Go Ride</Text>
                                <Text style={styles.amount}>Welcome to Gosht Ride{"\n"}tickets online at lowest fares Check{"\n"}Welcome Air flight fares, schedule</Text>
                                <TouchableOpacity onPress={() => navigation.navigate('SelectLocation')}>

                                    <View style={styles.mainbtn}>

                                        <View style={styles.btnview}>
                                            <Text style={styles.textbtn}>Get Started</Text>

                                        </View>


                                    </View>
                                </TouchableOpacity>

                            </View>
                        </View>
                    </View> :
                    <View style={styles.containerMain}>
                        <Image source={require('../../../Assets/welcome.png')} style={styles.img} resizeMode="contain" ></Image>
                        <View style={styles.bottomView}>
                            <View style={styles.main1}>
                                <Text style={styles.text2}>Welcome to Go Ride</Text>
                                <Text style={styles.amount}>Welcome to Gosht Ride{"\n"}tickets online at lowest fares Check{"\n"}Welcome Air flight fares, schedule</Text>
                                <TouchableOpacity onPress={(item) => Nextview({ back: true })}>

                                    <View style={styles.mainbtn}>

                                        <View style={styles.btnview}>
                                            <Text style={styles.textbtn}>Get Started</Text>

                                        </View>


                                    </View>
                                </TouchableOpacity>

                            </View>
                        </View>
                    </View>
            }
        </SafeAreaView>
    );

};


export default SwiperScreen;
