// import React from 'react';
// import {createDrawerNavigator} from '@react-navigation/drawer';
// import {ContactStackNavigator} from './StackNavigator';
// import TabNavigator from './TabNavigator';
// // import Home from '../app/screens/HomeScreens/Home';
// import DrawerBg from '../Components/Mainheader';
// import PickUp from '../Screens/OnlineScreen/PickUp';
 
 
// const Drawer = createDrawerNavigator();
 
// const DrawerNavigator = () => {
//  return (
//   //  <Drawer.Navigator
//   //  drawerContent={DrawerBg}
     
//   //    >
//   //    <Drawer.Screen
//   //      name="FirstPage"
//   //      options={{drawerLabel: 'First page Option'}}
//   //      component={PickUp}
//   //    />
     
   
//   //  </Drawer.Navigator>
//   <Drawer.Navigator
//    // drawerContent={DrawerBg}
//      drawerContentOptions={{
//       //  activeTintColor: 'green',
//       //  itemStyle: {marginVertical: 5},
//      }}
//      >
//      <Drawer.Screen
//        name="FirstPage"
//        options={{drawerLabel: 'First page Option'}}
//        component={TabNavigator}
//      />
//       <Drawer.Screen
//        name="SecondPage"
//        options={{drawerLabel: 'Second page Option'}}
//        component={TabNavigator}
//      />
//       <Drawer.Screen
//        name="PickUp"
//        component={PickUp}
//        options={{ drawerLabel: 'PickUp' }}
//      />
//      </Drawer.Navigator>
//  );
// };
 
// export default DrawerNavigator;
import React from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {ContactStackNavigator} from './StackNavigator';
import TabNavigator from './TabNavigator';
import OfflineScreen from '../Screens/OfflineScreen';
import OnlineScreen from '../Screens/OnlineScreen';
import Home from '../Screens/TabScreens/Home'
import FavouriteDriver from '../Screens/FavouriteDriver'
import DrawerBg from '../Components/DrawerBg';
import MyProfile from '../Screens/TabScreens/MyProfile'
 import PickupPoint from '../Screens/TabScreens/PickupPoint'
const Drawer = createDrawerNavigator();
 
const DrawerNavigator = () => {
 return (
   <Drawer.Navigator
   drawerContent={DrawerBg}
    
     >
        <Drawer.Screen
       name="OfflineScreen"
       component={OfflineScreen}
       options={{ drawerLabel: 'OfflineScreen' }}
     />
       <Drawer.Screen
       name="OnlineScreen"
       component={OnlineScreen}
       options={{ drawerLabel: 'OnlineScreen' }}
     />
      <Drawer.Screen
       name="Home"
       component={TabNavigator}
       options={{ drawerLabel: 'TabNavigator' }}
     />
      <Drawer.Screen
       name="Profile"
       component={MyProfile}
       options={{ drawerLabel: 'MyProfile' }}
     />
       <Drawer.Screen
       name="FavouriteDriver"
       component={FavouriteDriver}
       options={{ drawerLabel: 'FavouriteDriver' }}
     />
        <Drawer.Screen
       name="PickupPoint"
       component={PickupPoint}
       options={{ drawerLabel: 'PickupPoint' }}
     />
     
     
      {/* <Drawer.Screen
       name="FirstPage"
       options={{drawerLabel: 'First page Option'}}
       component={TabNavigator}
     />  */}
     
      
     
     {/* <Drawer.Screen
       name="SecondPage"
       options={{drawerLabel: 'Second page Option'}}
       component={TabNavigator}
     />
      <Drawer.Screen
       name="SignIn"
       component={SignIn}
       options={{ drawerLabel: 'Home' }}
     /> */}
   
   </Drawer.Navigator>
 );
};
 
export default DrawerNavigator;