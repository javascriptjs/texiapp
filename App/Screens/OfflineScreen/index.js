

import React, { useState, useEffect } from 'react';
import { SafeAreaView, StyleSheet, View, Text, Image, TouchableOpacity, Switch } from 'react-native';
import styles from './style';
import MenuHeader from '../../Components/MenuHeader';
import {useSelector, useDispatch} from 'react-redux';

import MapView, { Callout, Marker } from 'react-native-maps';
const OfflineScreen = ({ navigation }) => {
    const dispatch = useDispatch();
    const CurrentLocation = useSelector(state => state.GET_CURRENT_LOCATION);
    console.log("CurrentLocation...",CurrentLocation)
    const [toggle, setToggle] = useState(false);
    console.log('toggle', toggle)
   
    const [togglevalue, setToggling] = useState(false);
    const [defaultRating, setDefaultRating] = useState(2);
    const [maxRating, setMaxRating] = useState([1, 2, 3, 4, 5]);

    const starImageFilled =
        'https://raw.githubusercontent.com/AboutReact/sampleresource/master/star_filled.png';
    const starImageCorner =
        'https://raw.githubusercontent.com/AboutReact/sampleresource/master/star_corner.png';

    const settoggle = () => {
        setToggle(!togglevalue)
        navigation.navigate("OnlineScreen")
    }
 
   
    const CustomRatingBar = () => {

        return (
            <View style={styles.customRatingBarStyle}>
                {maxRating.map((item, key) => {
                    return (
                        <TouchableOpacity
                            activeOpacity={0.7}
                            key={item}
                            onPress={() => setDefaultRating(item)}>
                            <Image
                                style={styles.starImageStyle}
                                resizeMode='contain'
                                source={
                                    item <= defaultRating
                                        ? { uri: starImageFilled }
                                        : { uri: starImageCorner }
                                }
                            />
                        </TouchableOpacity>
                    );
                })}
            </View>
        );
    };
    return (
        <View style={{ flex: 1 }}>

            <MenuHeader navigation={navigation} image={'menu'} props='Offline' toggleicon={
                <Switch
                    trackColor={{ false: '#d3d3d3', true: '#d3d3d3', }}
                    thumbColor="#242A37"

                    ios_backgroundColor="gray"
                    onValueChange={(value) => settoggle(value)}
                    value={toggle}
                />}
            ></MenuHeader>

            <View style={styles.containerMain}>
                <MapView style={{ flex: 1 }}
                    initialRegion={{
                        latitude:Number(CurrentLocation.currentLatitude),
                        longitude:Number(CurrentLocation.currentLongitude),
                        latitudeDelta: 0.40,
                        longitudeDelta: 0.40,
                    }}
                    showsUserLocation={true}>
                </MapView>

                <View style={styles.bottomView}>

                    <View style={styles.first}>
                        <View style={styles.img}>
                            <Image source={require('../../Assets/Oval.png')} style={styles.oval} resizeMode="contain" ></Image>
                        </View>
                        <View style={styles.mainview}>
                            <Text style={styles.name}>Jhon Smith</Text>
                            <Text style={styles.name2}>Basic level</Text>
                            <View style={styles.star}>
                                <CustomRatingBar />
                                <Text style={styles.logic}>
                                    ( {defaultRating}/{Math.max.apply(null, maxRating)} Review)
                                </Text>
                            </View>
                        </View>

                        <TouchableOpacity
                            activeOpacity={0.8}
                            style={{ alignItems: 'center' }}
                        >
                            <Text style={styles.number}>$30.00</Text>
                            <Text style={styles.earned}>Earned</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.width}>
                        <View style={styles.clock}>
                            <Image source={require('../../Assets/clock.png')} style={styles.clockimg} resizeMode="contain" >
                            </Image>
                            <Text style={styles.time}>20.30</Text>
                            <Text style={styles.hour}>Online Hours</Text>

                        </View>
                        <View style={styles.clock2}>
                            <Image source={require('../../Assets/speed.png')} style={styles.clockimg} resizeMode="contain" >
                            </Image>
                            <Text style={styles.time}>700 KM</Text>
                            <Text style={styles.hour}>Total Ride</Text>

                        </View>
                        <View style={styles.clock3}>
                            <Image source={require('../../Assets/business.png')} style={styles.clockimg} resizeMode="contain" >
                            </Image>
                            <Text style={styles.time}>200</Text>
                            <Text style={styles.hour}>Total Jobs</Text>

                        </View>
                    </View>


                </View>
            </View>

        </View>
    );

};


export default OfflineScreen;
