


import React, { useState } from 'react';
import { SafeAreaView, StyleSheet, View, Text, Image, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import styles from './style';
import Mainheader from '../../../Components/Mainheader';
import DropDownPicker from 'react-native-custom-dropdown';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../../Utility/index';
import CommonButton from '../../../Components/Button'
import ImageStart from '../../../Components/ImageStart';

const SelectLocation = ({ navigation }) => {
  const [pick, setpick] = useState('uk');
  const [pick1, setpick1] = useState('uk');
  const [pick2, setpick2] = useState('uk');
  const Nextview = () => {
    navigation.navigate("SignIn")
  }

  return (
    <View style={styles.mainhd}>
      <Mainheader navigation={navigation} navigating={() => navigation.goBack()} props='Select Location' image={"arrow-back"}></Mainheader>

      <ScrollView>
        <View style={styles.containerMain}>
          <ImageStart Images={require('../../../Assets/location1.png')}></ImageStart>

          <View style={styles.mainbtn}>
            <View style={styles.main1}>



              <View style={styles.iosdrop4}>
                <DropDownPicker
                  items={[
                    { label: 'UK', value: 'uk', },
                    { label: 'France', value: 'france' },
                  ]}
                  placeholder="Select your City"
                  zIndex={20}
                  arrowSize={wp('6%')}
                  itemStyle={styles.itemdrop}
                  labelStyle={{
                    fontSize: wp('4%'),
                    color: 'black',
                  }}

                  dropDownStyle={{ width: wp('80%'), alignSelf: 'center', backgroundColor: '#F4F5F7', borderTopEndRadius: 10, borderTopRightRadius: 10, borderTopLeftRadius: 10, borderBottomEndRadius: 10, borderBottomLeftRadius: 10, marginTop: hp('2%') }}

                  style={{ width: wp('80%'), alignSelf: 'center', marginTop: 7, borderBottomEndRadius: 10, backgroundColor: '#F4F5F7', borderTopRightRadius: 25, borderTopLeftRadius: 25, borderBottomLeftRadius: 25, borderBottomEndRadius: 25, borderBottomRightRadius: 25 }}

                  onChangeItem={item => setpick1({
                    pick1: item.value
                  })}
                  selectedLabelStyle={{ fontSize: wp('4%') }}
                />
              </View>

              <View style={styles.iosdrop5}>
                <DropDownPicker
                  items={[
                    { label: 'UK', value: 'uk', },
                    { label: 'France', value: 'france' },
                  ]}
                  zIndex={10}
                  placeholder="Select your Area "
                  arrowSize={wp('6%')}

                  itemStyle={styles.itemdrop}
                  style={{ width: wp('80%'), alignSelf: 'center', marginTop: 7, borderBottomEndRadius: 10, backgroundColor: '#F4F5F7', borderTopRightRadius: 25, borderTopLeftRadius: 25, borderBottomLeftRadius: 25, borderBottomEndRadius: 25, borderBottomRightRadius: 25 }}
                  dropDownStyle={{ width: wp('80%'), alignSelf: 'center', backgroundColor: '#F4F5F7', borderTopEndRadius: 10, borderTopRightRadius: 10, borderTopLeftRadius: 10, borderBottomEndRadius: 10, borderBottomLeftRadius: 10, marginTop: hp('2%') }}
                  labelStyle={{
                    fontSize: wp('4%'),
                    color: 'black',
                  }}
                  onChangeItem={item => setpick({
                    pick: item.value
                  })}
                  selectedLabelStyle={{ fontSize: wp('4%') }}
                />
              </View>

              {/* <TouchableOpacity onPress={() => Nextview()}>
                                
                                <View style={styles.mainbtn1}>

                                    <View style={styles.btnview}>
                                        <Text style={styles.textbtn}>Continue</Text>

                                    </View>


                                </View>

                            </TouchableOpacity> */}
              <CommonButton navigating={() => Nextview()} props={"Continue"}></CommonButton>


            </View>



          </View>
        </View>
      </ScrollView>
    </View>
  );

};


export default SelectLocation;

