import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../Utility/index';
import constants from '../../Constants/colors';


const styles = StyleSheet.create({
    main:
        { flex: 1, backgroundColor: constants.backgroundDull},

    imageview:
    {
        width: wp('30%'),
        marginTop: hp('4%'),
        alignSelf: "center",
        alignItems: "center",
        backgroundColor:"#FFFFFF",
        elevation:10,
        padding: wp("1%"),
       


    },
    rating:
    { fontSize: wp("4%") },
    row1:
    {
        flexDirection: "row",
        // width: wp("95%"),
        // justifyContent: "space-between",
        alignSelf: "center",
        padding:wp("1%")
    },
    row2:
    { flexDirection: "column",
     width: wp("15%") 
    },
     row3:
     { flexDirection: "row", 
    //  width: wp("10%"), 
     alignSelf:"flex-end",

     alignItems: "center" },
     col2:
     { flexDirection: "column",
      width: wp("10%") },
    col1:
    {
        flexDirection: "row",
        backgroundColor: "white",
        width: wp("25%"),
        alignSelf:"center",
        alignItems: "center",


    },
    Text2: {
        color: "grey",
        fontSize: wp("4%"),
        textAlign: "center",
        justifyContent: "center",
        alignItems: "center",
    },
    Text1: {
        color: "black",

        marginTop: hp("3%"),
        fontSize: wp("4.8%"),
        fontWeight: "bold",
        textAlign: "center",
        justifyContent: "flex-start",
    },
    img:
        { borderRadius: 100 },

});
export default styles;
