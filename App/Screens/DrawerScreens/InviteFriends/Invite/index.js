import React, { useEffect, useState } from 'react';
import { View, Text, ImageBackground, Image, Switch, TouchableOpacity, FlatList, TextInput } from 'react-native';
import Mainheader from '../../../../Components/Mainheader';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../../../Utility/index';
import constants from '../../../../Constants/colors';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import CommonButton from '../../../../Components/Button'
import styles from './style';
import { ScrollView } from 'react-native';
import {Input, Button, CheckBox} from 'react-native-elements';
import { Avatar } from 'react-native-paper';


const Invite = ({ navigation }) => {

    const [Search, SetSearch] = useState('');

    const [isSelected1, setSelection1] = useState(false);
    const [isSelected2, setSelection2] = useState(false);
    const [isSelected3, setSelection3] = useState(false);


    return (
        <View style={styles.mainview}>

            <Mainheader navigation={navigation} navigating={() => navigation.goBack()} props='Invite Friends' image={"arrow-back"}></Mainheader>

            <ScrollView>

                <View style={styles.btnview}>
                    <View style={styles.rowsearch}>
                        <View style={styles.col1}>
                            <MaterialIcons
                                name="search" size={wp("6%")} color={constants.greyesh} resizeMode="contain" />
                        </View>
                        <View style={styles.col2}>
                            <TextInput
                                style={{ fontSize: wp("4%"), color: constants.greyesh }}
                                placeholder="Search"
                                placeholderTextColor={constants.greyesh}
                                onChangeText={Search => SetSearch(Search)}

                            />
                        </View>
                        <TouchableOpacity>

                        <View style={styles.col3}>
                            <MaterialIcons
                                name="mic" size={wp("6%")} color={constants.greyesh} resizeMode="contain" />
                        </View>
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={styles.main}>
                    <View style={styles.imgview}>
                        {/* <Image source={require('../../../../Assets/Oval.png')} style={styles.img} resizeMode="contain" ></Image> */}
                        <Avatar.Image style={styles.img} size={wp("19%")} source={{ uri: 'https://i.stack.imgur.com/uoVWQ.png' }} />
                                                          
                    </View>
                    <View style={styles.column}>

                        <View style={{}}>
                            <Text style={styles.km}>Nilei Smith</Text>
                        </View>
                        <View style={styles.imgmain}>
                            <Text style={styles.km}>5 mutual friends</Text>

                        </View>

                    </View>
                    <View style={{width:wp("10%"),}}>

                      <CheckBox
                  checked={isSelected1}

                  checkedColor={constants.checkgreen}
                  size={wp("6%")}
                  onPress={() => setSelection1(!isSelected1)}
                />
                </View>
                </View>
                <View style={styles.main}>
                    <View style={styles.imgview}>
                    <Avatar.Image style={styles.img} size={wp("19%")} source={{ uri: 'https://i.stack.imgur.com/uoVWQ.png' }} />
                   
                    </View>
                    <View style={styles.column}>

                            <Text style={styles.km}>Nilei Smith</Text>
                        <View style={styles.imgmain}>
                            <Text style={styles.km}>5 mutual friends</Text>

                        </View>

                    </View>
                    <View style={{width:wp("10%"),}}>
                  
                      <CheckBox
                  checked={isSelected2}
                 

                  checkedColor={constants.checkgreen}
                  size={wp("6%")}
                  onPress={() => setSelection2(!isSelected2)}
                />
                </View>
                </View>
                
                <View style={styles.main}>
                    <View style={styles.imgview}>
                    <Avatar.Image style={styles.img} size={wp("19%")} source={{ uri: 'https://i.stack.imgur.com/uoVWQ.png' }} />

                    </View>
                    <View style={styles.column}>

                        <View style={{}}>
                            <Text style={styles.km}>Nilei Smith</Text>
                        </View>
                        <View style={styles.imgmain}>
                            <Text style={styles.km}>5 mutual friends</Text>

                        </View>

                    </View>
                    <View style={{width:wp("10%"),}}>
                  
                      <CheckBox
                  checked={isSelected3}
                  checkedColor={constants.checkgreen}

                  size={wp("6%")}
                  onPress={() => setSelection3(!isSelected3)}
                />
                </View>
                </View>
                <CommonButton props={"Next"}></CommonButton>

            </ScrollView>

        </View>


    );
};

export default Invite;
