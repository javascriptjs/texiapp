import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../Utility/index';

const iconStyles = {
    size: 100,
    color: '#FFFFFF',
};

const styles = StyleSheet.create({
    mainhd:
        { flex: 1, backgroundColor: 'white' },
        containerMain: {
            flex: 1,
           
        },
        bottomView: {
            width: '100%',
            height: "45%",
    
            backgroundColor: '#FFFFFF',
            elevation: 10,
    
            // justifyContent: 'center',
            alignItems: 'center',
            borderTopRightRadius: 30,
            borderTopLeftRadius: 30,
            position: 'absolute',
            bottom: 0,
    
    
    
        },
        hour:
        {color:'white',fontSize:wp('3%'),fontWeight:'700'},
        time:
        {color:'white',fontSize:wp('4%'),fontWeight:'700'},
        clockimg:
        { width: wp('10%'), height: hp('10%') },
        clock:
        {backgroundColor:'#FFBC00',width:wp('28%'),alignItems:'center',borderRadius:10,padding:wp('3%'),},
        clock2:
        {backgroundColor:'#FF5645',width:wp('28%'),alignItems:'center',borderRadius:10,padding:wp('3%'),},
        clock3:
        {backgroundColor:'#3A56FF',width:wp('28%'),alignItems:'center',borderRadius:10,padding:wp('3%'),},
        width:
        { flexDirection: 'row', width: wp('90%'),justifyContent:'space-between', marginTop: hp('2%'),marginBottom:hp('5%') },
        earned:
        { fontSize: wp('3.5%'), color: '#BEC2CE' },
        number:
        { fontSize: wp('4%'), fontWeight: '700' },
        logic:
        { color: 'black', left: wp('1%'), fontWeight: '700' ,fontSize:wp('3.5%')},
        star:
        { flexDirection: 'row', alignItems: 'center', width: wp('30%') },
        name2:
        { fontSize: wp('3.5%'), color: '#BEC2CE' },
        name:
        { fontSize: wp('4%'), fontWeight: '700' },
        mainview:
        { flexDirection: 'column', width: wp('50%') },
        oval:
        { width: wp('15%'), height: hp('10%') },
        first:
        { flexDirection: 'row', width: wp('90%'), justifyContent: 'space-between', marginTop: hp('5%') },
   img:
   { flexDirection: 'column', width: wp('20%'), justifyContent: 'flex-start' },
        main1:
        {
            alignSelf: "center",
        },
        container: {
            flex: 1,
            backgroundColor: 'white',
            padding: 10,
            justifyContent: 'center',
            textAlign: 'center',
          },
          titleText: {
            padding: 8,
            fontSize: 16,
            textAlign: 'center',
            fontWeight: 'bold',
          },
          textStyle: {
            textAlign: 'center',
            fontSize: 23,
            color: '#000',
            marginTop: 15,
          },
          textStyleSmall: {
            textAlign: 'center',
            fontSize: 16,
            color: '#000',
            marginTop: 15,
          },
          buttonStyle: {
            justifyContent: 'center',
            flexDirection: 'row',
            marginTop: 30,
            padding: 15,
            backgroundColor: '#8ad24e',
          },
          buttonTextStyle: {
            color: '#fff',
            textAlign: 'center',
          },
          customRatingBarStyle: {
            // justifyContent: 'center',
            flexDirection: 'row',
          },
          starImageStyle: {
            width: wp('4%'),
            height: hp('4%'),
            // resizeMode: 'cover',
          },
        //   visibilityBtn:
        //   {
        //     //   position: 'absolute',
        //       right: 3,
        //     //   height: 40,
        //     //   width: 35,
        //       padding: 5
        //   },
     
});
export default styles;
