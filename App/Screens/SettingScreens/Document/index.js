import React, { useEffect } from 'react';
import { useState } from 'react';
import { View, Text, ImageBackground, Image, Switch, TouchableOpacity } from 'react-native';
import Mainheader from '../../../Components/Mainheader';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from '../../../Utility';
import Icon from 'react-native-vector-icons/AntDesign';
import MapboxGL from '@react-native-mapbox-gl/maps';
import styles from "./style"
import constants from '../../../Constants/colors';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

const Document = ({ navigation }) => {
    

 
    return (

        <View style={{ flex: 1, backgroundColor: constants.backgroundDull}}>
            <Mainheader navigation={navigation} navigating={() => navigation.goBack()} props='Add Vehicle' image={"arrow-back"}></Mainheader>
            <View style={{ width: wp("90%"), alignSelf: "center", }}>
                <View style={{ width: wp("70%"), marginTop: hp("4%") ,height: hp("18%"), backgroundColor: constants.document, alignSelf: "center", justifyContent: "center" }}>
                    <View style={{ alignItems: "center", }}>
                        <FontAwesome name={"user"} size={wp("20%")} color={constants.white} />
                    </View>
                </View>
            <View style={styles.info}>
                <Text style={styles.infoview}>National Identification Cards</Text>
            </View>

            <View style={{ width: wp("70%"), marginTop: hp("4%") ,height: hp("18%"), backgroundColor: constants.document, alignSelf: "center", justifyContent: "center" }}>
                  <TouchableOpacity onPress={()=>navigation.navigate("License")}>
                    <View style={{ alignItems: "center", }}>
                        <FontAwesome name={"user"} size={wp("20%")} color={constants.white} />
                 
                    </View>
                    </TouchableOpacity>
                </View>
            <View style={styles.info}>
                <Text style={styles.infoview}>Driving license</Text>
            </View>
            </View>

        </View>
    );
};

export default Document;
