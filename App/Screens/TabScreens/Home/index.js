import React, { useEffect ,useState} from 'react';
import { View, Text, ImageBackground, Image,Switch,Linking } from 'react-native';
import Mainheader from '../../../Components/Mainheader';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from '../../../Utility';
import Icon from 'react-native-vector-icons/AntDesign';
import styles from './style';
import { TouchableOpacity } from 'react-native-gesture-handler';
const Home = ({ navigation }) => {
    const [toggle, setToggle] = useState(true);
    const openDialScreen = () => {
        let number = '';
        if (Platform.OS === 'ios') {
          number = 'telprompt:091123456789';
        } else {
          number = 'tel:091123456789';
        }
        Linking.openURL(number);
      };
  return (

    <View style={{}}>
             <Mainheader navigation={navigation} navigating={() => navigation.openDrawer()}  image={'menu'} props='Online' toggleicon={
                <Switch
                    trackColor={{ false: '#d3d3d3', true: '#d3d3d3', }}
                    thumbColor="#3A56FF"

                    ios_backgroundColor="gray"
                    onValueChange={(value) => setToggle(value)}
                    value={toggle}
                />}
            ></Mainheader>
       
   
          <View style={styles.main1}>
        <Image source={{ uri: "https://www.hayalanka.com/wp-content/uploads/2017/07/avtar-image.jpg" }} style={{ width: wp('10%'), height: hp('6%'), borderRadius: 20 }}></Image>
        <View style={{ width: wp('60%'), paddingLeft: wp('4%') }}>

          <Text style={{ fontSize: wp("4%"), fontWeight: "bold" }}>Jhon Smith</Text>
          <Text style={{ color: "grey", fontSize: wp("3.5%") }}>2.2 km</Text>

        </View>
        <Text style={{ fontSize: wp("4%"), fontWeight: "bold" }}>$20.00</Text>

      </View>
      <View style={{ width: wp('90%'), alignSelf: "center",}}>
      <View style={styles.main2}>

        <Text style={{ fontSize: wp("3.5%"), color: "grey" }}>PICK UP</Text>
        <Text style={{ fontSize: wp("4%"), fontWeight: "400" }}>969 Us  West, Demopolis AL 36732.</Text>
        </View>
      <View style={styles.main2}>

        <Text style={{ fontSize: wp("3.5%"), color: "grey", }}>DROP OFF</Text>
        <Text style={{ fontSize: wp("4%"), fontWeight: "400" }}>969 Us  West, Demopolis AL 36732.</Text>
       </View>
      <View style={styles.main2}>

        <Text style={{ fontSize: wp("3.5%"), color: "grey", }}>TRIP FARE</Text>
        <Text style={{ fontSize: wp("4%"), fontWeight: "400", }}>Discount            …………………………………. $30</Text>
        <Text style={{ fontSize: wp("4%"), fontWeight: "400" }}>Paid amount     .………………………………… $20</Text>
</View>
      </View>


  

         <View style={{flexDirection:"row",width:wp('90%'),justifyContent:"space-between",alignSelf:"center",marginTop:hp('5%')}}>

             <View style={{width:wp('28%'),height:hp('10%'),borderRadius:10,backgroundColor:"#FF5645",alignItems:"center",justifyContent:"center"}}>
             <Icon name="delete" size={20} color="white" />
         <Text style={{fontSize:14,fontWeight:"400",color:'white'}}>Delete</Text>

             </View>


             <View style={{width:wp('28%'),height:hp('10%'),borderRadius:10,backgroundColor:"#3A56FF",alignItems:"center",justifyContent:"center"}}>
             <Icon name="message1" size={20} color="white" />
         <Text style={{fontSize:14,fontWeight:"400",color:'white'}}>Message</Text>

             </View>
<TouchableOpacity onPress={() => openDialScreen()}>
             <View  style={{width:wp('28%'),height:hp('10%'),borderRadius:10,backgroundColor:"#36B789",alignItems:"center",justifyContent:"center"}}>
             <Icon name="phone" size={20} color="white" />
         <Text style={{fontSize:14,fontWeight:"400",color:'white'}}>Phone</Text>

             </View>
             </TouchableOpacity>
         </View>
    </View>
  );
};

export default Home;
