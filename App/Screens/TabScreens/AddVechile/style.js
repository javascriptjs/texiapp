import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../../Utility/index';
import constants from '../../../Constants/colors';

const iconStyles = {
    size: 100,
    color: '#FFFFFF',
};

const styles = StyleSheet.create({
main:
{ flex: 1 ,backgroundColor:constants.backgroundDull},
row1:
{ flexDirection: "row",    backgroundColor:constants.white,elevation:10, paddingTop: hp("2%"),
        paddingBottom: hp("2%"),
        paddingLeft: wp("2%"),width: wp('90%'), marginTop: hp('4%'), alignSelf: "center" },
        col:
        { width: wp('60%'),flexDirection:"row"},
        col1:
        { width: wp('60%'),flexDirection:"column"},
        text:
        { fontSize: wp("4%"), fontWeight: "bold" },
        text2:
        { color: "grey",fontSize: wp("3.5%") },
        img:
        {width:wp("15%"),alignSelf:"center",alignItems:"center", flexDirection:"column"},
btnview2:
{
  backgroundColor: "#3A56FF",
  marginTop: hp("15%"),
  borderRadius: 25,



  flexDirection: "row",
  justifyContent: "center",
  alignItems: "center",
  alignSelf: "center",
  width: wp("85%"),
  height: hp("7.5%"),
},
 
        textbtn:
        {
            color: "white",
            fontSize: wp("4%"),
            fontWeight:'500'
        },
        marginview1:
        { marginBottom:wp("10%")},

});
export default styles;
