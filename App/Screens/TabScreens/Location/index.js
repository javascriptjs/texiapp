import React, { useEffect } from 'react';
import { useState } from 'react';
import { View, Text, ImageBackground, Image,Switch } from 'react-native';
import MenuHeader from '../../../Components/MenuHeader';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from '../../../Utility';
import Icon from 'react-native-vector-icons/AntDesign';
import MapboxGL from '@react-native-mapbox-gl/maps';
import styles from "./style"
MapboxGL.setAccessToken(
  'pk.eyJ1IjoibmFuY3liaGFyZHdhaiIsImEiOiJja3A4N3BidGgwMHlpMndxbWN6Njk0YWNqIn0.dK2nmy8bRcSEpziySo0Ajw',
);
const Location = ({ navigation }) => {
    const [toggle, setToggle] = useState(true);
    const [coordinates] = useState([-73.99155, 40.73581]);
  
  return (

    <View style={{flex:1}}>
            
              <MenuHeader  navigation={navigation}  navigating={() => navigation.openDrawer()} image={'menu'} props='Online' toggleicon={
                <Switch
                    trackColor={{ false: '#d3d3d3', true: '#d3d3d3', }}
                    thumbColor="#3A56FF"

                    ios_backgroundColor="gray"
                    onValueChange={(value) => setToggle(value)}
                    value={toggle}
                />}
            ></MenuHeader>
        <MapboxGL.MapView style={styles.map}>
                <MapboxGL.Camera zoomLevel={8} centerCoordinate={coordinates} />
                <MapboxGL.PointAnnotation coordinate={coordinates} id="Test" />


            </MapboxGL.MapView>
   
    </View>
  );
};

export default Location;
