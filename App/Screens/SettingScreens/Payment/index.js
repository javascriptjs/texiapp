import React, { useEffect } from 'react';
import { useState } from 'react';
import { ScrollView } from 'react-native';
import { View, Text, ImageBackground, Image, Switch, TextInput, TouchableOpacity } from 'react-native';
import Mainheader from '../../../Components/Mainheader';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../../Utility/index';
import ImageStart from '../../../Components/ImageStart';
import Mainheading from '../../../Components/Mainheading';
import Subheading from '../../../Components/Subheading';
import CommonButton from '../../../Components/Button'
import YellowBtn from '../../../Components/yellowbtn'
import constants from '../../../Constants/colors';
import RadioButton from 'react-native-radio-button';
import TextInputing from '../../../Components/TextInputing'
import styles from './style';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

const Payment = ({ navigation }) => {


    const [isSelected1, setSelection1] = useState(false);
    const [isSelected2, setSelection2] = useState(false);
    const [isSelected3, setSelection3] = useState(false);
    const Nextview = () => {
        navigation.navigate("PaymentMethod")
    }

    return (
        <View style={{ flex: 1, backgroundColor: constants.backgroundDull }}>

            <Mainheader navigation={navigation} navigating={() => navigation.goBack()} props='Payment Method' image={"arrow-back"}></Mainheader>
            <ScrollView>
                <View style={styles.containerMain}>
                    <Image source={require('../../../Assets/swipe2.png')} style={styles.img} resizeMode="contain" ></Image>
                    <View style={{ marginTop: hp("4%") }}>
                        {/* <View style={{ flexDirection: "row", width: wp("90%"), alignSelf: "center", backgroundColor: "pink" }}>
                            <View style={{ flexDirection: "column", width: wp("60%") }}>
                                <Text style={{ fontSize: wp("4%"), color: constants.black_Text }}>Visa **********250</Text>
                            </View>
                            <View style={{ flexDirection: "column", width: wp("30%") }}>
                                <Text style={{ fontSize: wp("4%"), color: constants.black_Text }}>Change</Text>
                            </View>
                        </View> */}
                        <View style={styles.row1} >


                            <View style={{ width: wp('60%'), paddingLeft: wp('4%') }}>

                                <Text style={{ fontSize: wp("4%"), color: constants.black_Text }}>Visa **********250</Text>

                            </View>
                            <View style={styles.iconarrow2}>
                                <TouchableOpacity>
                                    <Text style={{ fontSize: wp("4%"), color: constants.pink }}>Change</Text>
                                </TouchableOpacity>
                                {/* <MaterialIcons name="arrow-forward-ios" size={wp("6%")} color={constants.blackicon} /> */}
                            </View>
                        </View>


                        <View style={styles.row1} >

                            <View style={styles.imgview}>
                                <RadioButton
                                    animation={'bounceIn'}
                                    size={wp("3%")}
                                    isSelected={isSelected1}
                                    outerColor={constants.bluebtn}
                                    innerColor={constants.bluebtn}
                                    onPress={() => setSelection1(!isSelected1)}
                                />
                            </View>
                            <View style={styles.textview}>

                                <Text style={styles.text}>Credit / Debit / ATM Card</Text>

                            </View>
                            <View style={styles.iconarrow}>
                                <TouchableOpacity onPress={() => navigation.navigate("StripePayment")}>
                                    <MaterialIcons name="arrow-forward-ios" size={wp("6%")} color={constants.blackicon} />
                                </TouchableOpacity>
                            </View>
                        </View>

                        <View style={styles.row1} >

                            <View style={styles.imgview}>
                                <RadioButton
                                    animation={'bounceIn'}
                                    size={wp("3%")}
                                    isSelected={isSelected2}
                                    outerColor={constants.bluebtn}
                                    innerColor={constants.bluebtn}
                                    onPress={() => setSelection2(!isSelected2)}
                                />

                            </View>
                            <View style={styles.textview}>

                                <Text style={styles.text}>Cash on  Payment</Text>

                            </View>
                            <View style={styles.iconarrow}>
                                <TouchableOpacity>
                                    <MaterialIcons name="arrow-forward-ios" size={wp("6%")} color={constants.blackicon} />
                                </TouchableOpacity>
                            </View>
                        </View>

                     
                        <View style={styles.row1} >

                            <View style={styles.imgview}>
                                <MaterialIcons name="credit-card" size={wp("6%")} style={{ alignSelf: "center" }} color={constants.bluebtn} />



                            </View>
                            <View style={styles.textview}>

                                <Text style={styles.text}>Add a new card</Text>

                            </View>
                            <View style={styles.iconarrow}>
                                <TouchableOpacity>

                                    <MaterialIcons name="arrow-forward-ios" size={wp("6%")} color={constants.blackicon} />
                                </TouchableOpacity>

                            </View>
                        </View>
                        <CommonButton navigating={() => Nextview()} props={"Process to Checkout"}></CommonButton>

                    </View>
                </View>

            </ScrollView>
        </View>

    );
};

export default Payment;
