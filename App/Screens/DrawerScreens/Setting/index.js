import React, { useEffect,useState } from 'react';
import { View, Text, ImageBackground, Image, Switch, TouchableOpacity,Linking,Alert } from 'react-native';
import Mainheader from '../../../Components/Mainheader';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from '../../../Utility';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons'
import styles from './style';
import constants from '../../../Constants/colors';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import Feather from 'react-native-vector-icons/Feather';
import { ScrollView } from 'react-native';
const Setting = ({ navigation }) => {
    const [toggle, setToggle] = useState(false);
    const [toggle2, setToggle2] = useState(false);
    const [toggle3, setToggle3] = useState(false);
      
        const SignOut =()=>{
            Alert.alert(
                '',
    
                'Are you sure want to sign out?',
                [
                    {
                        text: 'Cancel',
                        onPress: () => console.log('Cancel Button Pressed'),
                        style: 'cancel',
                    },
    
                    { text: 'Sign out', onPress: () => Signout_Function() },
                ],
            );
        }
        const Signout_Function = () => {
            navigation.navigate("SignIn")
        }
    return (

        <View style={{}}>
            <Mainheader navigation={navigation} navigating={() => navigation.goBack()} props='Settings' image={"arrow-back"}></Mainheader>
            <ScrollView style={{ marginBottom: "20%" }}>
                <View>
                    <View style={{ flexDirection: "row", width: wp('90%'),marginTop: hp('4%'), alignSelf: "center", }}>
                        <Image source={{ uri: "https://www.hayalanka.com/wp-content/uploads/2017/07/avtar-image.jpg" }} style={{ width: wp('10%'), height: hp('6%'), borderRadius: 20 ,}}></Image>
                        <View style={{ width: wp('70%'), paddingLeft: wp('4%') }}>

                            <Text style={{ fontSize: wp("4%"), fontWeight: "bold" }}>Jhon Smith</Text>
                            <Text style={{ color: "grey", fontSize: wp("3.5%") }}>Basic Member</Text>

                        </View>
                        <View style={{ width: wp('10%'), alignItems: "center", justifyContent: "center" }}>

                            <MaterialIcons name="arrow-forward-ios" size={20} color={constants.grey} />
                        </View>

                    </View>
                    <Text style={{ backgroundColor: constants.bluebtn, fontSize: wp("4.5%"), fontWeight: "700", color: constants.white, width: wp("90%"), alignSelf: 'center', marginTop: hp('4%'), padding: wp("3%"), borderTopLeftRadius: 20, borderTopRightRadius: 20 }}>Accounts</Text>
                    <View style={{ fontSize: wp("5%"), fontWeight: "700", color: constants.white, width: wp("90%"), alignSelf: 'center', }}>
                        
                        <TouchableOpacity style={styles.row1} onPress={()=>navigation.navigate("ChangePasswd")}>
                            <View style={styles.imgview}>
                                <FontAwesome5 name="lock" size={20} style={{}} color={constants.blackicon} />
                            </View>
                            <View style={styles.textview}>

                                <Text style={styles.text}>Change Password</Text>

                            </View>
                            <View style={styles.iconarrow}>

                                <MaterialIcons name="arrow-forward-ios" size={wp("6%")} color={constants.blackicon} />
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.row1} onPress={()=>navigation.navigate("AddVechile")}>
                            <View style={styles.imgview}>
                                <MaterialCommunityIcons name="bell-ring" size={wp("6%")} style={{}} color={constants.blackicon} />

                            </View>
                            <View style={styles.textview}>

                                <Text style={styles.text}>Vehicle Managent</Text>

                            </View>
                            <View style={styles.iconarrow}>

                                <MaterialIcons name="arrow-forward-ios" size={wp("6%")} color={constants.blackicon} />
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.row1} onPress={()=>navigation.navigate("Document")}>
                            <View style={styles.imgview}>
                                <MaterialIcons name="settings" size={wp("6%")} style={{}} color={constants.blackicon} />

                            </View>
                            <View style={styles.textview}>

                                <Text style={styles.text}>Document Management</Text>

                            </View>
                            <View style={styles.iconarrow}>

                                <MaterialIcons name="arrow-forward-ios" size={wp("6%")} color={constants.blackicon} />
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.row1} onPress={()=>navigation.navigate("Payment")}>
                            <View style={styles.imgview}>
                                <MaterialIcons name="payment" size={wp("6%")} style={{}} color={constants.blackicon} />

                            </View>
                            <View style={styles.textview}>

                                <Text style={styles.text}>Payment</Text>

                            </View>
                            <View style={styles.iconarrow}>

                                <MaterialIcons name="arrow-forward-ios" size={wp("6%")} color={constants.blackicon} />
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.row1} onPress={() => SignOut()}>

                            <View style={styles.imgview}>
                                <SimpleLineIcons name="logout" size={wp("6%")} style={{}} color={constants.blackicon} />

                            </View>
                            <View style={styles.textview}>

                                <Text style={styles.text}>Sign Out</Text>

                            </View>
                            <View style={styles.iconarrow}>

                                <MaterialIcons name="arrow-forward-ios" size={wp("6%")} color={constants.blackicon} />
                            </View>
                        </TouchableOpacity>
                    </View>
                    <Text style={{ fontWeight: "700", color: constants.bluebtn, width: wp("90%"), alignSelf: 'center', marginTop: hp('4%'), padding: wp("3%"), fontSize: wp("4.5%") }}>More Options</Text>


                    <View style={styles.row1}>
                        <View style={styles.imgview}>
                            <MaterialCommunityIcons name="email-newsletter" size={wp("6%")} style={{}} color={constants.blackicon} />

                        </View>
                        <View style={styles.textview}>

                            <Text style={styles.text}>Newsletter</Text>

                        </View>
                        <View style={styles.iconarrow}>
                            <Switch
                                trackColor={{ false: '#d3d3d3', true: '#d3d3d3', }}
                                thumbColor="#3A56FF"

                                ios_backgroundColor="gray"
                                onValueChange={(value) => setToggle(value)}
                                value={toggle}
                            />
                            {/* <MaterialIcons name="arrow-forward-ios" size={20} color={constants.blackicon} /> */}
                        </View>
                    </View>
<TouchableOpacity onPress={()=>navigation.navigate("ChatModule")}>
                    <View style={styles.row1}>
                        <View style={styles.imgview}>
                            <MaterialCommunityIcons name="email" size={wp("6%")} style={{}} color={constants.blackicon} />

                        </View>
                        <View style={styles.textview}>

                            <Text style={styles.text}>Text Message</Text>

                        </View>
                        <View style={styles.iconarrow}>
                            <Switch
                                trackColor={{ false: '#d3d3d3', true: '#d3d3d3', }}
                                thumbColor="#3A56FF"

                                ios_backgroundColor="gray"
                                onValueChange={(value2) => setToggle2(value2)}
                                value={toggle2}
                            />
                        </View>
                    </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.row1} >
                        <View style={styles.imgview}>
                            <Feather name="phone-call" size={wp("6%")} style={{}} color={constants.blackicon} />

                        </View>
                        <View style={styles.textview}>

                            <Text style={styles.text}>Phone Call</Text>

                        </View>
                        <View style={styles.iconarrow}>
                            <Switch
                                trackColor={{ false: '#d3d3d3', true: '#d3d3d3', }}
                                thumbColor="#3A56FF"

                                ios_backgroundColor="gray"
                                onValueChange={(value3) => setToggle3(value3)}
                                value={toggle3}
                            />
                        </View>
                    </TouchableOpacity>

                    <View style={styles.row1}>
                        <View style={styles.imgview}>
                            <Image source={require('../../../Assets/dollar.png')} style={{ width: wp('6%'), height: hp('4%') }} resizeMode="contain" ></Image>


                        </View>
                        <View style={styles.textview1}>

                            <Text style={styles.text}>Currency</Text>

                        </View>
                        <View style={styles.textview2}>
                            <Text style={styles.text1}>$USD</Text>

                        </View>
                        <View style={styles.iconarrow1}>

                            <MaterialIcons name="arrow-forward-ios" size={wp("6%")} color={constants.blackicon} />
                        </View>
                    </View>
                    <View style={styles.row1}>
                        <View style={styles.imgview}>
                            <FontAwesome5 name="language" size={wp("6%")} style={{}} color={constants.blackicon} />



                        </View>
                        <View style={styles.textview1}>

                            <Text style={styles.text}>language</Text>

                        </View>
                        <View style={styles.textview2}>
                            <Text style={styles.text1}>English</Text>

                        </View>
                        <View style={styles.iconarrow1}>

                            <MaterialIcons name="arrow-forward-ios" size={wp("6%")} color={constants.blackicon} />
                        </View>
                    </View>

                    <View style={styles.row1}>
                        <View style={styles.imgview}>
                            <MaterialCommunityIcons name="link-variant" size={wp("6%")} style={{}} color={constants.blackicon} />


                        </View>
                        <View style={styles.textview1}>

                            <Text style={styles.text}>Linked Accounts</Text>

                        </View>
                        <View style={styles.textview2}>
                            <Text style={styles.text1}>Facebook</Text>

                        </View>
                        <View style={styles.iconarrow1}>

                            <MaterialIcons name="arrow-forward-ios" size={wp("6%")} color={constants.blackicon} />
                        </View>
                    </View>

                </View>
            </ScrollView>
        </View>
    );
};

export default Setting;
