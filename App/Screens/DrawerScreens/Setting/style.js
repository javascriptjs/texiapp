import { Dimensions, StyleSheet, Platform } from 'react-native';
const window = Dimensions.get('window');
import constants from '../../../Constants/colors';

import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from '../../../Utility/index';
const { strings, colors, fonts, urls, PATH } = constants;
const styles = StyleSheet.create({

    mainbtn1:
    {
        marginBottom: "10%", alignSelf: "center"
    },
    btnview:
    {
        backgroundColor: "#3A56FF",
        marginTop: hp("5%"),
        borderRadius: 50,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        alignSelf: "center",

        width: wp("80%"),
        height: hp("7.5%"),
    },

    textbtn:
    {
        color: "white",
        // fontWeight: "bold",
        fontSize: wp("4%")
    },
    row1:
        { flexDirection: "row", width: wp('90%'), marginTop: hp('4%'), alignSelf: "center", alignItems: "center" },
    imgview:
        { width: wp('10%'), },
    textview:
        { width: wp('70%'), paddingLeft: wp('4%') },
    text:
    {
        color: constants.blackicon, fontSize: wp("4%"),
    },
    text1:
    {
        color: constants.greyesh, fontSize: wp("4%"),alignSelf:"flex-end"
    },
    iconarrow:
        { width: wp('10%'), alignItems: "center", justifyContent: "center" },
        textview1:
        { width: wp('40%'), paddingLeft: wp('4%') },
        textview2:
        { width: wp('30%'), paddingLeft: wp('0%'),},
});

export default styles;
