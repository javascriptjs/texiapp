
import React, { useState } from 'react'
import { View, Text, ImageBackground, Image, StyleSheet, Alert } from 'react-native';
import constants from '../Constants/colors';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons'
import { TouchableOpacity } from 'react-native-gesture-handler';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../Utility/index";
import Navigatingtext from "../Components/Navigatingtext";
const DrawerBg = ({ navigation }) => {
    const SignOut = () => {
        Alert.alert(
            '',

            'Are you sure want to sign out?',
            [
                {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Button Pressed'),
                    style: 'cancel',
                },

                { text: 'Sign out', onPress: () => Signout_Function() },
            ],
        );
    }
    const Signout_Function = () => {
        navigation.navigate("SignIn")
    }
    return (
        <View style={{ flex: 1 }}>

            <View style={styles.first}>
                <View style={styles.img}>
                    <Image source={require('../Assets/Oval.png')} style={styles.oval} resizeMode="contain" ></Image>
                </View>
                <View style={styles.mainview}>
                    <Text style={styles.name}>Jhon Smith</Text>
                    <Text style={styles.name2}>Basic level</Text>
                    <View style={styles.star}>

                    </View>
                </View>

            </View>
            <View style={styles.width}>
                <View style={styles.clock}>
                    <Image source={require('../Assets/clock.png')} style={styles.clockimg} resizeMode="contain" >
                    </Image>
                    <Text style={styles.time}>20.30</Text>
                    <Text style={styles.hour}>Online Hours</Text>

                </View>
                <View style={styles.clock2}>
                    <Image source={require('../Assets/speed.png')} style={styles.clockimg} resizeMode="contain" >
                    </Image>
                    <Text style={styles.time}>700 KM</Text>
                    <Text style={styles.hour}>Total Ride</Text>

                </View>
                <View style={styles.clock3}>
                    <Image source={require('../Assets/business.png')} style={styles.clockimg} resizeMode="contain" >
                    </Image>
                    <Text style={styles.time}>200</Text>
                    <Text style={styles.hour}>Total Jobs</Text>

                </View>
            </View>
            <View style={{ fontSize: wp("5%"), fontWeight: "700", color: constants.white, width: wp("60%"), alignSelf: 'center', }}>
                <Navigatingtext Navigating={() => navigation.navigate('Home')} lefticon={
                    <MaterialIcons name="home" size={wp("6%")} style={{}} color={constants.blackicon} />
                }
                    prop={"Home"}></Navigatingtext>
                <Navigatingtext Navigating={() => navigation.navigate('MyWallet')} lefticon={
                    <FontAwesome5 name="wallet" size={wp("6%")} style={{}} color={constants.blackicon} />
                }
                    prop={"My Wallet"}></Navigatingtext>

                <Navigatingtext Navigating={() => navigation.navigate('History')} lefticon={
                    <Entypo name="back-in-time" size={wp("6%")} style={{}} color={constants.blackicon} />
                }
                    prop={"History"}></Navigatingtext>

                <Navigatingtext Navigating={() => navigation.navigate('FavouriteDriver')} lefticon={
                    <MaterialCommunityIcons name="bell-ring" size={wp("6%")} style={{}} color={constants.blackicon} />
                }
                    prop={"Notifications"}></Navigatingtext>
                <Navigatingtext Navigating={() => navigation.navigate('InviteFriends')} lefticon={
                    <MaterialCommunityIcons name="gift" size={wp("6%")} style={{}} color={constants.blackicon} />
                }
                    prop={"Invite Friends"}></Navigatingtext>
                <Navigatingtext Navigating={() => navigation.navigate('Setting')} lefticon={
                    <MaterialIcons name="settings" size={wp("6%")} style={{}} color={constants.blackicon} />
                }
                    prop={"Settings"}></Navigatingtext>


                <Navigatingtext Navigating={() => SignOut()} lefticon={
                    <MaterialIcons name="logout" size={wp("6%")} style={{}} color={constants.blackicon} />
                }
                    prop={"Sign Out"}></Navigatingtext>
            </View>

        </View>

    );

}
export default DrawerBg;

const styles = StyleSheet.create({
    mainhd:
        { flex: 1, backgroundColor: 'white' },
    containerMain: {
        flex: 1,

    },
    bottomView: {
        width: '100%',
        height: "45%",

        backgroundColor: '#FFFFFF',
        elevation: 10,

        // justifyContent: 'center',
        alignItems: 'center',
        borderTopRightRadius: 30,
        borderTopLeftRadius: 30,
        position: 'absolute',
        bottom: 0,



    },
    hour:
        { color: 'white', fontSize: wp('1.5%'), fontWeight: '700' },
    time:
        { color: 'white', fontSize: wp('1.5%'), fontWeight: '700' },
    clockimg:
        { width: wp('10%'), height: hp('10%') },
    clock:
        { backgroundColor: '#FFBC00', width: wp('18%'), alignItems: 'center', borderRadius: 10, padding: wp('3%'), },
    clock2:
        { backgroundColor: '#FF5645', width: wp('18%'), alignItems: 'center', borderRadius: 10, padding: wp('3%'), },
    clock3:
        { backgroundColor: '#3A56FF', width: wp('18%'), alignItems: 'center', borderRadius: 10, padding: wp('3%'), },
    width:
        { flexDirection: 'row', alignSelf: "center", width: wp('60%'), justifyContent: 'space-between', marginTop: hp('2%'), marginBottom: hp('5%') },
    earned:
        { fontSize: wp('3.5%'), color: '#BEC2CE' },
    number:
        { fontSize: wp('4%'), fontWeight: '700' },
    logic:
        { color: 'black', left: wp('1%'), fontWeight: '700', fontSize: wp('3.5%') },
    star:
        { flexDirection: 'row', alignItems: 'center', width: wp('30%') },
    name2:
        { fontSize: wp('3.5%'), color: '#BEC2CE' },
    name:
        { fontSize: wp('4%'), fontWeight: '700' },
    mainview:
        { flexDirection: 'column', width: wp('50%') },
    oval:
        { width: wp('15%'), height: hp('10%') },
    first:
        { flexDirection: 'row', width: wp('60%'), alignSelf: "center", marginTop: hp('5%'), alignItems: "center" },
    img:
        { flexDirection: 'column', width: wp('20%'), justifyContent: 'flex-start' },
    main1:
    {
        alignSelf: "center",
    },
    container: {
        flex: 1,
        backgroundColor: 'white',
        padding: 10,
        justifyContent: 'center',
        textAlign: 'center',
    },
    titleText: {
        padding: 8,
        fontSize: 16,
        textAlign: 'center',
        fontWeight: 'bold',
    },
    textStyle: {
        textAlign: 'center',
        fontSize: 23,
        color: '#000',
        marginTop: 15,
    },
    textStyleSmall: {
        textAlign: 'center',
        fontSize: 16,
        color: '#000',
        marginTop: 15,
    },
    buttonStyle: {
        justifyContent: 'center',
        flexDirection: 'row',
        marginTop: 30,
        padding: 15,
        backgroundColor: '#8ad24e',
    },
    buttonTextStyle: {
        color: '#fff',
        textAlign: 'center',
    },
    customRatingBarStyle: {
        // justifyContent: 'center',
        flexDirection: 'row',
    },
    starImageStyle: {
        width: wp('4%'),
        height: hp('4%'),
        // resizeMode: 'cover',
    },

    map: {
        flex: 1, height: '50%'
    },
    row1:
        { flexDirection: "row", width: wp('60%'), marginTop: hp('4%'), alignSelf: "center", alignItems: "center" },
    imgview:
        { width: wp('10%'), },
    textview:
        { width: wp('70%'), paddingLeft: wp('4%') },
    text:
        { color: "grey", fontSize: wp("3.5%") },
    iconarrow:
        { width: wp('10%'), alignItems: "center", justifyContent: "center" },

});
