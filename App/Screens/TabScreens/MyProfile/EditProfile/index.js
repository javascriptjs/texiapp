

import React, { useState, useRef, useEffect } from 'react';
import { SafeAreaView, StyleSheet, View, Text, Image, TouchableOpacity, Switch, ScrollView } from 'react-native';
import styles from './style';
import LinearGradient from 'react-native-linear-gradient';
import Mainheader from '../../../../Components/Mainheader';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../../../Utility/index';
import MapboxGL from '@react-native-mapbox-gl/maps';
import { Input, Button } from 'react-native-elements';
import constants from '../../../../Constants/colors';

import Mainheading from '../../../../Components/Mainheading';
import Subheading from '../../../../Components/Subheading';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import RowArrowView from '../../../../Components/RowArrowView'
import TextInputleft from '../../../../Components/TextInputleft'
import CommonButton from '../../../../Components/Button'

const EditProfile = ({ navigation }) => {

    const [Username, SetUsername] = useState(true);
    const [Phone, SetPhone] = useState('');
    const [Email, SetEmail] = useState('');
    const [Gender, SetGender] = useState('')
    const [Birthday, SetBirthday] = useState('')



    return (
        <View style={styles.main}>

            <Mainheader navigation={navigation} navigating={() => navigation.goBack()} props='Edit Profile' image={"arrow-back"}
                toggleicon={
                    <MaterialCommunityIcons

                        name="account-edit" size={wp("6%")} color={constants.blackicon} onPress={() => navigation.navigate('EditProfile')} />


                }/>
            <ScrollView >
                <View >
                    <View style={styles.imageview}>
                        <Image source={{ uri: "https://www.hayalanka.com/wp-content/uploads/2017/07/avtar-image.jpg" }} style={{ width: wp('20%'), height: hp('10%'), borderRadius: 50, alignSelf: "center" }}></Image>
                        <Mainheading props={"Jhon Smith"}></Mainheading>
                        <Text style={styles.username}>Gold Member</Text>

                    </View>
                    <View style={{ marginTop: hp("4%") }}></View>
                    <TextInputleft Image3={'edit'} onchangetexting={Username => SetUsername(Username)}></TextInputleft>
                    <View style={styles.margin}>
                        <TextInputleft Image3={'edit'} onchangetexting={Phone => SetPhone(Phone)}></TextInputleft>
                    </View>
                    <View style={styles.margin}>
                        <TextInputleft Image3={'edit'} onchangetexting={Email =>
                            SetEmail(Email)
                        }></TextInputleft>
                    </View>
                    <View style={styles.margin}>
                        <TextInputleft Image3={'edit'} onchangetexting={Gender =>
                            SetGender(Gender)
                        }></TextInputleft>
                    </View>
                    <View style={styles.margin}>
                        <TextInputleft Image3={'edit'} onchangetexting={Birthday =>
                            SetBirthday(Birthday)
                        }></TextInputleft>
                    </View>
                    <View style={styles.marginview1}>
                        <CommonButton navigating={() => Nextview()} props={"SIGN IN"}></CommonButton>

                    </View>



                </View>
            </ScrollView>
        </View>

    );

};


export default EditProfile;

