

import React, { useState } from 'react';
import { SafeAreaView, StyleSheet, View, Text, Image, TouchableOpacity,Alert, TextInput, ScrollView } from 'react-native';
import styles from './style';
import Mainheader from '../../../Components/Mainheader';
import { Header} from 'react-native-elements';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../../Utility/index';
import CommonButton from '../../../Components/Button'
import TextInputleft from '../../../Components/TextInputleft'
import TextInputing from '../../../Components/TextInputing'

import ImageStart from '../../../Components/ImageStart';
import Mainheading from '../../../Components/Mainheading';
import Subheading from '../../../Components/Subheading';
import * as Utility from '../../../Utility/index';

const ForgetPasswd = ({ navigation }) => {
    const [email, Setemail] = useState('');
    const Nextview = () => {
        if (Utility.isFieldEmpty((email))) {
            return Alert.alert('Please fill your e-mail');
        } else if (Utility.isValidEmail(email)) {
            return Alert.alert('Please fill valid e-mail');
    }
    navigation.navigate("ChangePasswd")

    }
    return (
        <View style={styles.main}>
             <Mainheader navigation={navigation} navigating={() => navigation.goBack()} props='Forget Password' image={"arrow-back" }></Mainheader>
            
            <ScrollView>
                <View style={styles.containerMain}>
                    <ImageStart Images={require('../../../Assets/forget.png')}></ImageStart>

                        <View style={styles.main1}>
                            <Text style={styles.amount}>We will send a mail to {"\n"}the email address you registered {"\n"}to regain your password</Text>
                            <TextInputing Image1={'email'} autoCapitaL={"none"} onchangetexting={email => Setemail(email)}></TextInputing>
                          
                            {/* <TextInputleft placeholdertext={"Email Address"}  onchangetexting={email => Setemail(email)}></TextInputleft> */}
                            <View style={{ marginTop: hp("2%"),}}>
                            <Subheading props={"Email sent to ex*****@gmail.com"}></Subheading>
                            </View>
                           <CommonButton navigating={() => Nextview()} props={"Send"}></CommonButton>
                        </View>

                        

                </View>
            </ScrollView>
        </View>
    );

};


export default ForgetPasswd;
