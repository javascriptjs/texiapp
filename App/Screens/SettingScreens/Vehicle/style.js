import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../../Utility/index';
import constants from '../../../Constants/colors';



const styles = StyleSheet.create({
main:
{ flex: 1 ,backgroundColor:constants.backgroundDull},

 marginview:
 { marginTop: hp("3%"), alignSelf: "center", width: wp("80%"), },
 marginview1:
 { marginBottom:wp("10%")},
text:
{ fontSize: wp("3.5%"), textAlign: "left" },
});
export default styles;
