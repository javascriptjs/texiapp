import React, { useEffect, useState } from 'react';
import { View, Text, ImageBackground, Image, Switch, TouchableOpacity, FlatList, TextInput } from 'react-native';
import Mainheader from '../../Components/Mainheader';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../Utility/index';
import constants from '../../Constants/colors';
import Icon from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import styles from './style';
import { ScrollView } from 'react-native';
import Mainheading from '../../Components/Mainheading';
import { color } from 'react-native-reanimated';
import RadioButton from 'react-native-radio-button';
import { Avatar } from 'react-native-paper';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

const SearchReader = ({ navigation }) => {


    const [isSelected1, setSelection1] = useState(false);
    const [Like, SetLike] = useState(false);
    const [Play, SetPlay] = useState(false);

    const [isSelected3, setSelection3] = useState(false);
    const [Cancelled, SetCancelled] = useState(false);
    const [Complete, SetComplete] = useState(false);
    const [defaultRating, setDefaultRating] = useState(2);
    const [maxRating, setMaxRating] = useState([1, 2, 3, 4, 5]);

    const starImageFilled =
        'https://raw.githubusercontent.com/AboutReact/sampleresource/master/star_filled.png';
    const starImageCorner =
        'https://raw.githubusercontent.com/AboutReact/sampleresource/master/star_corner.png';

    console.log("Complete", Complete)


    const data = [
        {
            text1: 'Jhon Smith',
            text2: 'Bike Rider',
            text3: '$20.00/hr',
        },


    ];
    const CustomRatingBar = () => {
        return (
            <View style={styles.customRatingBarStyle}>
                {maxRating.map((item, key) => {
                    return (
                        <TouchableOpacity
                            activeOpacity={0.7}
                            key={item}
                            onPress={() => setDefaultRating(item)}>
                            <Image
                                style={styles.starImageStyle}
                                resizeMode='contain'
                                source={
                                    item <= defaultRating
                                        ? { uri: starImageFilled }
                                        : { uri: starImageCorner }
                                }
                            />
                        </TouchableOpacity>
                    );
                })}
            </View>
        );
    };
    return (
        <View style={styles.main}>

            <Mainheader navigation={navigation} navigating={() => navigation.goBack()} image={"arrow-back"}></Mainheader>
            <View style={styles.btnview}>
                <View style={styles.rowsearch}>
                    {/* <View style={styles.col1}>
                        <MaterialIcons
                            name="search" size={wp("6%")} color={constants.greyesh} resizeMode="contain" />
                    </View> */}
                    <View style={styles.col2}>
                        <TextInput
                            style={{ fontSize: wp("4%"), color: constants.greyesh }}
                            placeholder="Search"
                            placeholderTextColor={constants.greyesh}
                            onChangeText={Search => SetSearch(Search)}

                        />
                    </View>
                    <TouchableOpacity>

                        <View style={styles.col3}>
                            <MaterialIcons
                                name="search" size={wp("6%")} color={constants.greyesh} resizeMode="contain" />
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
            <View>
                <View style={{ flexDirection: "row", width: wp('90%'), marginTop: hp('5%'), justifyContent: "space-evenly", alignSelf: "center" }}>

                    <TouchableOpacity onPress={() => SetComplete(false)}>
                        <Text style={{ fontSize: wp('4.5%'), fontWeight: '700', color: Complete == false ? constants.bluebtn : constants.black_Text }}>People</Text>
                    </TouchableOpacity>
                    <View style={{ borderWidth: 1 }}></View>
                    <TouchableOpacity onPress={() => SetComplete(true)}>
                        <Text style={{ fontSize: wp('4.5%'), fontWeight: '700', color: Complete == true ? constants.bluebtn : constants.black_Text }}>Place</Text>
                    </TouchableOpacity>
                    <View style={{ borderWidth: 1 }}></View>
                    <TouchableOpacity onPress={() => SetComplete(null)}>
                        <Text style={{ fontSize: wp('4.5%'), fontWeight: '700', color: Complete == null ? constants.bluebtn : constants.black_Text }}>Suggestion</Text>
                    </TouchableOpacity>

                </View>
            </View>
            <ScrollView>
                <FlatList
                    data={data}
                    renderItem={({ item, index }) => (
                        <View>
                            <View style={styles.main}>
                                <View style={styles.imgview}>
                                    <Avatar.Image style={styles.img} size={wp("19%")} source={{ uri: 'https://i.stack.imgur.com/uoVWQ.png' }} />
                                </View>
                                <View style={styles.column}>
                                    <View style={styles.row}>
                                        <Text style={styles.names}>{item.text1}</Text>

                                    </View>
                                    <Text style={styles.km}>{item.text2}</Text>
                                    <View style={{ flexDirection: 'row', alignItems: 'center', width: wp('30%') }}>

                                        <CustomRatingBar />
                                        <Text style={styles.logic}>
                                            {defaultRating}/{Math.max.apply(null, maxRating)}
                                        </Text>

                                    </View>

                                    <Text style={styles.km2}>{item.text3}</Text>

                                </View>
                                <View style={styles.column2}>
                                    <RadioButton
                                        animation={'bounceIn'}
                                        size={wp("3%")}
                                        isSelected={isSelected1}
                                        outerColor={constants.checkgreen}
                                        innerColor={constants.checkgreen}
                                        onPress={() => setSelection1(!isSelected1)}
                                    />
                                    {
                                        Like == false ?
                                            <MaterialCommunityIcons name={"heart-outline"} size={wp("6%")} color={constants.grey} onPress={() => SetLike(true)} />
                                            :
                                            <MaterialCommunityIcons name={"heart"} size={wp("6%")} color={constants.red} onPress={() => SetLike(false)} />
                                    }
                                    {
                                        Play == false ?
                                            <MaterialCommunityIcons name={"arrow-right-drop-circle"} size={wp("6%")} color={constants.bluebtn} onPress={() => SetPlay(true)} />

                                            :
                                            <MaterialCommunityIcons name={"pause-circle"} size={wp("6%")} color={constants.bluebtn} onPress={() => SetPlay(false)} />

                                    }
                                </View>
                            </View>

                        </View>
                    )} />
            </ScrollView>

        </View>


    );
};

export default SearchReader;
