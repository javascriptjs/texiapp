import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../../Utility/index';
import constants from '../../../Constants/colors';


const styles = StyleSheet.create({
  main:
    { flex: 1, backgroundColor: constants.backgroundDull },
  username:
  {
    color: "grey",
    fontSize: wp("4%"),
    textAlign: "center",
    justifyContent: "center",
    alignItems: "center",
  },
  imageview:
  {
    width: wp('90%'),
    marginTop: hp('4%'),
    alignSelf: "center",
  },
  infoview:
  {
    color: constants.grey,
    marginTop: hp("3%"),
    fontSize: wp("4.2%"),
    fontWeight: "bold",
    justifyContent: "flex-start",
  },
  info:
    { width: wp('90%'), alignSelf: "center", marginTop: hp("5%") },
});
export default styles;
