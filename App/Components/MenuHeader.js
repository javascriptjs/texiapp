// import React, { useState } from 'react';
// import {
//   View,
//   Text,
//   Image,
//   ScrollView,
//   TouchableOpacity,
//   Alert,
//   Platform,
// } from 'react-native';
// import {
//   widthPercentageToDP as wp,
//   heightPercentageToDP as hp,
// } from '../Utility/index';
// const Mainheader = ({ navigation,route}) => {

//   return (
// <View>
//   <View style={{
//       flexDirection: "row",
//       elevation:10,

//       backgroundColor: "white",


//       width: "100%",
//       justifyContent: 'space-between',
//       paddingTop:"5%",
//       paddingBottom:"5%"

//   }}>

//       <TouchableOpacity onPress={() => navigation.goBack()} >

//         <Image
//          source={require('../Assets/backarrow.png')} 
//           resizeMode="contain"
//           style={{
//             height: hp('3%'),
//             width: wp('8%'),
//             left:wp("5%"),
//             justifyContent: 'flex-start',
//           }}
//         />
//       </TouchableOpacity>
//       <Text style={{ fontWeight: 'bold', fontSize: wp('4%'),right:wp("35%")}}>Select Location</Text>

//       </View>
//     </View>

//   );
// };
// export default Mainheader;


import React, { useState } from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  TouchableOpacity,
  Alert,
  Platform,
  Switch,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from '../Utility/index';
import { Header } from 'react-native-elements';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import constants from '../Constants/colors';
const MenuHeader = ({ navigation, route, props, image, navigating, toggleicon }) => {

  return (
    <View>
      <Header
        leftComponent={
          <TouchableOpacity onPress={() => navigation.openDrawer()}>
            <MaterialIcons name={image} size={wp("6%")} color={constants.blackicon} />

          </TouchableOpacity>
        }
        centerComponent={{ text: props, style: { color: 'black', fontWeight: 'bold', fontSize: wp('4%'), } }}

        rightComponent={
          toggleicon

        }
        containerStyle={{
          backgroundColor: 'white',
          elevation: 10,
        }}
      />
    </View>

  );
};
export default MenuHeader;

