import React, { useEffect, useState } from 'react';
import { View, Text, ImageBackground, Image, Switch, TouchableOpacity, FlatList, TextInput } from 'react-native';
import Mainheader from '../../../Components/Mainheader';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../../Utility/index';
import constants from '../../../Constants/colors';
import Icon from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import styles from './style';
import { ScrollView } from 'react-native';
import Mainheading from '../../../Components/Mainheading';
import { color } from 'react-native-reanimated';
import RadioButton from 'react-native-radio-button';
import { Avatar } from 'react-native-paper';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MenuHeader from '../../../Components/Mainheader';

const MyWallet = ({ navigation }) => {




    const data = [
        {
            text1: 'Jhon Smith',
            text2: 'ID #1250',
            text3: 'ID #1250',
            text4: '$25.00',
            text5: 'Completed',
        },

        {
            text1: 'Jhon Smith',
            text2: 'ID #1250',
            text3: 'ID #1250',
            text4: '$25.00',
            text5: 'Completed',
        },
        {
            text1: 'Jhon Smith',
            text2: 'ID #1250',
            text3: 'ID #1250',
            text4: '$25.00',
            text5: 'Completed',
        },
        {
            text1: 'Jhon Smith',
            text2: 'ID #1250',
            text3: 'ID #1250',
            text4: '$25.00',
            text5: 'Completed',
        },
        {
            text1: 'Jhon Smith',
            text2: 'ID #1250',
            text3: 'ID #1250',
            text4: '$25.00',
            text5: 'Completed',
        },
    ];

    return (
        <View style={{ flex: 1, backgroundColor: constants.backgroundDull }}>
            {/* <MenuHeader navigation={navigation} image={'menu'} props='My Wallet' ></MenuHeader> */}
            <Mainheader navigation={navigation}  navigating={() => navigation.goBack()}props='My Wallet' image={"arrow-back"}></Mainheader>

            <ScrollView >
                <View style={styles.imageview}>
                    <Text style={styles.text2}>$325.00</Text>

                    <Text style={styles.username}>Total Income</Text>

                </View>

                    <View style={styles.mainbtn1}>

                        <View style={styles.btnview}>
                            <View style={styles.cols1}>
                                {/* <MaterialIcons name={"arrow-forward-ios"} size={wp("6%")} color={constants.white} /> */}
                    <Image source={require('../../../Assets/icondolar.png')} style={{width:wp("10%")}} resizeMode="contain" ></Image>
                            
                            </View>
                            <View style={{ flexDirection: "column", width: wp("70%"),paddingLeft:wp("5%") }}>
                                <Text style={styles.textbtn}>Payment Method</Text>
                            </View>
                            <View style={styles.cols2}>
                <TouchableOpacity  onPress={()=>navigation.navigate("Payment")}>

                                <MaterialIcons name={"arrow-forward-ios"} size={wp("6%")} color={constants.white} />
                </TouchableOpacity>
                           
                            </View>
                        </View>



                    </View>


                <View style={{ marginBottom: wp("15%") }}>
                    <View style={styles.info}>
                        <Text style={styles.infoview}>PAYMENT HISTORY</Text>
                    </View>
                    <FlatList
                        data={data}
                        renderItem={({ item, index }) => (
                            <View>
                                <View style={styles.main}>
                                    <View style={styles.imgview}>
                                        <Avatar.Image style={styles.img} size={wp("19%")} source={{ uri: 'https://i.stack.imgur.com/uoVWQ.png' }} />
                                    </View>
                                    <View style={styles.column}>
                                        <View style={styles.row}>
                                            <Text style={styles.names}>{item.text1}</Text>

                                        </View>


                                        <Text style={styles.km2}>{item.text3}</Text>

                                    </View>

                                    <View style={styles.column2}>
                                        <View style={styles.row}>
                                            <Text style={styles.names}>{item.text4}</Text>

                                        </View>


                                        <TouchableOpacity>
                                            <Text style={styles.km3}>{item.text5}</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>

                            </View>
                        )} />
                </View>

            </ScrollView>
        </View>


    );
};

export default MyWallet;
