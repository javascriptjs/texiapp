import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../Utility/index';

const iconStyles = {
    size: 100,
    color: '#FFFFFF',
};

const styles = StyleSheet.create({

  mainbtn1:
  {  
  // marginBottom:"10%"
  
},
mainbtn:
  { 
    // marginTop:hp("4%"), 
  // marginBottom:"10%"
},

btnview:
{
  backgroundColor: "#FFBC00",
  // marginTop: hp("15%"),
  borderRadius: 25,

  flexDirection: "row",
  justifyContent: "center",
  alignItems: "center",
  alignSelf: "center",

  width: wp("85%"),
  height: hp("7.5%"),
},
btnview2:
{
  backgroundColor: "#3A56FF",
  marginTop: hp("5%"),
  borderRadius: 25,



  flexDirection: "row",
  justifyContent: "center",
  alignItems: "center",
  alignSelf: "center",

  width: wp("85%"),
  height: hp("7.5%"),
},
    mainhd:
        { flex: 1, backgroundColor: 'white' },
        containerMain: {
            flex: 1,
            
           
        },
        bottomView: {
            width: '90%',
          
    
            justifyContent: 'center',
            alignSelf:'center',
            alignItems: 'center',
            
    
    
    
        },
        textbtn:
        {
            color: "white",
            fontSize: wp("4%"),
            fontWeight:'500'
        },
        main1:
        {
            alignSelf: "center",
        },
        container: {
            flex: 1,
            backgroundColor: 'white',
            padding: 10,
            justifyContent: 'center',
            textAlign: 'center',
          },
          titleText: {
            padding: 8,
            fontSize: 16,
            textAlign: 'center',
            fontWeight: 'bold',
          },
          textStyle: {
            textAlign: 'center',
            fontSize: 23,
            color: '#000',
            marginTop: 15,
          },
          textStyleSmall: {
            textAlign: 'center',
            fontSize: 16,
            color: '#000',
            marginTop: 15,
          },
          buttonStyle: {
            justifyContent: 'center',
            flexDirection: 'row',
            marginTop: 30,
            padding: 15,
            backgroundColor: '#8ad24e',
          },
          buttonTextStyle: {
            color: '#fff',
            textAlign: 'center',
          },
          customRatingBarStyle: {
            flexDirection: 'row',
          },
          starImageStyle: {
            width: wp('4%'),
            height: hp('4%'),
          },
       
        map: {
              flex:1
             },

             container:{
              flex: 1,
            },mapContainer: {
              flex: 1,
            },
            map: {
              flex: 1,
              // width: Dimensions.get("window").width,
              // height: Dimensions.get("window").height,
            }

});
export default styles;
