import React, { useEffect, useState } from 'react';
import { View, Text, ImageBackground, Image, Switch, TouchableOpacity, FlatList } from 'react-native';
import Mainheader from '../../../Components/Mainheader';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../../Utility/index';
import constants from '../../../Constants/colors';
import Icon from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import styles from './style';
import { ScrollView } from 'react-native';
import Mainheading from '../../../Components/Mainheading';
import { color } from 'react-native-reanimated';
import { Avatar } from 'react-native-paper';


const History = ({ navigation }) => {

    const [Cancelled, SetCancelled] = useState(false);
    const [Complete, SetComplete] = useState(false);
    console.log("Complete", Complete)


    const data = [
        {
            text: 'Jhon Smith',
            btn: 'Complete',
            km: '2.2km',
            calendar: '20 Feb, 2020',
            hours: '2 Hours',
            price: "200.00",

        },
        {
            text: 'Jhon Smith',
            btn: 'Complete',
            km: '2.2km',
            calendar: '20 Feb, 2020',
            hours: '2 Hours',
            price: "200.00",


        },
        {
            text: 'Jhon Smith',
            btn: 'Complete',
            km: '2.2km',
            calendar: '20 Feb, 2020',
            hours: '2 Hours',
            price: "200.00",

        },
    ];

    return (
        <View style={{ flex: 1, backgroundColor:constants.backgroundDull }}>

            <Mainheader navigation={navigation} navigating={() => navigation.goBack()} props='History' image={"arrow-back"}></Mainheader>
            <View>
                <View style={{ flexDirection: "row", width: wp('90%'), marginTop: hp('5%'), justifyContent: "space-evenly", alignSelf: "center" }}>
                    <TouchableOpacity onPress={() => SetComplete(false)}>
                        <Text style={{ fontSize: wp('4.5%'), fontWeight: '700', color: Complete == false ?constants.checkgreen:constants.black_Text}}>Complete</Text>
                    </TouchableOpacity>
                    <View style={{ borderWidth: 1, }}></View>
                    <TouchableOpacity onPress={() => SetComplete(true)}>
                        <Text style={{ fontSize: wp('4.5%'), fontWeight: '700'  ,color: Complete == true ?constants.yellow:constants.black_Text}}>Upcoming</Text>
                    </TouchableOpacity>
                    <View style={{ borderWidth: 1 }}></View>
                    <TouchableOpacity onPress={() => SetComplete(null)}>
                        <Text style={{ fontSize: wp('4.5%'), fontWeight: '700' ,color: Complete == null ?constants.red:constants.black_Text}}>Cancelled</Text>
                    </TouchableOpacity>

                </View>
            </View>
            <ScrollView>
                <FlatList
                    data={data}
                    renderItem={({ item, index }) => (
                        <View>
                            {Complete == false ?
                                <View style={styles.main}>
                                    <View style={styles.imgview}>
                                        {/* <Image source={require('../../../Assets/Oval.png')} style={styles.img} resizeMode="contain" ></Image> */}
                    <Avatar.Image style={styles.img} size={wp("19%")} source={{ uri: 'https://i.stack.imgur.com/uoVWQ.png' }} />
                                  
                                    </View>
                                    <View style={styles.column}>
                                        <View style={styles.row}>
                                            <Text style={styles.names}>{item.text}</Text>
                                            <TouchableOpacity style={{
                                                backgroundColor: constants.checkgreen,
                                                paddingLeft: wp("3%"),
                                                paddingRight: wp("3%"),

                                                borderRadius: 30
                                            }} onPress={() => navigation.navigate("HistoryDetail")}>
                                                <Text style={{ color: constants.white, fontSize: wp("4%") }}>Complete</Text>
                                            </TouchableOpacity>
                                        </View>
                                        <View style={{ marginTop: hp("1%") }}>
                                            <Text style={styles.km}>{item.km}</Text>
                                        </View>
                                        <View style={styles.imgmain}>
                                            <Icon name="calendar" size={wp("6%")} color={constants.grey} />

                                            <Text style={{ color: constants.grey, fontSize: wp("3.5%") }}>{item.calendar}</Text>
                                            <Ionicons name="time-outline" size={wp("6%")} color={constants.grey} />

                                            <Text style={{ color: constants.grey, fontSize: wp("3.5%") }}>{item.hours}</Text>
                                            <MaterialCommunityIcons name="sack" size={wp("6%")} color={constants.grey} />

                                            <Text style={{ color: constants.grey, fontSize: wp("3.5%") }}>${item.price}</Text>
                                        </View>

                                    </View>

                                </View>
                                : Complete == true ?
                                    <View style={styles.main}>
                                        <View style={styles.imgview}>
                                            <Image source={require('../../../Assets/Oval.png')} style={styles.img} resizeMode="contain" ></Image>
                                        </View>
                                        <View style={styles.column}>
                                            <View style={styles.row}>
                                                <Text style={styles.names}>{item.text}</Text>
                                                <TouchableOpacity style={{
                                                    backgroundColor: constants.yellow,
                                                    paddingLeft: wp("7%"),
                                                    paddingRight: wp("7%"),
                                                    
                                                    borderRadius: 30
                                                }} onPress={() => navigation.navigate("HistoryDetail")}>
                                                    <Text style={{ color: constants.white, fontSize: wp("4%") }}>Upcoming</Text>
                                                </TouchableOpacity>
                                            </View>
                                            <View style={{ marginTop: hp("1%") }}>
                                                <Text style={styles.km}>{item.km}</Text>
                                            </View>
                                            <View style={styles.imgmain}>
                                                <Icon name="calendar" size={wp("6%")} color={constants.grey} />

                                                <Text style={{ color: constants.grey, fontSize: wp("3.5%") }}>{item.calendar}</Text>
                                                <Ionicons name="time-outline" size={wp("6%")} color={constants.grey} />

                                                <Text style={{ color: constants.grey, fontSize: wp("3.5%") }}>{item.hours}</Text>
                                                <MaterialCommunityIcons name="sack" size={wp("6%")} color={constants.grey} />

                                                <Text style={{ color: constants.grey, fontSize: wp("3.5%") }}>${item.price}</Text>
                                            </View>

                                        </View>

                                    </View> :
                                    <View style={styles.main}>
                                        <View style={styles.imgview}>
                                            <Image source={require('../../../Assets/Oval.png')} style={styles.img} resizeMode="contain" ></Image>
                                        </View>
                                        <View style={styles.column}>
                                            <View style={styles.row}>
                                                <Text style={styles.names}>{item.text}</Text>
                                                <TouchableOpacity style={{
                                                    backgroundColor: constants.red,
                                                    paddingLeft: wp("7%"),
                                                    paddingRight: wp("7%"),
                                                    
                                                    borderRadius: 30
                                                }} onPress={() => navigation.navigate("HistoryDetail")}>
                                                    <Text style={{ color: constants.white, fontSize: wp("4%") }}>Cancelled</Text>
                                                </TouchableOpacity>
                                            </View>
                                            <View style={{ marginTop: hp("1%") }}>
                                                <Text style={styles.km}>{item.km}</Text>
                                            </View>
                                            <View style={styles.imgmain}>
                                                <Icon name="calendar" size={wp("6%")} color={constants.grey} />

                                                <Text style={{ color: constants.grey, fontSize: wp("3.5%") }}>{item.calendar}</Text>
                                                <Ionicons name="time-outline" size={wp("6%")} color={constants.grey} />

                                                <Text style={{ color: constants.grey, fontSize: wp("3.5%") }}>{item.hours}</Text>
                                                <MaterialCommunityIcons name="sack" size={wp("6%")} color={constants.grey} />

                                                <Text style={{ color: constants.grey, fontSize: wp("3.5%") }}>${item.price}</Text>
                                            </View>

                                        </View>

                                    </View>}
                        </View>
                    )} />
            </ScrollView>

        </View>


    );
};

export default History;
