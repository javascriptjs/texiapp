import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import Splash from '../Screens/Splash';
import SwiperScreen from "../Screens/AuthScreens/SwiperScreen"
import SelectLocation from "../Screens/AuthScreens/SelectLocation"
import SignUp from "../Screens/AuthScreens/SignUp"
import SignIn from "../Screens/AuthScreens/SignIn"
import OtpVerify from "../Screens/AuthScreens/OtpVerify"
import ForgetPasswd from "../Screens/AuthScreens/ForgetPasswd"
import ChangePasswd from "../Screens/AuthScreens/ChangePasswd"
import OfflineScreen from "../Screens/OfflineScreen"
import OnlineScreen from "../Screens/OnlineScreen"
import PickUp from "../Screens/OnlineScreen/PickUp"
import TabNavigator from './TabNavigator';
import DrawerScreen from './DrawerNavigator';
import History from '../Screens/DrawerScreens/History'
import InviteFriends from '../Screens/DrawerScreens/InviteFriends'
///////// TAB SCREENS /////////////////////////

import Home from "../Screens/TabScreens/Home"
import Location from "../Screens/TabScreens/Location"
import AddVechile from "../Screens/TabScreens/AddVechile"
import Invite from "../Screens/DrawerScreens/InviteFriends/Invite"
import HistoryDetail from "../Screens/DrawerScreens/HistoryDetail"
import Setting from "../Screens/DrawerScreens/Setting"
import EditProfile from '../Screens/TabScreens/MyProfile/EditProfile'
import MyProfile from '../Screens/TabScreens/MyProfile';
import Vehicle from '../Screens/SettingScreens/Vehicle';
import License from '../Screens/SettingScreens/License';
import PaymentMethod from '../Screens/SettingScreens/PaymentMethod'
import Payment from '../Screens/SettingScreens/Payment'

import Document from '../Screens/SettingScreens/Document'
import FavouriteDriver from '../Screens/FavouriteDriver'
import SearchReader from '../Screens/SearchReader'
import MyWallet from '../Screens/DrawerScreens/MyWallet'
import PickupPoint from '../Screens/TabScreens/PickupPoint'
import StripePayment from '../Screens/SettingScreens/StripePayment'
import ChatModule from '../Screens/SettingScreens/ChatModule';
const Stack = createStackNavigator();

const MainStackNavigator = () => {
  return (
    
    <Stack.Navigator>
      
      
    
        <Stack.Screen
        name="Splash"
        component={Splash}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="SwiperScreen"
        component={SwiperScreen}
        options={{ headerShown: false }}
      />
       <Stack.Screen
        name="SelectLocation"
        component={SelectLocation}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="SignUp"
        component={SignUp}
        options={{ headerShown: false }}
      />
     <Stack.Screen
        name="SignIn"
        component={SignIn}
        options={{ headerShown: false }}
      />
     
    
     <Stack.Screen
        name="OtpVerify"
        component={OtpVerify}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="ForgetPasswd"
        component={ForgetPasswd}
        options={{ headerShown: false }}
      />
    
     
        
       <Stack.Screen
        name="PickUp"
        component={PickUp}
        options={{ headerShown: false }}
      />
        <Stack.Screen
        name="TabNavigator"
        component={TabNavigator}
        options={{ headerShown: false }}
      />
       <Stack.Screen
        name="DrawerScreen"
        component={DrawerScreen}
        options={{ headerShown: false }}
      />
       <Stack.Screen
        name="OnlineScreen"
        component={OnlineScreen}
        options={{ headerShown: false }}
      />
       <Stack.Screen
        name="Home"
        component={Home}
        options={{ headerShown: false }}
      />
        <Stack.Screen
        name="Location"
        component={Location}
        options={{ headerShown: false }}
      />
      
      <Stack.Screen
        name="AddVechile"
        component={AddVechile}
        options={{ headerShown: false }}
      />
       <Stack.Screen
        name="History"
        component={History}
        options={{ headerShown: false }}
      />
       <Stack.Screen
        name="InviteFriends"
        component={InviteFriends}
        options={{ headerShown: false }}
      />
        <Stack.Screen
        name="Invite"
        component={Invite}
        options={{ headerShown: false }}
      />
    <Stack.Screen
        name="Setting"
        component={Setting}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="HistoryDetail"
        component={HistoryDetail}
        options={{ headerShown: false }}
      />
     <Stack.Screen
        name="ChangePasswd"
        component={ChangePasswd}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="OfflineScreen"
        component={OfflineScreen}
        options={{ headerShown: false }}
      />
           <Stack.Screen
        name="MyProfile"
        component={MyProfile}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="EditProfile"
        component={EditProfile}
        options={{ headerShown: false }}
      />
       
       <Stack.Screen
        name="Vehicle"
        component={Vehicle}
        options={{ headerShown: false }}
      />
       <Stack.Screen
        name="Document"
        component={Document}
        options={{ headerShown: false }}
      />
      
       <Stack.Screen
        name="PaymentMethod"
        component={PaymentMethod}
        options={{ headerShown: false }}
      />
      
      <Stack.Screen
        name="SearchReader"
        component={SearchReader}
        options={{ headerShown: false }}
      />
    
    <Stack.Screen
        name="MyWallet"
        component={MyWallet}
        options={{ headerShown: false }}
      />
  <Stack.Screen
        name="FavouriteDriver"
        component={FavouriteDriver}
        options={{ headerShown: false }}
      />
       <Stack.Screen
        name="PickupPoint"
        component={PickupPoint}
        options={{ headerShown: false }}
      />
        <Stack.Screen
        name="License"
        component={License}
        options={{ headerShown: false }}
      />
       <Stack.Screen
        name="StripePayment"
        component={StripePayment}
        options={{ headerShown: false }}
      />
       <Stack.Screen
        name="Payment"
        component={Payment}
        options={{ headerShown: false }}
      />
       <Stack.Screen
        name="ChatModule"
        component={ChatModule}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
};

export { MainStackNavigator };
