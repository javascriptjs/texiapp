import React, { useEffect } from 'react';
import { useState } from 'react';
import { View, Text, ImageBackground, PermissionsAndroid, Image, Switch, TouchableOpacity, Alert } from 'react-native';
import Mainheader from '../../../Components/Mainheader';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from '../../../Utility';
import styles from "./style"
import TextInputleft from '../../../Components/TextInputleft'
import { ScrollView } from 'react-native-gesture-handler';
import CommonButton from '../../../Components/Button'
import constants from '../../../Constants/colors';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Modals from 'react-native-modal';
import * as ImagePicker from "react-native-image-picker"
const License = ({ navigation }) => {
    const [Name, SetvehicleName] = useState('');
    const [isVisible, setIsVisible] = useState(false)
    console.log("isVisible", isVisible)
    const [Modal, SetvehicleModal] = useState('');

    const [Cameraresponse, setCameraresponse] = useState('');
    const [Imageresponse, setImageresponse] = useState('');
    console.log("Imageresponse", Imageresponse)
    const [Imageuri, SetImage] = useState('');
    console.log("Imageresponse", Imageuri)

    const closemodal = () => {
        setIsVisible(false)
    }

    const PopUp = () => {
        setIsVisible(true)

    }
    const PopupCancel = () => {
        setIsVisible(false)

    }
    const options = {
        title: 'Select Avatar',
        customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
        storageOptions: {
            skipBackup: true,
            path: 'images',
        },
    };
    const CameraLaunch = async () => {
        setIsVisible(false)

        try {

            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.CAMERA,
                {
                    title: "App Camera Permission",
                    message: "App needs access to your camera ",
                    buttonNeutral: "Ask Me Later",
                    buttonNegative: "Cancel",
                    buttonPositive: "OK"
                }
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                console.log("Camera permission given");
                ImagePicker.launchCamera(options, (response) => {
                    console.log('Response = ', response);
                    SetImage(response.assets[0].uri)
                    if (response.didCancel) {
                        console.log('User cancelled image picker');
                    } else if (response.error) {
                        console.log('ImagePicker Error: ', response.error);
                    } else if (response.customButton) {
                        console.log('User tapped custom button: ', response.customButton);
                        alert(response.customButton);
                    } else {
                        const source = { uri: response.uri };

                        // You can also display the image using data:
                        // const source = { uri: 'data:image/jpeg;base64,' + response.data };
                        // alert(JSON.stringify(response));s
                        console.log('response', JSON.stringify(response));
                        setCameraresponse({
                            filePath: response,
                            fileData: response.data,
                            fileUri: response.uri
                        });

                    }
                });
            } else {
                console.log("Camera permission denied");
            }
        } catch (err) {
            console.warn(err);
        }
    };

    const ChooseImage = () => {
        setIsVisible(false)

        let options = {
            title: 'Select Image',
            customButtons: [
                { name: 'customOptionKey', title: 'Choose Photo from Custom Option' },
            ],
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.launchImageLibrary(options, (response) => {
            console.log('Response =>>> ', response);
            SetImage(response.assets[0].uri)
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
                alert(response.customButton);
            } else {
                const source = { uri: response.uri };


                console.log('response', JSON.stringify(response));
                setCameraresponse({
                    filePath: response,
                    fileData: response.data,
                    fileUri: response.uri
                });
            }
        });
    }

    return (

        <View style={{ flex: 1, backgroundColor: constants.backgroundDull }}>
            <Mainheader navigation={navigation} navigating={() => navigation.goBack()} props='Driving license' image={"arrow-back"}></Mainheader>
            <ScrollView >
                <View style={{ width: wp("90%"), alignSelf: "center", }}>
                    <View style={{ width: wp("70%"), marginTop: hp("4%"), height: hp("18%"), elevation: 5, backgroundColor: constants.white, alignSelf: "center", justifyContent: "center" }}>
                        <TouchableOpacity onPress={() => PopUp()}>
                            <View style={{ alignItems: "center", }}>
                                {Imageuri == "" ?
                                    <MaterialIcons name={"cloud-upload"} size={wp("20%")} color={constants.grey_Background} />
                                    : <Image

                                        source={{ uri: Imageuri }}
                                        resizeMode={"contain"}
                                        style={{ width: wp("40%"), height: hp("15") }}

                                    />}
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.info}>
                        <Text style={styles.infoview}>National Identification Cards</Text>
                    </View>
                </View>
                <View style={{ marginTop: hp("4%") }}>

                    <View style={styles.marginview}>
                        <Text style={styles.text}>CARD NUMBER</Text>
                        <TextInputleft Image3={'arrow-forward-ios'} onchangetexting={Name => SetvehicleName(Name)}></TextInputleft>
                    </View>
                    <View style={styles.marginview}>
                        <Text style={styles.text}>EXPIRATION DATE</Text>
                        <TextInputleft Image3={'arrow-forward-ios'} onchangetexting={Modal => SetvehicleModal(Modal)}></TextInputleft>
                    </View>

                    <View style={styles.marginview1}>
                        <CommonButton navigating={() => navigation.navigate("Setting")} props={"Save Now"}></CommonButton>

                    </View>
                </View>
                <Modals isVisible={isVisible} onBackdropPress={() => closemodal()}>
                    <View>
                        <View style={{ width: wp("80%"), alignSelf: "center" }}>
                            <View style={{ width: wp("80%"), borderTopRightRadius: 10, borderTopLeftRadius: 10, justifyContent: "center", backgroundColor: "white", elevation: 10, height: hp("7.5%") }}>
                                <TouchableOpacity onPress={() => CameraLaunch("")}>
                                    <Text style={{ fontSize: wp('4%'), textAlign: "center" }}>Take a picture</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ width: wp("80%"), borderBottomEndRadius: 10, borderBottomStartRadius: 10, justifyContent: "center", marginTop: hp("0.5%"), backgroundColor: "white", elevation: 10, height: hp("7.5%") }}>
                                <TouchableOpacity onPress={() => ChooseImage("")}>

                                    <Text style={{ fontSize: wp('4%'), textAlign: "center" }}>Choose a picture</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={styles.btnview}>
                            <TouchableOpacity activeOpacity={"0"} onPress={() => PopupCancel("")}>
                                <Text style={styles.textbtn}>Cancel</Text>
                            </TouchableOpacity>
                        </View>

                    </View>
                </Modals>
            </ScrollView>
        </View>
    );
};

export default License;
// import React, { Fragment, Component } from 'react';
// import * as ImagePicker from "react-native-image-picker"

// import {
//   SafeAreaView,
//   StyleSheet,
//   ScrollView,
//   View,
//   Text,
//   StatusBar,
//   Image,
//   Button,
//   Dimensions,
//   TouchableOpacity,
//   PermissionsAndroid
// } from 'react-native';

// import {
//   Header,
//   LearnMoreLinks,
//   Colors,
//   DebugInstructions,
//   ReloadInstructions,
// } from 'react-native/Libraries/NewAppScreen';

// const options = {
//   title: 'Select Avatar',
//   customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
//   storageOptions: {
//     skipBackup: true,
//     path: 'images',
//   },
// };

// export default class License extends Component {
//   constructor(props) {
//     super(props)
//     this.state = {
//       filepath: {
//         data: '',
//         uri: ''
//       },
//       fileData: '',
//       fileUri: ''
//     }
//   }

//   chooseImage = () => {
//     let options = {
//       title: 'Select Image',
//       customButtons: [
//         { name: 'customOptionKey', title: 'Choose Photo from Custom Option' },
//       ],
//       storageOptions: {
//         skipBackup: true,
//         path: 'images',
//       },
//     };
//     ImagePicker.launchImageLibrary(options, (response) => {
//       console.log('Response = ', response);

//       if (response.didCancel) {
//         console.log('User cancelled image picker');
//       } else if (response.error) {
//         console.log('ImagePicker Error: ', response.error);
//       } else if (response.customButton) {
//         console.log('User tapped custom button: ', response.customButton);
//         alert(response.customButton);
//       } else {
//         const source = { uri: response.uri };

//         // You can also display the image using data:
//         // const source = { uri: 'data:image/jpeg;base64,' + response.data };
//         // alert(JSON.stringify(response));s
//         console.log('response', JSON.stringify(response));
//         this.setState({
//           filePath: response,
//           fileData: response.data,
//           fileUri: response.uri
//         });
//       }
//     });
//   }
//   requestCameraPermission = async () => {
//     try {
//       const granted = await PermissionsAndroid.request(
//         PermissionsAndroid.PERMISSIONS.CAMERA,
//         {
//           title: "App Camera Permission",
//           message:"App needs access to your camera ",
//           buttonNeutral: "Ask Me Later",
//           buttonNegative: "Cancel",
//           buttonPositive: "OK"
//         }
//       );
//       if (granted === PermissionsAndroid.RESULTS.GRANTED) {
//         console.log("Camera permission given");
//           ImagePicker.launchCamera(options, (response) => {
//       console.log('Response = ', response);

//       if (response.didCancel) {
//         console.log('User cancelled image picker');
//       } else if (response.error) {
//         console.log('ImagePicker Error: ', response.error);
//       } else if (response.customButton) {
//         console.log('User tapped custom button: ', response.customButton);
//         alert(response.customButton);
//       } else {
//         const source = { uri: response.uri };

//         // You can also display the image using data:
//         // const source = { uri: 'data:image/jpeg;base64,' + response.data };
//         // alert(JSON.stringify(response));s
//         console.log('response', JSON.stringify(response));
//         this.setState({
//           filePath: response,
//           fileData: response.data,
//           fileUri: response.uri
//         });
//       }
//     });
//       } else {
//         console.log("Camera permission denied");
//       }
//     } catch (err) {
//       console.warn(err);
//     }
//   };
//   launchCamera = () => {
//     let options = {
//       storageOptions: {
//         skipBackup: true,
//         path: 'images',
//       },
//     };
//     ImagePicker.launchCamera(options, (response) => {
//         console.log('Response = ', response);
//         if (response.didCancel) {
//          console.log('User cancelled image picker');
//          alert('User cancelled image picker');
//         } else if (response.error) {
//          console.log('ImagePicker Error: ', response.error);
//          alert('ImagePicker Error: ' + response.error);
//         } else {
//          let source = response;
//          this.setState({
//           filePath: source,
//          });
//         }
//        });

//   }

//   launchImageLibrary = () => {
//     let options = {
//       storageOptions: {
//         skipBackup: true,
//         path: 'images',
//       },
//     };
//     ImagePicker.launchImageLibrary(options, (response) => {
//       console.log('Response = ', response);


//     });

//   }

//   renderFileData() {
//     if (this.state.fileData) {
//       return <Image source={{ uri: 'data:image/jpeg;base64,' + this.state.fileData }}
//         style={styles.images}
//       />
//     } else {
//       return <Image source={require('../../../Assets/dollar.png')}
//         style={styles.images}
//       />
//     }
//   }

//   renderFileUri() {
//     if (this.state.fileUri) {
//       return <Image
//         source={{ uri: this.state.fileUri }}
//         style={styles.images}
//       />
//     } else {
//       return <Image
//         source={require('../../../Assets/dollar.png')}
//         style={styles.images}
//       />
//     }
//   }
//   render() {
//     return (
//       <Fragment>
//         <StatusBar barStyle="dark-content" />
//         <SafeAreaView>
//           <View style={styles.body}>
//             <Text style={{textAlign:'center',fontSize:20,paddingBottom:10}} >Pick Images from Camera & Gallery</Text>
//             <View style={styles.ImageSections}>
//               <View>
//                 {this.renderFileData()}
//                 <Text  style={{textAlign:'center'}}>Base 64 String</Text>
//               </View>
//               <View>
//                 {this.renderFileUri()}
//                 <Text style={{textAlign:'center'}}>File Uri</Text>
//               </View>
//             </View>

//             <View style={styles.btnParentSection}>
//               <TouchableOpacity onPress={this.chooseImage} style={styles.btnSection}  >
//                 <Text style={styles.btnText}>Choose File</Text>
//               </TouchableOpacity>

//               <TouchableOpacity onPress={this.requestCameraPermission} style={styles.btnSection}  >
//                 <Text style={styles.btnText}>Directly Launch Camera</Text>
//               </TouchableOpacity>

//               <TouchableOpacity onPress={this.launchImageLibrary} style={styles.btnSection}  >
//                 <Text style={styles.btnText}>Directly Launch Image Library</Text>
//               </TouchableOpacity>
//             </View>

//           </View>
//         </SafeAreaView>
//       </Fragment>
//     );
//   }
// };

// const styles = StyleSheet.create({
//   scrollView: {
//     backgroundColor: Colors.lighter,
//   },

//   body: {
//     backgroundColor: Colors.white,
//     justifyContent: 'center',
//     borderColor: 'black',
//     borderWidth: 1,
//     height: Dimensions.get('screen').height - 20,
//     width: Dimensions.get('screen').width
//   },
//   ImageSections: {
//     display: 'flex',
//     flexDirection: 'row',
//     paddingHorizontal: 8,
//     paddingVertical: 8,
//     justifyContent: 'center'
//   },
//   images: {
//     width: 150,
//     height: 150,
//     borderColor: 'black',
//     borderWidth: 1,
//     marginHorizontal: 3
//   },
//   btnParentSection: {
//     alignItems: 'center',
//     marginTop:10
//   },
//   btnSection: {
//     width: 225,
//     height: 50,
//     backgroundColor: '#DCDCDC',
//     alignItems: 'center',
//     justifyContent: 'center',
//     borderRadius: 3,
//     marginBottom:10
//   },
//   btnText: {
//     textAlign: 'center',
//     color: 'gray',
//     fontSize: 14,
//     fontWeight:'bold'
//   }
// });