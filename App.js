
import React from 'react';
import {Provider} from 'react-redux';
import configureStore from './App/Redux/configureStore';
import { NavigationContainer } from '@react-navigation/native';
import { MainStackNavigator } from './App/Navigation/StackNavigator';
const store = configureStore();
const App = () => {
  return (
    <Provider store={store}>
    <NavigationContainer>
      <MainStackNavigator></MainStackNavigator>
    </NavigationContainer>
    </Provider>
  )
}
export default App;
