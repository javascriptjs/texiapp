import React, { useEffect } from 'react';
import { View, Text, ImageBackground, Image, Switch, TextInput, TouchableOpacity, StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../Utility/index';

import constants from '../Constants/colors';

const Whitebtn = ({ navigation, props, navigating }) => {


    return (

        <View>
            <TouchableOpacity onPress={navigating}>

                <View style={styles.mainbtn1}>

                    <View style={styles.btnview}>
                        <Text style={styles.textbtn}>{props}</Text>

                    </View>


                </View>

            </TouchableOpacity>



        </View>

    );
};

export default Whitebtn;

const styles = StyleSheet.create({
    mainbtn1:
    {
        // marginBottom: "10%"
    },
    textbtn:
    {
        color: constants.dullblack,
        // fontWeight: "bold",
        fontSize: wp("4%")
    },
    btnview:
    {
        backgroundColor: constants.faidwhite,
        elevation:5,
        marginTop: hp("5%"),
        borderRadius: 50,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        alignSelf: "center",

        width: wp("80%"),
        height: hp("7.5%"),


      
    },
});