
import React, { useState } from 'react'
import { View, Text, ImageBackground, Image, StyleSheet } from 'react-native';

import { TouchableOpacity } from 'react-native-gesture-handler';
// import PATH from "../Constants/ImagePath"
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../Utility/index";
const rating = ({ navigation }) => {

    const [defaultRating, setDefaultRating] = useState(2);
       const [maxRating, setMaxRating] = useState([1, 2, 3, 4, 5]);

    const starImageFilled =
        'https://raw.githubusercontent.com/AboutReact/sampleresource/master/star_filled.png';
    const starImageCorner =
        'https://raw.githubusercontent.com/AboutReact/sampleresource/master/star_corner.png';

       const CustomRatingBar = () => {

          return (
              <View style={  {
                // justifyContent: 'center',
                flexDirection: 'row',
            }}>
                  {maxRating.map((item, key) => {
                      return (
                          <TouchableOpacity
                              activeOpacity={0.7}
                              key={item}
                              onPress={() => setDefaultRating(item)}>
                              <Image
                                  style={styles.starImageStyle}
                                  resizeMode='contain'
                                  source={
                                      item <= defaultRating
                                          ? { uri: starImageFilled }
                                          : { uri: starImageCorner }
                                  }
                              />
                          </TouchableOpacity>
                      );
                  })}
              </View>
          );
      };
    return (
        <View style={{  }}>
           {/* <Text style={  { color: 'black', left: wp('1%'), fontWeight: '700', fontSize: wp('3.5%') }}>
                ( {defaultRating}.{Math.max.apply(null, maxRating)} Review)
</Text> */}
            

        </View>

    );

}
export default rating;
