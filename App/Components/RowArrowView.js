

import React, { useState } from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Alert,
  Platform,
  Switch,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from '../Utility/index';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import constants from '../Constants/colors';

const RowArrowView = ({ navigating, prop1,prop2}) => {

  return (
    <View style={styles.row1}>
   
    <View style={styles.textview1}>

        <Text style={styles.text}>{prop1}</Text>

    </View>
    <View style={styles.textview2}>
        <Text style={styles.text1}>{prop2}</Text>

    </View>
    <TouchableOpacity onpress={navigating} style={styles.iconarrow}>

        <MaterialIcons name="arrow-forward-ios" size={wp("6%")} color={constants.blackicon} />
    </TouchableOpacity>
</View>

  );
};
export default RowArrowView;


const styles = StyleSheet.create({
    row1:
        { flexDirection: "row", width: wp('90%'), marginTop: hp('4%'), alignSelf: "center", alignItems: "center" },
    imgview:
        { width: wp('10%'), },
    textview:
        { width: wp('70%'), paddingLeft: wp('4%') },
    text:
    {
        color: constants.blackicon, fontSize: wp("4%"),
    },
    text1:
    {
        color: constants.greyesh, fontSize: wp("4%"),alignSelf:"flex-end"
    },
    iconarrow:
        { width: wp('10%'), alignItems: "center", justifyContent: "center" },
        textview1:
        { width: wp('35%'), },
        textview2:
        { width: wp('45%'),paddingLeft: wp('4%')},
});