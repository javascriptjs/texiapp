
import React, { useState }  from 'react'
import  {View,Text,ImageBackground,Image,StyleSheet}from 'react-native';
import constants from '../Constants/colors';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons'
import { TouchableOpacity } from 'react-native-gesture-handler';
// import PATH from "../Constants/ImagePath"
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../Utility/index";

const  Navigatingtext=({navigation,lefticon,prop,Navigating})=>{
  
 
   return(
  
    <TouchableOpacity onPress={Navigating}>
   
    <View style={styles.row1}>
                    <View style={styles.imgview}>
                        {lefticon}

                    </View>
                    <View style={styles.textview}>

                        <Text style={styles.text}>{prop}</Text>

                    </View>
                    <View style={styles.iconarrow}>

                        <MaterialIcons name="arrow-forward-ios" size={wp("6%")} color={constants.blackicon} />
                    </View>
                </View>
                </TouchableOpacity>

   );

}
export default Navigatingtext;

const styles = StyleSheet.create({
 
    row1:
    { flexDirection: "row", width: wp('60%'), marginTop: hp('4%'), alignSelf: "center",alignItems:"center" },
    imgview:
    { width: wp('10%'), },
    textview:
    { width: wp('40%'), paddingLeft: wp('4%') },
  text:
  {color:constants.blackicon, fontSize: wp("4%") },
  iconarrow:
  { width: wp('10%'), alignItems: "center", justifyContent: "center" },

});
