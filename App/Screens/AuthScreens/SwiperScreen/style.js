import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../../Utility/index';

const iconStyles = {
    size: 100,
    color: '#FFFFFF',
};

const styles = StyleSheet.create({
    slide: {
        flex: 1,
        backgroundColor: 'white',

        marginTop: hp('10%'),
    },
    // header: {
    //     justifyContent: 'center',
    //     alignSelf: 'center',
    //     fontFamily: 'Avenir',
    //     // fontSize: wp("4%"),
    //     marginTop: '20%',
    //     fontWeight: 'bold',
    // },
    // text: {
    //     fontFamily: 'Avenir',
    //     fontSize: 18,
    //     marginHorizontal: 40,
    //     textAlign: 'center',
    // },
    containerMain: {
        flex: 1,

    },
    img:
    {
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,

        width: wp("100%"),
        height: hp("35%"),
        top: hp("5%")

    },
    main1:
    {
        alignSelf: "center",
    },

    slide: {
        flex: 1,
        backgroundColor: 'white',

        marginTop: hp('10%'),

    },
    bottomView: {
        width: '100%',
        height: "50%",

        backgroundColor: 'blue',
        elevation: 10,

        justifyContent: 'center',
        alignItems: 'center',
        borderTopRightRadius: 30,
        borderTopLeftRadius: 30,
        position: 'absolute',
        bottom: 0,



    },

    text2:
    {
        color: "white",
        // fontSize: 20,

        fontSize: wp("4.8%"),
        fontWeight: "bold",
        textAlign: "center",
        justifyContent: "flex-start",
        marginTop: hp("5%")
    },
    mainbtn:
        { marginBottom: "20%" },
    btnview:
    {
        backgroundColor: "#FFBC00",
        marginTop: "10%",
        borderRadius: 25,



        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        alignSelf: "center",

        width: wp("80%"),
        height: hp("7.5%"),
    },


    textbtn:
    {
        color: "white",
        // fontWeight: "bold",

        // fontSize: wp("4.8%"),
        fontSize: wp("4%")
    },
    amount:
    {
        color: "white",
        fontSize: wp("4%"),
        marginTop: "5%",
        textAlign: "center",
        justifyContent: "center",
        alignItems: "center",
    },
    // textStyle: {
    //     color: '#fff',
    //     fontSize: 18,
    // },
});
export default styles;