import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../../../Utility/index';
import constants from '../../../../Constants/colors';


const styles = StyleSheet.create({
main:
{ flex: 1, backgroundColor: '#F1F1F1' },
containerMain: {
  flex: 1,

},
main1:
{
    alignSelf: "center",
    backgroundColor: "#FFFFFF",
    elevation: 10,
    borderRadius: 20,
    width: "90%",
    marginBottom: "10%"

},
marginview1:
 { marginBottom:wp("10%")},
margin:
{marginTop: hp("3%")},
 username:
 {
  color: "grey",
  fontSize: wp("4%"),
  textAlign: "center",
  justifyContent: "center",
  alignItems: "center",
},
imageview:
{ width: wp('90%'), 
marginTop: hp('4%'), 
alignSelf: "center",
 },
infoview:
{
    color: constants.greyesh,
  marginTop: hp("3%"),
  fontSize: wp("4.8%"),
  fontWeight: "bold",
  justifyContent: "flex-start",},
  info:
  {width:wp('90%'),alignSelf:"center",marginTop:hp("5%")},
});
export default styles;
