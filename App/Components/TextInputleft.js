
import React, { useState } from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Alert,
  Platform,
  Switch,
  TextInput
} from 'react-native';
import constants from '../Constants/colors';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from '../Utility/index';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

const  TextInputleft= ({ navigation,edit,Image3,placeholdertext,Keyboard,secureTextEntryes,onchangetexting, autoCapitaL}) => {

  return (
    // <View style={styles.container1}>
    <View style={styles.textBoxBtnHolder}>
        <View style={{ left: wp("3%"),flexDirection:"column",width:wp("65%")}}>
        <TextInput
            style={{  fontSize: wp("4%") ,}}
            placeholder={placeholdertext}
            placeholderTextColor={constants.blackicon}

                                        keyboardType={Keyboard}
                                         onChangeText={onchangetexting}
                                        autoCapitalize={autoCapitaL}
                                        secureTextEntry={secureTextEntryes}
        />
        </View>
        
        {/* <TouchableOpacity
            // activeOpacity={0.8}
            style={styles.visibilityBtn}
            // onPress={managePasswordVisibility}
        > */}
         <TouchableOpacity 
         onPress={edit}>
        <View style={{flexDirection:"column",width:wp("15%"),alignSelf:"center",alignItems:"center"}}>
                    <MaterialIcons name={Image3} size={wp("6%")}  color={constants.greyesh} />
</View>
            {/* <Image source={Image3}  style={styles.btnImage} /> */}
        </TouchableOpacity>
    </View>
// </View>
       


  );
};
export default TextInputleft;


const styles = StyleSheet.create({
    container1:
    {
        flex: 1,
        marginTop: "5%",


        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 25,
    },
    textBoxBtnHolder:
    {
        // paddingLeft: "3%",
        
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        // borderColor: '#000',
        alignSelf: "center",
        width: wp("80%"),
        height: hp("7.5%"),

        backgroundColor: constants.faidwhite,
        marginTop: hp("1%"),
        // marginTop: "10%",
        borderRadius: 25,
        elevation: 10,
    },
    visibilityBtn:
    {
        // position: 'absolute',
        // right: 3,
        // left: 3,

        // height: 40,
        width: wp("10%"),
        // padding: 5,
        flexDirection:"column"
        
    },
    btnImage:
    {
        resizeMode: 'contain',
        // height: '100%',
        // width: '100%'
    }
});