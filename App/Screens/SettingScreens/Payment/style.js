import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');
import constants from '../../../Constants/colors';

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../../Utility/index';
const iconStyles = {
    size: 100,
    color: '#FFFFFF',
};

const styles = StyleSheet.create({
   

    img:
    {
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,

        width: wp("100%"),
        height: hp("35%"),
        top: "5%"

    },
    row1:
    { flexDirection: "row", 
    elevation:5, 
     paddingTop: hp("2%"),
    paddingBottom: hp("2%"),
    backgroundColor:constants.white, width: wp('90%'), marginTop: hp('3%'), alignSelf: "center", alignItems: "center", },
imgview:
    { width: wp('10%'),},
textview:
    { width: wp('70%'), paddingLeft: wp('4%') },
    
text:
{
    color: constants.blackicon, fontSize: wp("4%"),
},
text1:
{
    color: constants.greyesh, fontSize: wp("4%"),alignSelf:"flex-end"
},
iconarrow2:
    { width: wp('30%'), alignItems: "center", justifyContent: "center" },
iconarrow:
    { width: wp('10%'), alignItems: "center", justifyContent: "center" },
    textview1:
    { width: wp('40%'), paddingLeft: wp('4%') },
    textview2:
    { width: wp('30%'), paddingLeft: wp('0%'),},
    iosandroid: {
        ...Platform.select({
          ios: {
            bottom: hp('1.6%'),
          },
          android: {
            height: 10,
            width: 30,
          },
        }),
      },
    
});
export default styles;