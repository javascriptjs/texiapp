import React, { useEffect } from 'react';
import { useState } from 'react';
import { View, Text, ImageBackground, Image, Switch, TouchableOpacity } from 'react-native';
import Mainheader from '../../../Components/Mainheader';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from '../../../Utility';
import Icon from 'react-native-vector-icons/AntDesign';
import styles from './style';
import CommonButton from '../../../Components/Button'
import constants from '../../../Constants/colors';

const HistoryDetail = ({ navigation }) => {
  const [toggle, setToggle] = useState(true);

  return (

    <View style={{backgroundColor:constants.backgroundDull,flex:1}}>
      <Mainheader navigation={navigation} navigating={() => navigation.goBack()} props='History Details' image={"arrow-back"}></Mainheader>


      <View style={styles.main1}>
        <Image source={{ uri: "https://www.hayalanka.com/wp-content/uploads/2017/07/avtar-image.jpg" }} style={{ width: wp('10%'), height: hp('6%'), borderRadius: 20 }}></Image>
        <View style={{ width: wp('60%'), paddingLeft: wp('4%') }}>

          <Text style={{ fontSize: wp("4%"), fontWeight: "bold" }}>Jhon Smith</Text>
          <Text style={{ color: "grey", fontSize: wp("3.5%") }}>2.2 km</Text>

        </View>
        <Text style={{ fontSize: wp("4%"), fontWeight: "bold" }}>$20.00</Text>

      </View>
      <View style={{ width: wp('90%'), alignSelf: "center",}}>
      <View style={styles.main2}>

        <Text style={{ fontSize: wp("3.5%"), color: "grey" }}>PICK UP</Text>
        <Text style={{ fontSize: wp("4%"), fontWeight: "400" }}>969 Us  West, Demopolis AL 36732.</Text>
        </View>
      <View style={styles.main2}>

        <Text style={{ fontSize: wp("3.5%"), color: "grey", }}>DROP OFF</Text>
        <Text style={{ fontSize: wp("4%"), fontWeight: "400" }}>969 Us  West, Demopolis AL 36732.</Text>
       </View>
      <View style={styles.main2}>

        <Text style={{ fontSize: wp("3.5%"), color: "grey", }}>TRIP FARE</Text>
        <Text style={{ fontSize: wp("4%"), fontWeight: "400", }}>Discount            …………………………………. $30</Text>
        <Text style={{ fontSize: wp("4%"), fontWeight: "400" }}>Paid amount     .………………………………… $20</Text>
</View>
      </View>

      <CommonButton  props={"Delete History"}></CommonButton>

    </View>
  );
};

export default HistoryDetail;
