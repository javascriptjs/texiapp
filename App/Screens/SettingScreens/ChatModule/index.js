//////////CLASSS CODE /////////////////

import React, {Component} from 'react';
import {useState, useEffect, useCallback} from 'react';
import {
  View,
  Text,
  ImageBackground,
  Image,
  StyleSheet,
  Alert,
  RecyclerViewBackedScrollView,
  ScrollView,
  RefreshControl,
} from 'react-native';
import {GiftedChat} from 'react-native-gifted-chat';
import SocketIOClient from 'socket.io-client';
import {useSelector} from 'react-redux';
import {connect} from 'react-redux';
import * as Utility from '../../../Utility/index';
import * as Services from '../../../api/services';
import * as Url from '../../../api/Url';
import Loader from '../../../Constants/Loader';
import {default as UUID} from 'uuid';
var socket = SocketIOClient('http://93.188.167.68:4500');

var newData = [];
var typingMessage = '';
class ChatHome extends Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      dataname: '',
      receiverMessage: [],
      userId: '',
      toUser: 'admin',
      fromUser: '',
      displaytext: '',
      displaydate: '',
      typingMessage: false,
      loader: false,
      page: 1,
      chatMessage: [],
    };
    this.onInputTextChanged = this.onInputTextChanged.bind(this);
  }

  componentDidMount() {
    this.socketConnetion();

    this.setAllUserData();
    this.roomSet();
    this.recevicMessage();
    this.getTyping();
  }
  //////=================== SOCKET CONNETION =============
  socketConnetion = () => {
    console.log('check funcation');
    let socket = SocketIOClient('http://93.188.167.68:4500');
    socket.on('connect', function () {
      console.log('socket conneted');
    });
    console.log('response selceted user', this.state.toUser);
    console.log('response selceted user', this.state.fromUser);
  };

  /////================ON SEND MESSAGE ==================
  onSend = async (messages = []) => {
    console.log('message', messages);
    socket.emit('chat-msg', {
      msg: messages[0].text,
      msgTo: this.state.toUser,
      date: Date.now(),
    });
    this.setState((previousState) => ({
      messages: GiftedChat.append(previousState.messages, messages),
    }));
  };
  /////===============TWO USER ROOM SET AND OLD CHAT GET =========================
  roomSet = async () => {
    console.log('current user', this.state.fromUser);
    let token = await Utility.getFromLocalStorge('token');
    socket.on('set-room', async (room) => {
      console.log('get room id ', room);
      this.setState({
        loader: true,
        room: room,
      });
      let res = await Services.get(
        Url.getOldChat +
          `room_id=${room}&pageNo=${this.state.page}&pageSize=10`,
        token,
      );
      if (res.items) {
        let response = [];
        response = res.items;
      
        let currentName = this.state.fromUser;
        let receviceUser = this.state.toUser;
        response.reverse().forEach(function (item, index) {
          let objUser = item;
          let messageID = UUID.v4();
          let text = '';
          let user = {};
          if (item.msgFrom == currentName) {
            user = {
              _id: 1,
              name: currentName,
            };
          } else {
            user = {
              _id: 0,
              name: receviceUser,
            };
          }

          response.splice(index, 1);

          objUser._id = messageID;
          objUser.user = user;
          objUser.text = item.msg;
          response.splice(index, 0, objUser);
        });
        console.log('response////', response);

        this.setState((previousState) => ({
          messages: GiftedChat.append(previousState.messages, response),
        }));
      }
      this.setState({
        loader: false,
      });
    });
  };

  ///////////=========================RECEVIED MESSAGE ====================
  recevicMessage = async () => {
    socket.on('chat-msg', async (data) => {
      console.log('recevied message', data);
      let messageID = UUID.v4();
      await console.log('recevier messgae////////??????', messageID);

      let chat = {
        _id: messageID,
        createdAt: '2021-01-15T08:28:54.546Z',
        text: data.msg,
        user: {
          _id: '600150ce1f1f8a23dbb65ef7',
          name: data.msgFrom,
        },
      };
      if (chat.user.name !== this.state.fromUser) {
        this.setState((previousState) => ({
          messages: GiftedChat.append(previousState.messages, chat),
        }));
        await this.setState({
          typingMessage: false,
        });
      }
    });
  };

  ///////================== SET TWO USER DATA IN STATE ===================
  setAllUserData = async () => {
    let currentUser = await Utility.getFromLocalStorge('userData');
    console.log('current user data');
    socket.emit('set-user-data', currentUser);
    socket.emit('set-room', {
      name1: currentUser + '-' + this.state.toUser,
      name2: this.state.toUser + '-' + currentUser,
    });

    await this.setState({
      fromUser: currentUser,
    });
  };

  /////////============== GET TYPING EVENT =========================
  getTyping = () => {
    socket.on('typing', (msg) => {
      typingMessage = 'sajil';
      // this.setTypingData()
      console.log('user typing', msg);
      if (msg !== '') {
        typingMessage = msg;
        this.setState({
          typingMessage: true,
        });
      }
    });
  };
  setTypingData = (msg) => {
    console.log('setting of res', msg);
  };

  //////================ ON TYPING EVENT SEND ===================
  onInputTextChanged() {
    socket.emit('typing');
  }
  ///////////============== RENDER FOOTER IN GIFTED CHAT ========================
  renderFooter = () => {
    return <Text>{this.state.typingMessage}</Text>;
  };
  onEndReached = async () => {
    let token = await Utility.getFromLocalStorge('token');

    console.log('Loading Messages', this.state.room);
    this.setState({
      page: this.state.page + 1,
    });
    this.roomSet();

    let res = await Services.get(
      Url.getOldChat +
        `room_id=${this.state.room}&pageNo=${this.state.page}&pageSize=10`,
      token,
    );
    if (res.items) {
      let response = [];
      response = res.items;
   
      let currentName = this.state.fromUser;
      let receviceUser = this.state.toUser;
      response.reverse().forEach(function (item, index) {
        let objUser = item;
        let messageID = UUID.v4();
        let text = '';
        let user = {};
        if (item.msgFrom == currentName) {
          user = {
            _id: 1,
            name: currentName,
          };
        } else {
          user = {
            _id: 0,
            name: receviceUser,
          };
        }

        response.splice(index, 1);

        objUser._id = messageID;
        objUser.user = user;
        objUser.text = item.msg;
        response.splice(index, 0, objUser);
      });
      console.log('response////', response);

      this.setState((nextState) => ({
        messages: GiftedChat.append(nextState.messages, response),
      }));
    }
  };
  isCloseToTop({layoutMeasurement, contentOffset, contentSize}) {
    const paddingToTop = 80;
    return (
      contentSize.height - layoutMeasurement.height - paddingToTop <=
      contentOffset.y
    );
  }
  textMessgae = async () => {
    console.log('check');
    let token = await Utility.getFromLocalStorge('token');

    console.log('Loading Messages', this.state.room);
    this.setState({
      page: this.state.page + 1,
    });
    this.roomSet();

    let res = await Services.get(
      Url.getOldChat +
        `room_id=${this.state.room}&pageNo=${this.state.page}&pageSize=10`,
      token,
    );
    if (res.items) {
      let response = [];
      response = res.items;
  
      let currentName = this.state.fromUser;
      let receviceUser = this.state.toUser;
      response.reverse().forEach(function (item, index) {
        let objUser = item;
        let messageID = UUID.v4();
        let text = '';
        let user = {};
        if (item.msgFrom == currentName) {
          user = {
            _id: 1,
            name: currentName,
          };
        } else {
          user = {
            _id: 0,
            name: receviceUser,
          };
        }

        response.splice(index, 1);

        objUser._id = messageID;
        objUser.user = user;
        objUser.text = item.msg;
        response.splice(index, 0, objUser);
      });
      console.log('response////', response);

      this.setState((nextState) => ({
        messages: GiftedChat.append(nextState.messages, response),
      }));
    }
  };
  render() {
    return (
      <View style={{flex: 1}}>
        <Loader isLoading={this.state.loader} />

        <View
          style={{
            width: '100%',
            height: 50,
            justifyContent: 'center',
            backgroundColor: 'blue',
            marginTop: "5%",
            alignItems: 'center',
          }}>
          <Text style={{textAlign: 'center', color: 'white', fontSize: 25}}>
            Message To {this.state.toUser}
          </Text>
        </View>

        <GiftedChat
          messages={this.state.messages}
          onSend={(messages) => this.onSend(messages)}
          user={{_id: 1, name: this.state.fromUser}}
          showUserAvatar
          onInputTextChanged={this.onInputTextChanged}
          loadEarlier={true}
          isTyping={this.state.typingMessage}
          onLoadEarlier={this.onEndReached}
          listViewProps={{
            scrollEventThrottle: 400,
            onScroll: ({nativeEvent}) => {
              if (this.isCloseToTop(nativeEvent)) this.textMessgae();
            },
          }}
        />
      </View>
    );
  }
}
export default ChatHome;



