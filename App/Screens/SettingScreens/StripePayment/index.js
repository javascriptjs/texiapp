

import React, { Component,useState } from "react";
import { StyleSheet, View, Switch,BackHandler, TouchableOpacity,Text,TextInput,Alert} from "react-native";
import { CreditCardInput, LiteCreditCardInput,} from "react-native-credit-card-input";
import Stripe from 'react-native-stripe-api';
import * as Utility from '../../../Utility/index';
import CommonButton from '../../../Components/Button'

const STRIPE_ERROR = 'Payment service error. Try again later.';
const SERVER_ERROR = 'Server error. Try again later.';
// const STRIPE_PUBLISHABLE_KEY = 'pk_test_MzEz6Ixn8ZKY8WTEPLQUu3oz00WnBQ4UkY';
const style = StyleSheet.create({
  switch: {
    alignSelf: "center",
    marginTop: 20,
    marginBottom: 20,
  },
  container: {
    backgroundColor: "#F5F5F5",
    marginTop: 60,
  },
  label: {
    color: "black",
    fontSize: 12,
  },
  input: {
    fontSize: 16,
    color: "black",
  },
});


const StripePayment = ({ navigation }) => {
    const [username, setusername] = useState('');
    const [submitted, Setosubmit] = useState(false);
    const [error, Seterror] = useState('');
    const [card_number, Setcarnumber] = useState('');
    const [card_exp_month, Setcardmonth] = useState('');

    const [card_exp_year, Setcardyear] = useState('');

    const [card_cvc, Setcardcvc] = useState('');
    const [cardData, SetcardData] = useState(false);
    const [email, Setemail] = useState('');
    const [plan, Setplan] = useState('');

    
    
    
      const onSubmit = async (creditCardInput) => {
       
        Setosubmit({ submitted: true });
        let creditCardToken;
        creditCardToken = await getCreditCardToken(creditCardInput);
      }
     const getToke = async (card) => {
       
        const apiKey = 'pk_test_51HWGuZAsGGcPOYa87l6oVAErRu9owDYypYiVYzjPdFNqTr7dh5VJmFzfMONb2ZmvMsceSAOELDvHdR3KxJ0KQXAa00v8YKRxoP';
        const client = new Stripe(apiKey);
        const token = await client.createToken({
          number: card.card_number,
          exp_month: card.exp_month,
          exp_year: card.exp_year,
          cvc: card.cvc,
          address_zip: '12345',
        });
        console.log("tokenuuuuemaillllll", token)
        if(email==""){
         return Alert.alert("Please fill Email Id")
        }
        if(Utility.isValidEmail(email)){
          Alert.alert("Please fill the Valid email")
        }
       
        if (token.error) {
         return Alert.alert("", token.error.code)
        }
        else {
        
          let TokenId = token.id
          console.log("TokenId", TokenId)
          let body = {
            userId: '',
            userName:'',
            amount: '',
         
            stripeToken: TokenId,
            customerEmail:email,
          }
       
          console.log("body  ", body)
          goToHome();
         
         
         }
        }
      
     const goToHome = () => {
   
//    navigation.navigate('Home');
   }
     const getCreditCardToken = (creditCardData) => {
        const card = {
          'card_number': creditCardData.cardData.values.number.replace(/ /g, ''),
          'exp_month': creditCardData.cardData.values.expiry.split('/')[0],
          'exp_year': creditCardData.cardData.values.expiry.split('/')[1],
          'cvc': creditCardData.cardData.values.cvc,
          'cardholder':creditCardData.cardData.values.name,
        }
        console.log("card detail bataoooo",card)
    
        if (card.card_number.length < 15) {
            Setcarnumber({
            card_number: card.card_number
          })
          return Alert.alert("please fill 16 digit valid number")
        }
        if (card.exp_month === "" || card.exp_month === undefined) {
          return Alert.alert("Please fill exp_month")
        }
        else if (card.exp_year === "" || card.exp_year === undefined) {
          return Alert.alert("Please fill exp_year")
        }
        else if (card.cvc === "") {
          return Alert.alert("Please fill cvc number")
        }
        else {
          getToke(card)
        }
    
    }
    
 let state = { useLiteCreditCardInput: false };

  let _onChange = (formData) => console.log(JSON.stringify(formData, null, " "));
 let  _onFocus = (field) => console.log("focusing", field);
 let _setUseLiteCreditCardInput = (useLiteCreditCardInput) => ({ useLiteCreditCardInput });

    return (
      <View style={style.container}>
      
           <Text style={{ fontSize: 22, fontWeight: "700", textAlign: "center", color: "black", textDecorationLine: "underline", marginTop: "5%", marginBottom: "5%" }}>
            Enter Card Detail
         </Text>
           <View>
            <CreditCardInput validColor={"black"}
                inputContainerStyle={{ borderBottomColor: "black", borderBottomWidth: 1 }}
                labelStyle={{ color: "black" }}
                 invalidColor={"red"} placeholderColor={"black"} requiresName onChange={(cardData) => SetcardData({ cardData })} />
            </View>
            
            <TextInput 
            onChangeText={email => Setemail({ email })}
            placeholder={"Enter the  Email"} placeholderTextColor={"black"} style={{ width: "90%", borderBottomColor: "black", borderBottomWidth: 1, alignSelf: "center" ,color:"black"}}>

            </TextInput>
              
               <CommonButton navigating={() =>onSubmit(cardData)} props={"SUBMIT"}></CommonButton>

             </View>
    );
  }
export default StripePayment;
