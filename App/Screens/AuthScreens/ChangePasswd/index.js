

import React, { useState } from 'react';
import { SafeAreaView, StyleSheet, View, Text, Image,Alert, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import styles from './style';
import Mainheader from '../../../Components/Mainheader';
import { Header } from 'react-native-elements';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../../Utility/index';
import CommonButton from '../../../Components/Button'
import TextInputleft from '../../../Components/TextInputleft'
import ImageStart from '../../../Components/ImageStart';
import Mainheading from '../../../Components/Mainheading';
import Subheading from '../../../Components/Subheading';
import * as Utility from '../../../Utility/index';

const ChangePasswd = ({ navigation }) => {
    const [hidePassword, sethidePassword] = useState(true);
    const [password, SetPassword] = useState('');
    const [confirmPassword, Setconf_password] = useState('');
    const [oldpassword, SetOldPassword] = useState('')
    // const managePasswordVisibility = () => {
    //     sethidePassword({ hidePassword: !hidePassword });
    // }

    const Nextview = () => {
        if (
            Utility.isFieldEmpty((password && confirmPassword && oldpassword))
        ) {
            return Alert.alert('Please fill all the fields');
        } else if (Utility.isValidComparedPassword(password, confirmPassword)) {
            return Alert.alert('Password Mismatch');
        }
        navigation.navigate("Setting")
    }

    return (
        <View style={styles.mainhd}>
            <Mainheader navigation={navigation} navigating={() => navigation.goBack()} image={"arrow-back"}></Mainheader>

            <ScrollView>
                <View style={styles.containerMain}>
                    <ImageStart Images={require('../../../Assets/changepwd.png')}></ImageStart>


                    <View style={styles.main1}>
                        <View style={{ marginTop: hp('4%') }}>
                          
                            <TextInputleft placeholdertext={"Old Password"} Image3={'remove-red-eye'} onchangetexting={oldpassword => SetOldPassword(oldpassword)}></TextInputleft>
                            <View style={{marginTop: hp("3%")}}>
                            <TextInputleft placeholdertext={"Password"} secureTextEntryes={hidePassword} Image3={'remove-red-eye'} onchangetexting={password => SetPassword(password)}></TextInputleft>
                            </View>
                            <View style={{marginTop: hp("3%")}}>
                            <TextInputleft placeholdertext={"Confirm Password"} secureTextEntryes={hidePassword} Image3={'remove-red-eye'} onchangetexting={confirmPassword =>
                                Setconf_password(confirmPassword)
                            }></TextInputleft>
                            </View>
                        </View>
                        <CommonButton navigating={() => Nextview()} props={"Save Now !"}></CommonButton>

                    </View>
                </View>
            </ScrollView>
        </View>
    );

};


export default ChangePasswd;
