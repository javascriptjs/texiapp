import React, { useEffect } from 'react';
import { useState } from 'react';
import { View, Text, ImageBackground, Image, Switch } from 'react-native';
import Mainheader from '../../../Components/Mainheader';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from '../../../Utility';
import Icon from 'react-native-vector-icons/AntDesign';
import MapboxGL from '@react-native-mapbox-gl/maps';
import styles from "./style"
import { CheckBox } from 'react-native-elements'
import constants from '../../../Constants/colors';
import CommonButton from '../../../Components/Button'

const AddVechile = ({ navigation }) => {
  const [isSelected1, setSelection1] = useState(false);
  const [isSelected2, setSelection2] = useState(false);
  const Nextview = () => {
    navigation.navigate("Vehicle")
  }
  return (

    <View style={styles.main}>
      <Mainheader navigation={navigation} navigating={() => navigation.goBack()} props='Vehicle Management' image={"arrow-back"}></Mainheader>

      {/* 

      <View style={{ flexDirection: "row",backgroundColor:"pink",    paddingTop: hp("2%"),
        paddingBottom: hp("2%"),
        paddingLeft: wp("2%"), width: wp('90%'), marginTop: hp('4%'), alignSelf: "center" }}>
       
        <Image source={{ uri: "https://icones.pro/wp-content/uploads/2021/03/icone-de-voiture-symbole-png-jaune.png" }} style={{ width: wp('10%'), height: hp('6%'), borderRadius: 20 }}></Image>
        <View style={{ width: wp('60%'), paddingLeft: wp('4%') }}>

          <Text style={{ fontSize: 16, fontWeight: "bold" }}>Personal Vehicle</Text>
          <Text style={{ color: "grey" }}>43R 240.8090.30</Text>

        </View>
       
        <CheckBox
          checked={isSelected1}
          checkedColor={constants.checkgreen}
          size={wp("6%")}
          onPress={() => setSelection1(!isSelected1)}
        />
      </View> */}
<View style={styles.row1}>

<View style={styles.img}>
  <Image  source={{ uri: "https://icones.pro/wp-content/uploads/2021/03/icone-de-voiture-symbole-png-jaune.png" }} style={{ width: wp('12%'), height: hp('6%'), borderRadius: 100 }}></Image>

</View>

<View style={styles.col}>
  <View style={styles.col1}>

    <Text style={styles.text}>Personal Vehicle</Text>
    <Text style={styles.text2}>43R 240.8090.30</Text>
  </View>
</View>

{/* <CheckBox
  checked={true}
/> */}
<View style={{ width: wp('10%'), flexDirection: "column" }}>

  <CheckBox
              checked={isSelected1}

    checkedColor={constants.checkgreen}
    size={wp("6%")}
    onPress={() => setSelection1(!isSelected1)}

  />
</View>
</View>
      <View style={styles.row1}>

        <View style={styles.img}>
          <Image source={require('../../../Assets/logo.png')} style={{ width: wp('12%'), height: hp('6%'), borderRadius: 100 }}></Image>

        </View>

        <View style={styles.col}>
          <View style={styles.col1}>

            <Text style={styles.text}>Company Vehicle</Text>
            <Text style={styles.text2}>43R 240.8090.30</Text>
          </View>
        </View>

        {/* <CheckBox
          checked={true}
        /> */}
        <View style={{ width: wp('10%'), flexDirection: "column" }}>

          <CheckBox
            checked={isSelected2}
            checkedColor={constants.checkgreen}
            size={wp("6%")}
            onPress={() => setSelection2(!isSelected2)}
          />
        </View>
      </View>


      <CommonButton navigating={() => Nextview()} props={"Add New"}></CommonButton>



    </View>
  );
};

export default AddVechile;
