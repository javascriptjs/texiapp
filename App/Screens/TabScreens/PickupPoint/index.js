

import React, { useState, useRef, useEffect } from 'react';
import { SafeAreaView, StyleSheet, View, Text, Image, TouchableOpacity, Dimensions, Switch, } from 'react-native';
import styles from './style';
import Mainheader from '../../../Components/Mainheader';

import MapView, { Callout, Marker } from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import constants from '../../../Constants/colors';
import {useSelector, useDispatch} from 'react-redux';

const { width, height } = Dimensions.get('window')
const ASPECT_RATIO = width / height
const LATITUDE_DELTA = 0.005
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO

const PickupPoint = ({ navigation }) => {
    const GOOGLE_MAPS_APIKEY = "AIzaSyAU2NF-HSqBGtddXdSELItUumaj2LEz1IQ";
    const dispatch = useDispatch();
    const CurrentLocation = useSelector(state => state.GET_CURRENT_LOCATION);
    const [mapsize, setmap] = useState(false)
  
    const updown = (value) => {
        setmap(value)
    }
    const [toggle, setToggle] = useState(true);
    const [pickup, Setpickup] = useState('');

    
    const [Nextsceen, setvalue] = useState('');
    const Nextview = (item) => {
        console.log('item', item)
        if (item.back == true) {
            setvalue(true);
        }
       


    }


    return (
        <View style={{ flex: 1, backgroundColor: constants.backgroundDull }}>

<Mainheader navigation={navigation} navigating={() => navigation.goBack()} props='Pickup' image={"arrow-back"}></Mainheader>
          
            <MapView style={styles.map}
                // minZoomLevel={12}
                zoomEnabled={true}
                // minZoomLevel={10}
                initialRegion={{
                    latitude:Number(CurrentLocation.currentLatitude),
                    longitude:Number(CurrentLocation.currentLongitude),
                    latitudeDelta: 0.40,
                    longitudeDelta: 0.40,
                }}>

                <Marker coordinate={{ latitude:Number(CurrentLocation.currentLatitude),
                        longitude:Number(CurrentLocation.currentLongitude),}}>
                <Image
                        source={require('../../../Assets/UberX.png')}
                        style={{ width: 30, height: 30, resizeMode: "contain" }}
                        resizeMode="contain"
                    />
                </Marker>
              
                <Marker
                    coordinate={{ latitude: 30.709457, longitude:76.77549 }}>
                  

                </Marker>
                <MapViewDirections
                    origin={{ latitude:Number(CurrentLocation.currentLatitude),
                        longitude:Number(CurrentLocation.currentLongitude),}}
                    destination={{ latitude: 30.709457, longitude:76.77549 }}
                    apikey={"AIzaSyAU2NF-HSqBGtddXdSELItUumaj2LEz1IQ"}
                    strokeWidth={5}
                    strokeColor="blue"
                />
            </MapView>
            


        </View>
    );

};


export default PickupPoint;
